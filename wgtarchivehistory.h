#ifndef WGTARCHIVEHISTORY_H
#define WGTARCHIVEHISTORY_H

#include <QWidget>
#include "wgtaddoutdoc.h"
//#include "udatabase.h"

namespace Ui {
class wgtarchivehistory;
}

class wgtarchivehistory : public QWidget
{
    Q_OBJECT

public:
    explicit wgtarchivehistory(QWidget *parent = 0);
    ~wgtarchivehistory();
	DBOperations *dboper;
	int iMode;

public slots:
	void SetArcHistoryFields(int iRow);
	void SetArchiveHistoryLinks();

signals:
	void BackToArchive(int iMode);

private slots:
    void on_pbBack2Archive_clicked();

    void on_tvIncoming_doubleClicked(const QModelIndex &index);

    void on_tvDocuments_doubleClicked(const QModelIndex &index);

private:
    Ui::wgtarchivehistory *ui;
	wgtaddoutdoc *wAddOutDoc;
};

#endif // WGTARCHIVEHISTORY_H
