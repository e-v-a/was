#include "wgtlawsuites.h"
#include "ui_wgtlawsuites.h"
#include "datedelegate.h"

wgtLawsuites::wgtLawsuites(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtLawsuites)
{
    ui->setupUi(this);
	iRowIndex=-1;
	wAddOutDoc = new wgtaddoutdoc(this);
	wAddOutDoc->setVisible(false);
    wAddOutDoc->setWindowFlags(Qt::Dialog);
    wAddOutDoc->setWindowFlags(wAddOutDoc->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wAddOutDoc->setWindowModality(Qt::WindowModal);
	connect(wAddOutDoc,SIGNAL(updateLSTable()),this,SLOT(updateLSTable()));
    actEdit=new QAction(tr("Edit"),this);
    actDelete=new QAction(tr("Delete"),this);
	actSearch=new QAction(tr("Search"),this);
	actSearch->setCheckable(true);
	QList<QKeySequence> scList;
	scList.append(QKeySequence(Qt::CTRL + Qt::Key_F));
	scList.append(QKeySequence(Qt::Key_F3));
	actSearch->setShortcuts(scList);
    connect(actEdit, SIGNAL(triggered()), this, SLOT(EditAction()));
    connect(actDelete, SIGNAL(triggered()), this, SLOT(DeleteAction()));
	connect(actSearch, SIGNAL(toggled(bool)), this, SLOT(on_pbShowSearch_toggled(bool)));
	addAction(actSearch);
	ui->tvOutcoming->addAction(actEdit);
	ui->tvOutcoming->addAction(actDelete);
	ui->tvOutcoming->setContextMenuPolicy(Qt::ActionsContextMenu);
	ui->pbSendToAgreement->setVisible(false);
    bIsSearch=false;
	bIsNew=false;
	ui->lblNoMatches->setVisible(false);
	ui->pbAddCaseCard->setVisible(false);
	ui->pbDeleteCaseCard->setVisible(false);
	ui->wgtArcNotify->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtLawsuites::~wgtLawsuites()
{
    delete ui;
}

void wgtLawsuites::on_leSearch_returnPressed()
{
	ui->pbSearch->click();
}

void wgtLawsuites::on_leSearch_textChanged(const QString &arg1)
{
	if(arg1!="") ui->pbClear->setVisible(true);
    else ui->pbClear->setVisible(false);
}

void wgtLawsuites::on_pbClear_clicked()
{
    QString strFilter;
    ui->leSearch->clear();
    strFilter="[executor] = '"+dboper->UsrSettings.sUserName+"' ORDER BY [claimdate] DESC, [casenum] DESC";
    dboper->mdlDB05->setFilter(strFilter);
    dboper->mdlDB05->select();

    if(dboper->mdlDB05->rowCount() > 0) {
        iRowCount=dboper->mdlDB05->rowCount();
        iCurrentRow=0;
        emit setStatusText(iCurrentRow+1, iRowCount);
        SetLSFormFields(iCurrentRow);
    }
	ui->lblNoMatches->setVisible(false);
}

void wgtLawsuites::on_pbSearch_clicked()
{
    QString strSearchString;
    if(ui->leSearch->text()!="") {
        bIsSearch=true;
        QString arg1=ui->leSearch->text();
        strSearchString="([casenum] LIKE '%"+arg1+"%' OR [complainant] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1+"%') AND ([executor]='";
        strSearchString+=dboper->UsrSettings.sUserName+"') ORDER BY [exdate] DESC, [casenum] DESC";

        dboper->mdlDB05->setFilter(strSearchString);
        dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount() == 0) {
			ui->wgtArcNotify->setVisible(true);
			ui->lblNoMatches->setVisible(true);
		}
        else if(dboper->mdlDB05->rowCount() > 0) {
			ui->lblNoMatches->setVisible(false);
			ui->wgtArcNotify->setVisible(false);
            iRowCount=dboper->mdlDB05->rowCount();
            iCurrentRow=0;
            emit setStatusText(iCurrentRow+1, iRowCount);
            SetLSFormFields(iCurrentRow);
        }
    }

}

void wgtLawsuites::on_pbArc_clicked()
{
	QString strFields;
	QString strValues;
	QString strBuffer;
	bool bNotAgreed=false;
	if(dboper->stmdlEOoutcomingDocs->rowCount()>0) {
		for(int i=0;i<dboper->stmdlEOoutcomingDocs->rowCount();i++) {
			if(!dboper->stmdlEOoutcomingDocs->record(i).value("agreed").toBool()  
				&& (dboper->stmdlEOoutcomingDocs->record(iCurrentRow).value("doctype").toString()!=tr("Decision") 
				&& dboper->stmdlEOoutcomingDocs->record(iCurrentRow).value("doctype").toString()!=tr("Ruling") 
				&& dboper->stmdlEOoutcomingDocs->record(iCurrentRow).value("doctype").toString()!=tr("Judgement"))) {
				QMessageBox::warning(0, tr("Archiving is not possible"),	
					tr("One or several outcoming documents are not agreed. Archiving is not possible until all procedural documents are agreed."));
				bNotAgreed=true;
				break;
			}
		}
	}
	if(!bNotAgreed) {
		QString strSlashedUrl;
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Archive case"));
		msgQuestion.setText(tr("You're going to archive current case. Any information about case will be freezed. Do you want to continue?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) {
			strFields="[casenum],[claimdate],[complainant],[defendant],[thirdpersons],[otherpersons],[judge],[trialdatetime],[courtaddress],[casecardurl],[result],[instance],[executor]";
			strValues="'"+ui->leCaseNum->text()+"',#"+ui->deClaimDate->date().toString("yyyy-MM-dd")+"#,'";
			strValues+=ui->teComplainant->toPlainText()+"','"+ui->teDefendant->toPlainText()+"','"+ui->teThirdPersons->toPlainText()+"','";
			strValues+=ui->teOtherPersons->toPlainText()+"','"+ui->leJudge->text()+"',#";
			strValues+=ui->dteTrialDateTime->dateTime().toString("yyyy-MM-dd hh:mm:ss")+"#,'"+ui->leCourtAddress->text()+"','";
			strSlashedUrl=ui->lblCaseLink->text();
			strSlashedUrl.replace("'","\'\'");
			strValues+=strSlashedUrl+"','"+ui->cbResult->currentText()+"','"+ui->cbInstance->currentText()+"','";
			strValues+=dboper->UsrSettings.sUserName+"'";

			dboper->dbQuery(PRIVATE_QUERY,"DB05",1,strFields,strValues,"lawsuites_arc",false,"");
			dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","lawsuites_arc",true,"[casenum]='"+ui->leCaseNum->text()+"'");
			dboper->query.next();
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedto]$[attachedtodocid]";
			strValues="'"+tr("Lawsuites archive")+"'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"incoming",true,"[attachedto]='"+tr("Lawsuite")+"' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedtodoctype]$[attachedtodocid]";
			strValues="'ls_a'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,"[attachedtodoctype]='ls' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","lawsuites",true,"[casenum]='"+ui->leCaseNum->text()+"'");
			dboper->mdlDB05->select();
			iRowCount=dboper->mdlDB05->rowCount();
			iCurrentRow=0;
			emit setStatusText(iCurrentRow, iRowCount);
			SetLSFormFields(iCurrentRow);
		}
	}
}

void wgtLawsuites::on_leCaseNum_textChanged(const QString &arg1)
{
	QString strCaseLink;
	strCaseLink="<a href='http://kad.arbitr.ru/Card?number=";
	strCaseLink+=ui->leCaseNum->text() +"'>http://kad.arbitr.ru/Card?number=";
	strCaseLink+=ui->leCaseNum->text() + "</a>";
	ui->lblCaseLink->setText(strCaseLink);
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_deClaimDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_teComplainant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_teDefendant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_teThirdPersons_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_teOtherPersons_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_pbAddCaseCard_clicked()
{
	bool bOk;
	QString strCaseLinkUrl=QInputDialog::getText(this,tr("Add link to case card"),tr("Paste URL from kad.arbitr.ru"),QLineEdit::Normal,"http://", &bOk);
	if(bOk && !strCaseLinkUrl.isEmpty())
		ui->lblCaseLink->setText(strCaseLinkUrl);
	
}

void wgtLawsuites::on_pbDeleteCaseCard_clicked()
{
	ui->lblCaseLink->setText("http://");
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_cbResult_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_pbAdd_clicked()
{
	wAddOutDoc->setAODVars("ls", iRowIndex);
    wAddOutDoc->setVisible(true);
}

void wgtLawsuites::on_pbSendToAgreement_clicked()
{
	if(ui->tvOutcoming->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("agreed").toBool()) {
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvOutcoming->currentIndex().row(),8),true);
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvOutcoming->currentIndex().row(),9),false);
		dboper->stmdlEOoutcomingDocs->submitAll();
		ui->pbSendToAgreement->setVisible(false);
	}
}

void wgtLawsuites::on_pbShowSearch_toggled(bool checked)
{
	if(ui->pbSave->isEnabled()) AskSaving();
    bIsSearch=checked;
	ui->wgtSearch->setVisible(checked);
    ui->leSearch->clear();
    (checked)?ui->leSearch->setFocus():ui->pbClear->click();
	ui->pbShowSearch->setChecked(checked);
	actSearch->setChecked(checked);
    ui->lblNoMatches->setVisible(false);
	ui->pbClear->setVisible(false);
}

void wgtLawsuites::on_pbDelete_clicked()
{
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setWindowTitle("was");
	msgQuestion.setText(tr("You're going to delete the case. This will cause the removal of all attached documents. Are you sure?"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAdd->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);
	
	if(msgQuestion.exec()==QMessageBox::Yes) {
		QSqlQuery query;
		QString strBuffer;
		QString strTextFile;
		QString strScanFile;
		strBuffer.setNum(iRowIndex);
		QFile flAttachment;
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","documents",true,"[attachedtodoctype]='ls' AND [attachedtodocid]="+strBuffer);
		strBuffer=dboper->EnvSettings.strCommonFolder+"/";
		while(dboper->query.next()) {
			strTextFile=dboper->query.value("textfile").toString();
			if(strTextFile!="") {
				flAttachment.setFileName(strBuffer+strTextFile);
				flAttachment.remove();
			}
			strScanFile=dboper->query.value("scanfile").toString();
			if(strScanFile!="") {
				flAttachment.setFileName(strBuffer+strScanFile);
				flAttachment.remove();
			}
		}
		strBuffer.setNum(iRowIndex);
		dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,"[attachedtodoctype]='ls' AND [attachedtodocid]="+strBuffer);
		dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","admcases",true,"[id]="+strBuffer);
	}
	ui->pbFirst->click();
}

void wgtLawsuites::on_pbFirst_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=0;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetLSFormFields(iCurrentRow);
}

void wgtLawsuites::on_pbPrev_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow--;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetLSFormFields(iCurrentRow);
}

void wgtLawsuites::on_pbSave_clicked()
{
	QString strBuffer;
	QStringList stlFields;
	QList<QVariant> stlValues;

	stlFields.append("1");//casenum
	stlValues.append(ui->leCaseNum->text());
	stlFields.append("2");//claimdate
	stlValues.append(ui->deClaimDate->date());
	stlFields.append("3");//complainant
	stlValues.append(ui->teComplainant->toPlainText());
	stlFields.append("4");//defendant
	stlValues.append(ui->teDefendant->toPlainText());
	stlFields.append("5");//thirdpersons
	stlValues.append(ui->teThirdPersons->toPlainText());
	stlFields.append("6");//otherpersons
	stlValues.append(ui->teOtherPersons->toPlainText());
	stlFields.append("7");//judge
	stlValues.append(ui->leJudge->text());
	stlFields.append("8");//trialdatetime
	stlValues.append(ui->dteTrialDateTime->dateTime());
	stlFields.append("9");//courtaddress
	stlValues.append(ui->leCourtAddress->text());
	stlFields.append("10");//casecardurl
	stlValues.append(ui->lblCaseLink->text());
	stlFields.append("11");//result
	stlValues.append(ui->cbResult->currentText());
	stlFields.append("12");//instance
	stlValues.append(ui->cbInstance->currentText());
	if(bIsNew) {
		stlFields.append("13");//executor
		stlValues.append(dboper->UsrSettings.sUserName);
		iCurrentRow=0;
	}
	ui->pbSave->setEnabled(!dboper->dbWriteData(bIsNew,true,iCurrentRow,stlFields,stlValues));
	bFormChanged=ui->pbSave->isEnabled();

	if(bIsNew) {
		bIsNew=false;
		emit LoadAddedLS(ui->leCaseNum->text());
	}
}

void wgtLawsuites::on_pbNext_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow++;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetLSFormFields(iCurrentRow);
}

void wgtLawsuites::on_pbLast_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=iRowCount-1;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetLSFormFields(iCurrentRow);
}

void wgtLawsuites::on_cbInstance_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::SetLSFormFields(int recordnum) {
	ui->leCaseNum->clear();
	ui->deClaimDate->clear();
	ui->teComplainant->clear();
	ui->teDefendant->clear();
	ui->teThirdPersons->clear();
	ui->teOtherPersons->clear();
	ui->leJudge->clear();
	ui->dteTrialDateTime->clear();
	ui->cbResult->clear();
	ui->cbInstance->clear();
	//ui->pbDeleteCaseCard->setVisible(false);
	ui->leCourtAddress->clear();
	ui->deClaimDate->setDate(QDate(2013,1,1));
	ui->dteTrialDateTime->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	ui->wgtSearch->setVisible(bIsSearch);
	if(ui->lblTitle->text()!=tr("Lawsuites")) ui->lblTitle->setText(tr("Lawsuites"));
	
	iRowIndex=dboper->mdlDB05->record(recordnum).value("id").toInt();
	ui->leCaseNum->setText(dboper->mdlDB05->record(recordnum).value("casenum").toString());
	ui->deClaimDate->setDate(dboper->mdlDB05->record(recordnum).value("claimdate").toDate());
	ui->teComplainant->setPlainText(dboper->mdlDB05->record(recordnum).value("complainant").toString());
	ui->teDefendant->setPlainText(dboper->mdlDB05->record(recordnum).value("defendant").toString());
	ui->teThirdPersons->setPlainText(dboper->mdlDB05->record(recordnum).value("thirdpersons").toString());
	ui->teOtherPersons->setPlainText(dboper->mdlDB05->record(recordnum).value("otherpersons").toString());
	ui->leJudge->setText(dboper->mdlDB05->record(recordnum).value("judge").toString());
	ui->dteTrialDateTime->setDateTime(dboper->mdlDB05->record(recordnum).value("trialdatetime").toDateTime());
	ui->lblCaseLink->setText(dboper->mdlDB05->record(recordnum).value("casecardurl").toString());
	/*if(dboper->mdlDB05->record(recordnum).value("casecardurl").toString()!="http://")
		ui->pbDeleteCaseCard->setVisible(true);
	else ui->pbDeleteCaseCard->setVisible(false);*/
	ui->leCourtAddress->setText(dboper->mdlDB05->record(recordnum).value("courtaddress").toString());

	if(ui->cbResult->count()==0) ui->cbResult->addItems(stlDecisionTypes);
	ui->cbResult->setCurrentIndex(ui->cbResult->findText(dboper->mdlDB05->record(recordnum).value("result").toString(),Qt::MatchExactly));

	if(ui->cbInstance->count()==0) ui->cbInstance->addItems(stlInstances);
	ui->cbInstance->setCurrentIndex(ui->cbInstance->findText(dboper->mdlDB05->record(recordnum).value("instance").toString(),Qt::MatchExactly));

	dboper->stmdlEOincomingDocs->setTable("incoming");
	QString strFilter;
	strFilter="[attachedto] = '";
	strFilter+=tr("Lawsuite");
	strFilter+="' AND [attachedtodocid]=";
	QString strBuffer;
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;
	
	dboper->stmdlEOincomingDocs->setFilter(strFilter);
	dboper->stmdlEOincomingDocs->select();
	dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
	dboper->stmdlEOincomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOincomingDocs->setHeaderData(8, Qt::Horizontal, tr("Addressee"));
	dboper->stmdlEOincomingDocs->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->stmdlEOincomingDocs->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	ui->tvIncoming->setModel(dboper->stmdlEOincomingDocs);
	ui->tvIncoming->hideColumn(0);
	int i;
	for(i=3;i<8;i++) ui->tvIncoming->hideColumn(i);
	ui->tvIncoming->hideColumn(9);
	for(i=12;i<20;i++) ui->tvIncoming->hideColumn(i);
	
	ui->tvIncoming->resizeColumnsToContents();
	ui->tvIncoming->horizontalHeader()->setStretchLastSection(true);

	dboper->stmdlEOoutcomingDocs->setTable("documents");
	strFilter="[attachedtodoctype] = 'ls'";
	strFilter+=" AND [attachedtodocid]=";
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;

	dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
	dboper->stmdlEOoutcomingDocs->select();
	dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document number"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Document date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Reg Num"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Document Type"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Post ID"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Execution date"));
	ui->tvOutcoming->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvOutcoming->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvOutcoming->setItemDelegateForColumn(7, new DataDelegate());
	ui->tvOutcoming->setModel(dboper->stmdlEOoutcomingDocs);
	ui->tvOutcoming->hideColumn(0);
	
	for(i=8;i<16;i++) ui->tvOutcoming->hideColumn(i);
	ui->tvOutcoming->resizeColumnsToContents();
	ui->tvOutcoming->horizontalHeader()->setStretchLastSection(true);
	ui->pbSave->setEnabled(false);
	bFormChanged=false;

	ui->leCaseNum->setEnabled(!iRowCount<1);
	ui->deClaimDate->setEnabled(!iRowCount<1);
	ui->teComplainant->setEnabled(!iRowCount<1);
	ui->teDefendant->setEnabled(!iRowCount<1);
	ui->teThirdPersons->setEnabled(!iRowCount<1);
	ui->teOtherPersons->setEnabled(!iRowCount<1);
	ui->leJudge->setEnabled(!iRowCount<1);
	ui->dteTrialDateTime->setEnabled(!iRowCount<1);
	ui->cbResult->setEnabled(!iRowCount<1);
	ui->cbInstance->setEnabled(!iRowCount<1);
	ui->leCourtAddress->setEnabled(!iRowCount<1);
	ui->pbShowSearch->setEnabled(!iRowCount<1);
	ui->pbDelete->setEnabled(!iRowCount<1);

	ui->twDocuments->setEnabled(!iRowCount<1);
	ui->pbFirst->setEnabled(!(iRowCount<=1 || iCurrentRow+1==1));
	ui->pbPrev->setEnabled(!(iRowCount<=1 || iCurrentRow+1==1));
	ui->pbNext->setEnabled(!(iRowCount<=1 || iCurrentRow+1==iRowCount));
	ui->pbLast->setEnabled(!(iRowCount<=1 || iCurrentRow+1==iRowCount));
}

void wgtLawsuites::EnableLSBlankFields() {
	ui->leCaseNum->clear();
	ui->leCaseNum->setEnabled(true);
	ui->deClaimDate->clear();
	ui->deClaimDate->setEnabled(true);
	ui->teComplainant->clear();
	ui->teComplainant->setEnabled(true);
	ui->teDefendant->clear();
	ui->teDefendant->setEnabled(true);
	ui->teThirdPersons->clear();
	ui->teThirdPersons->setEnabled(true);
	ui->teOtherPersons->clear();
	ui->teOtherPersons->setEnabled(true);
	ui->leJudge->clear();
	ui->leJudge->setEnabled(true);
	ui->dteTrialDateTime->clear();
	ui->dteTrialDateTime->setEnabled(true);
	ui->cbResult->clear();
	ui->cbResult->setEnabled(true);
	ui->cbInstance->clear();
	ui->cbInstance->setEnabled(true);
	//ui->pbDeleteCaseCard->setVisible(false);
	ui->leCourtAddress->clear();
	ui->leCourtAddress->setEnabled(true);
	ui->deClaimDate->setDate(QDate(2013,1,1));
	ui->dteTrialDateTime->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	if(ui->cbResult->count()==0) ui->cbResult->addItems(stlDecisionTypes);
	if(ui->cbInstance->count()==0) ui->cbInstance->addItems(stlInstances);

	ui->pbFirst->setEnabled(false);
	ui->pbPrev->setEnabled(false);
	ui->pbNext->setEnabled(false);
	ui->pbLast->setEnabled(false);
	ui->pbShowSearch->setEnabled(false);
	ui->pbDelete->setEnabled(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
	ui->lblTitle->setText(tr("New lawsuite"));
}

void wgtLawsuites::updateLSTable() {
	dboper->stmdlEOoutcomingDocs->select();
}

void wgtLawsuites::setLSOperLink() {
	wAddOutDoc->dboper=dboper;
}

void wgtLawsuites::clearLSSearch() {
	bIsSearch=false;
	ui->leSearch->clear();
	ui->pbClear->setVisible(false);
	ui->pbShowSearch->setChecked(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
}

void wgtLawsuites::on_leJudge_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_dteTrialDateTime_dateTimeChanged(const QDateTime &dateTime)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::on_leCourtAddress_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtLawsuites::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}

void wgtLawsuites::EditAction() {
	if(ui->tvOutcoming->currentIndex().isValid()) {
		wAddOutDoc->setAODVars("ls", iRowIndex,true,dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("id").toInt());
		wAddOutDoc->setVisible(true);
	}
}

void wgtLawsuites::DeleteAction() {
	if(ui->tvOutcoming->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("agreed").toBool()) {
		QString strID;
		QString strSelectString;
		QMessageBox msgQuestion;
		
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Delete document"));
		msgQuestion.setText(tr("You're going to delete selected document. Are you sure?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		
		msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
		
		msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
		msgQuestion.button(QMessageBox::No)->setText(tr("No"));

		QFont fntBold;
		fntBold.setFamily(ui->pbAdd->font().family());
		fntBold.setPointSize(8);
		fntBold.setBold(true);
		msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);
		if(msgQuestion.exec()==QMessageBox::Yes)
			wAddOutDoc->deleteDocument(dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("id").toInt());

		dboper->stmdlEOoutcomingDocs->select();
	}
}

void wgtLawsuites::on_tvOutcoming_clicked(const QModelIndex &index)
{
	if(!dboper->stmdlEOoutcomingDocs->record(index.row()).value("senttoagreement").toBool() 
		&& (dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()!=tr("Decision") 
		&& dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()!=tr("Ruling") 
		&& dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()!=tr("Judgement"))) ui->pbSendToAgreement->setVisible(true);
	else ui->pbSendToAgreement->setVisible(false);
}

void wgtLawsuites::on_tvOutcoming_doubleClicked(const QModelIndex &index)
{
	EditAction();
}