#include <qmessagebox.h>
#include "wgtamzclaims.h"
#include "ui_wgtamzclaims.h"
#include "datedelegate.h"

wgtAMZClaims::wgtAMZClaims(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAMZClaims)
{
    ui->setupUi(this);
	bFormChanged=false;
	wAddOutDoc = new wgtaddoutdoc(this);
	wAddOutDoc->setVisible(false);
    wAddOutDoc->setWindowFlags(Qt::Dialog);
    wAddOutDoc->setWindowFlags(wAddOutDoc->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wAddOutDoc->setWindowModality(Qt::WindowModal);
	connect(wAddOutDoc,SIGNAL(updateACLTable()),this,SLOT(updateACLTable()));
    actEdit=new QAction(tr("Edit"),this);
    actDelete=new QAction(tr("Delete"),this);
	actSearch=new QAction(tr("Search"),this);
	actSearch->setCheckable(true);
	QList<QKeySequence> scList;
	scList.append(QKeySequence(Qt::CTRL + Qt::Key_F));
	scList.append(QKeySequence(Qt::Key_F3));
	actSearch->setShortcuts(scList);
    connect(actEdit, SIGNAL(triggered()), this, SLOT(EditAction()));
    connect(actDelete, SIGNAL(triggered()), this, SLOT(DeleteAction()));
	connect(actSearch, SIGNAL(toggled(bool)), this, SLOT(on_pbShowSearch_toggled(bool)));
	addAction(actSearch);
	ui->tvDocuments->addAction(actEdit);
	ui->tvDocuments->addAction(actDelete);
	ui->tvDocuments->setContextMenuPolicy(Qt::ActionsContextMenu);
	ui->pbSend2Agreement->setVisible(false);
	ui->pbPublish->setVisible(false);
    bIsSearch=false;
	ui->lblNoMatches->setVisible(false);
	ui->wgtArcNotify->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtAMZClaims::~wgtAMZClaims()
{
    delete ui;
}

void wgtAMZClaims::on_pbFirst_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=0;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetClaimsFormFields(iCurrentRow);
}

void wgtAMZClaims::on_pbPrev_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow--;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetClaimsFormFields(iCurrentRow);
}

void wgtAMZClaims::on_pbSave_clicked()
{
	QStringList stlFields;
	QList<QVariant> stlValues;

	stlFields.append("4");//executor
	stlValues.append(ui->cbExecutor->currentText());
	stlFields.append("5");//complainant
	stlValues.append(ui->teComplainant->toPlainText());
	stlFields.append("6");//defendant
	stlValues.append(ui->teDefendant->toPlainText());
	stlFields.append("7");//clause
	stlValues.append(ui->leClause->text());
	stlFields.append("8");//nla
	stlValues.append(ui->cbNLA->currentText());
	stlFields.append("9");//summary
	stlValues.append(ui->teSummary->toPlainText());
	if(ui->cbProlong->isChecked()) {
		stlFields.append("10");//prolongdate
		stlValues.append(ui->deTill->date());
	}
	stlFields.append("11");//result
	stlValues.append(ui->cbResult->currentText());
	stlFields.append("12");//casenum
	stlValues.append(ui->leCaseNum->text());
	stlFields.append("13");//prolong
	stlValues.append(ui->cbProlong->isChecked());
	
	if(ui->cbExecutor->currentText()!=dboper->UsrSettings.sUserName) {
		QString strFilter;
		strFilter="[attachedto]='";
		strFilter+=tr("Claim");
		strFilter+="' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString();//strBuffer;
		emit ExecQuery(PRIVATE_QUERY,"DB05",2,"[executor]", "'"+ui->cbExecutor->currentText()+"'","incoming",true,strFilter);
	}

	ui->pbSave->setEnabled(!dboper->dbWriteData(false,true,iCurrentRow,stlFields,stlValues));
	bFormChanged=ui->pbSave->isEnabled();
}

void wgtAMZClaims::on_pbNext_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow++;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetClaimsFormFields(iCurrentRow);
}

void wgtAMZClaims::on_pbLast_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=iRowCount-1;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetClaimsFormFields(iCurrentRow);
}

void wgtAMZClaims::on_pbGoToCase_clicked()
{
	QString strFilter="[casenum]='"+ui->leCaseNum->text()+"'";
	dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"count(*)","","amzcases",true,strFilter);
	dboper->query.next();
	int iQuerySize=dboper->query.value(0).toInt();
	dboper->query.finish();
	//int iRowCount=-1;
	if(iQuerySize==0) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle("was");
		msgQuestion.setText(tr("There'll be created a new case '")+ui->leCaseNum->text()+"'. "+tr("Do you want to continue?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);

		msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
		msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

		msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
		msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
		msgQuestion.button(QMessageBox::No)->setText(tr("No"));

		QFont fntBold;
		fntBold.setFamily(ui->pbAdd->font().family());
		fntBold.setPointSize(8);
		fntBold.setBold(true);
		msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

		if(msgQuestion.exec()==QMessageBox::Yes) {
			QString strFields;
			QString strValues;
			strFields="[claimnum],[claimdate],[casenum],[handoverdate],[executor],[complainant],[defendant],[clause],[nla]";
			strValues="'";
			strValues+=ui->leRegnum->text(); //regnum
			strValues+="',#";
			strValues+=ui->deRegDate->date().toString("yyyy-MM-dd"); //regdate
			strValues+="#,'";
			strValues+=ui->leCaseNum->text(); //casenum
			strValues+="',#";
			strValues+=ui->deHandoverDate->date().toString("yyyy-MM-dd"); //handoverdate
			strValues+="#,'";
			strValues+=ui->cbExecutor->currentText(); //executor
			strValues+="','";
			strValues+=ui->teComplainant->toPlainText(); // complainant;
			strValues+="','";
			strValues+=ui->teDefendant->toPlainText(); // defendant;
			strValues+="','";
			strValues+=ui->leClause->text(); //clause
			strValues+="','"+ui->cbNLA->currentText()+"'"; //NLA

			dboper->dbQuery(PRIVATE_QUERY,"DB05",1,strFields, strValues,"amzcases",false,"");

			emit GoToCase(ui->leCaseNum->text());
		}
	}
	else emit GoToCase(ui->leCaseNum->text());
}

void wgtAMZClaims::on_pbSearch_clicked()
{
    QString strSearchString;
    if(ui->leSearch->text()!="") {
        bIsSearch=true;
        QString arg1=ui->leSearch->text();
        strSearchString="([regnum] LIKE '%"+arg1+"%' OR [complainant] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1;
        strSearchString+="%' OR [casenum] LIKE '%"+arg1+"%') AND ([executor]='";
        strSearchString+=dboper->UsrSettings.sUserName+"') ORDER BY [regdate] DESC, [regnum] DESC";

        emit setFilterMB(strSearchString);

		if(dboper->mdlDB05->rowCount() == 0) {
			ui->wgtArcNotify->setVisible(true);
			ui->lblNoMatches->setVisible(true);
		}
        else if(dboper->mdlDB05->rowCount() > 0) {
			ui->lblNoMatches->setVisible(false);
			ui->wgtArcNotify->setVisible(false);
            iRowCount=dboper->mdlDB05->rowCount();
            iCurrentRow=0;
            emit setStatusText(iCurrentRow+1, iRowCount);
            SetClaimsFormFields(iCurrentRow);
        }
    }
}

void wgtAMZClaims::on_pbAdd_clicked()
{
	wAddOutDoc->setAODVars("acl", iRowIndex);
    wAddOutDoc->setVisible(true);
}

void wgtAMZClaims::on_pbSend2Agreement_clicked()
{
	if(ui->tvDocuments->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("agreed").toBool()) {
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvDocuments->currentIndex().row(),8),true);
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvDocuments->currentIndex().row(),9),false);
		dboper->stmdlEOoutcomingDocs->submitAll();
		ui->pbSend2Agreement->setVisible(false);
		ui->pbPublish->setVisible(false);
	}
}

void wgtAMZClaims::on_pbPublish_clicked()
{

}

void wgtAMZClaims::SetClaimsFormFields(int recordnum) {
	ui->teComplainant->clear();
	ui->teDefendant->clear();
	ui->leClause->clear();
	ui->cbResult->clear();
	ui->cbExecutor->clear();
	//ui->cbNLA->clear();
    ui->wgtSearch->setVisible(bIsSearch);
	ui->cbProlong->setChecked(false);
	ui->deTill->setDate(QDate(2013,1,1));
	ui->deTill->setVisible(false);
	
	iRowIndex=dboper->mdlDB05->record(recordnum).value("id").toInt();
	ui->leRegnum->setText(dboper->mdlDB05->record(recordnum).value("regnum").toString());
	ui->deRegDate->setDate(dboper->mdlDB05->record(recordnum).value("regdate").toDate());
	ui->deHandoverDate->setDate(dboper->mdlDB05->record(recordnum).value("handoverdate").toDate());
	//ui->leExecutor->setText(dboper->mdlDB05->record(recordnum).value("executor").toString());
	ui->teComplainant->setText(dboper->mdlDB05->record(recordnum).value("complainant").toString());
	ui->teDefendant->setText(dboper->mdlDB05->record(recordnum).value("defendant").toString());
	ui->leClause->setText(dboper->mdlDB05->record(recordnum).value("clause").toString());
	ui->teSummary->setText(dboper->mdlDB05->record(recordnum).value("summary").toString());
	ui->cbProlong->setChecked(dboper->mdlDB05->record(recordnum).value("prolong").toBool());
	if(ui->cbProlong->isChecked()) ui->deTill->setDate(dboper->mdlDB05->record(recordnum).value("prolongdate").toDate());
	ui->lblTill->setVisible(ui->cbProlong->isChecked());
	ui->deTill->setVisible(ui->cbProlong->isChecked());

	if(ui->cbNLA->count()==0) ui->cbNLA->addItems(stlNLA);
	ui->cbNLA->setCurrentIndex(ui->cbNLA->findText(dboper->mdlDB05->record(recordnum).value("nla").toString(),Qt::MatchExactly));

	if(ui->cbResult->count()==0) ui->cbResult->addItems(stlDecisionTypes);
	ui->cbResult->setCurrentIndex(ui->cbResult->findText(dboper->mdlDB05->record(recordnum).value("result").toString(),Qt::MatchExactly));

	if(ui->cbExecutor->count()==0) ui->cbExecutor->addItems(stlExecutors);
	ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->mdlDB05->record(recordnum).value("executor").toString(),Qt::MatchExactly));
	if(dboper->UsrSettings.stkUserPermissions.bHandoverIncoming) ui->cbExecutor->setEnabled(true);
	else ui->cbExecutor->setEnabled(false);

	ui->leCaseNum->setText(dboper->mdlDB05->record(recordnum).value("casenum").toString());
	if(ui->leCaseNum->text()!="") ui->pbGoToCase->setEnabled(true);
	else ui->pbGoToCase->setEnabled(false);

	dboper->stmdlEOincomingDocs->setTable("incoming");
	QString strFilter;
	strFilter="[attachedto] = '";
	strFilter+=tr("Claim");
	strFilter+="' AND [attachedtodocid]=";
	QString strBuffer;
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;
	
	dboper->stmdlEOincomingDocs->setFilter(strFilter);
	dboper->stmdlEOincomingDocs->select();
	dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
	dboper->stmdlEOincomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOincomingDocs->setHeaderData(8, Qt::Horizontal, tr("Addressee"));
	dboper->stmdlEOincomingDocs->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->stmdlEOincomingDocs->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	ui->tvIncoming->setModel(dboper->stmdlEOincomingDocs);
	ui->tvIncoming->hideColumn(0);
	int i;
	for(i=3;i<8;i++) ui->tvIncoming->hideColumn(i);
	ui->tvIncoming->hideColumn(9);
	for(i=12;i<20;i++) ui->tvIncoming->hideColumn(i);

	ui->tvIncoming->resizeColumnsToContents();
	ui->tvIncoming->horizontalHeader()->setStretchLastSection(true);

    dboper->stmdlEOoutcomingDocs->setTable("documents");
    strFilter="[attachedtodoctype] = 'acl'";
    strFilter+=" AND [attachedtodocid]=";
    strBuffer.setNum(iRowIndex);
    strFilter+=strBuffer;

    dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
    dboper->stmdlEOoutcomingDocs->select();
    dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document number"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Document date"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Reg Num"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Document Type"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Post ID"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Execution date"));
	ui->tvDocuments->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(7, new DataDelegate());
    ui->tvDocuments->setModel(dboper->stmdlEOoutcomingDocs);
    ui->tvDocuments->hideColumn(0);

    for(i=8;i<16;i++) ui->tvDocuments->hideColumn(i);
    ui->tvDocuments->resizeColumnsToContents();
    ui->tvDocuments->horizontalHeader()->setStretchLastSection(true);

	ui->pbSave->setEnabled(false);
	bFormChanged=false;

	ui->teComplainant->setEnabled(!iRowCount<1);
	ui->teDefendant->setEnabled(!iRowCount<1);
	ui->leClause->setEnabled(!iRowCount<1);
	ui->cbNLA->setEnabled(!iRowCount<1);
	ui->cbResult->setEnabled(!iRowCount<1);
	ui->leCaseNum->setEnabled(!iRowCount<1);
	ui->teSummary->setEnabled(!iRowCount<1);
	ui->twIODocs->setEnabled(!iRowCount<1);
	ui->pbFirst->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbPrev->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbNext->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	ui->pbLast->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
}

void wgtAMZClaims::on_teComplainant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZClaims::on_teDefendant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZClaims::on_cbResult_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZClaims::on_leCaseNum_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
	if(ui->leCaseNum->text()!="") ui->pbGoToCase->setEnabled(true);
	else ui->pbGoToCase->setEnabled(false);
}

void wgtAMZClaims::updateACLTable() {
	dboper->stmdlEOoutcomingDocs->select();
}

void wgtAMZClaims::EditAction() {
    if(ui->tvDocuments->currentIndex().isValid()) {
		wAddOutDoc->setAODVars("acl", iRowIndex,true,dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("id").toInt());
		wAddOutDoc->setVisible(true);
	}
}

void wgtAMZClaims::DeleteAction() {
	if(ui->tvDocuments->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("agreed").toBool()) {
		QString strID;
		QString strSelectString;
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Delete document"));
		msgQuestion.setText(tr("You're going to delete selected document. Are you sure?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);

		msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
		msgQuestion.button(QMessageBox::No)->setText(tr("No"));

		QFont fntBold;
		fntBold.setFamily(ui->pbAdd->font().family());
		fntBold.setPointSize(8);
		fntBold.setBold(true);
		msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

		if(msgQuestion.exec()==QMessageBox::Yes)
			wAddOutDoc->deleteDocument(dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("id").toInt());

		dboper->stmdlEOoutcomingDocs->select();
	}
}

void wgtAMZClaims::on_tvDocuments_doubleClicked(const QModelIndex &index)
{
	EditAction();
}

void wgtAMZClaims::setClaimsDBOperLink() {
	wAddOutDoc->dboper=dboper;
}

void wgtAMZClaims::on_tvDocuments_clicked(const QModelIndex &index)
{
	if(!dboper->stmdlEOoutcomingDocs->record(index.row()).value("senttoagreement").toBool()) ui->pbSend2Agreement->setVisible(true);
	else ui->pbSend2Agreement->setVisible(false);
	
	ui->pbPublish->setVisible(false);
}

void wgtAMZClaims::on_leSearch_returnPressed()
{
	ui->pbSearch->click();
}

void wgtAMZClaims::on_pbClear_clicked()
{
    QString strFilter;
    ui->leSearch->clear();
    strFilter="[executor] = '"+dboper->UsrSettings.sUserName+"' ORDER BY [regdate] DESC, [regnum] DESC";
    
	emit setFilterMB(strFilter);

    if(dboper->mdlDB05->rowCount() > 0) {
        iRowCount=dboper->mdlDB05->rowCount();
        iCurrentRow=0;
        emit setStatusText(iCurrentRow+1, iRowCount);
        SetClaimsFormFields(iCurrentRow);
    }
	ui->lblNoMatches->setVisible(false);
}

void wgtAMZClaims::on_pbShowSearch_toggled(bool checked)
{
	if(ui->pbSave->isEnabled()) AskSaving();
    bIsSearch=checked;
	ui->wgtSearch->setVisible(checked);
    ui->leSearch->clear();
    (checked)?ui->leSearch->setFocus():ui->pbClear->click();
	ui->pbShowSearch->setChecked(checked);
	actSearch->setChecked(checked);
	ui->lblNoMatches->setVisible(false);
    ui->pbClear->setVisible(false);
}

void wgtAMZClaims::clearACLSearch() {
	bIsSearch=false;
	ui->leSearch->clear();
	ui->pbClear->setVisible(false);
	ui->pbShowSearch->setChecked(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
}

void wgtAMZClaims::on_leSearch_textChanged(const QString &arg1)
{
    if(arg1!="") ui->pbClear->setVisible(true);
    else ui->pbClear->setVisible(false);
}

void wgtAMZClaims::on_pbArc_clicked()
{
	QString strFields;
	QString strValues;
	QString strBuffer;
	bool bNotAgreed=false;
	if(dboper->stmdlEOoutcomingDocs->rowCount()>0) {
		for(int i=0;i<dboper->stmdlEOoutcomingDocs->rowCount();i++) {
			if(!dboper->stmdlEOoutcomingDocs->record(i).value("agreed").toBool()) {
				QMessageBox::warning(0, tr("Archiving is not possible"),	
					tr("One or several outcoming documents are not agreed. Archiving is not possible until all procedural documents are agreed."));
				bNotAgreed=true;
				break;
			}
		}
	}
	if(!bNotAgreed) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Archive claim"));
		msgQuestion.setText(tr("You're going to archive current claim. Any information about claim will be freezed. Do you want to continue?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) {
			strFields="[regnum],[regdate],[handoverdate],[executor],[complainant],[defendant],[clause],[nla],[summary],[prolongdate],[result],[casenum],[prolong]";
			strValues=ui->leRegnum->text() + ",#" + ui->deRegDate->date().toString("MM/dd/yyyy") + "#,#";
			strValues+=ui->deHandoverDate->date().toString("MM/dd/yyyy") + "#,'" + ui->cbExecutor->currentText() + "','";
			strValues+=ui->teComplainant->toPlainText() + "','" + ui->teDefendant->toPlainText() + "','" + ui->leClause->text();
			strValues+="','" + ui->cbNLA->currentText() + "','" + ui->teSummary->toPlainText() + "',#";
			strValues+=ui->deHandoverDate->date().toString("MM/dd/yyyy") + "#,'" + ui->cbResult->currentText() + "','";
			strValues+=ui->leCaseNum->text()+"',";
			if(ui->cbProlong->isChecked()) strValues+="1";
			else strValues+="0";
			
			dboper->dbQuery(PRIVATE_QUERY,"DB05",1,strFields,strValues,"amzclaims_arc",false,"");
			dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","amzclaims_arc",true,"[regnum]="+ui->leRegnum->text()+" AND [regdate]=#" + ui->deRegDate->date().toString("MM/dd/yyyy") + "#");
			dboper->query.next();
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedto]$[attachedtodocid]";
			strValues="'"+tr("Claims archive")+"'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"incoming",true,"[attachedto]='"+tr("Claim")+"' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedtodoctype]$[attachedtodocid]";
			strValues="'acl_a'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,"[attachedtodoctype]='acl' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","amzclaims",true,"[regnum]="+ui->leRegnum->text()+" AND [regdate]=#" + ui->deRegDate->date().toString("MM/dd/yyyy") + "#");
			dboper->mdlDB05->select();
			iRowCount=dboper->mdlDB05->rowCount();
			iCurrentRow=0;
			emit setStatusText(iCurrentRow, iRowCount);
			SetClaimsFormFields(iCurrentRow);
		}
	}
}

void wgtAMZClaims::on_leClause_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZClaims::on_cbNLA_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZClaims::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}

void wgtAMZClaims::on_cbExecutor_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZClaims::on_teSummary_textChanged()
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZClaims::on_cbProlong_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZClaims::on_deTill_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}
