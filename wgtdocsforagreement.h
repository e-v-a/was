#ifndef WGTDOCSFORAGREEMENT_H
#define WGTDOCSFORAGREEMENT_H

#include <QWidget>
#include "udatabase.h"

namespace Ui {
class wgtdocsforagreement;
}

class wgtdocsforagreement : public QWidget
{
    Q_OBJECT

public:
    explicit wgtdocsforagreement(QWidget *parent = 0);
    ~wgtdocsforagreement();
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	DBOperations *dboper;

signals:
	void setStatusText(int iCurrentRow, int iRowsCount);
	void UpdateDocsNum(int iRowsCount);
	void closeD4Awgt();

public slots:
	void SetD4AFormFields(int recordnum);
	void clearD4AFields();

private slots:
    void on_pbFirst_clicked();

    void on_pbPrev_clicked();

    void on_pbNext_clicked();

    void on_pbLast_clicked();

    void on_pbView_clicked();

    void on_pbReturn_clicked();

    void on_pbAgree_clicked();

private:
    Ui::wgtdocsforagreement *ui;
	int iNextRow;
};

#endif // WGTDOCSFORAGREEMENT_H
