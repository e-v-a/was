<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>QObject</name>
    <message>
        <location filename="udatabase.cpp" line="135"/>
        <source>Error Number: </source>
        <translation>Номер ошибки:</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="135"/>
        <source>EDMS05</source>
        <translation>СЭД05</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Unable to read settings</source>
        <translation>Невозможно прочитать файл настроек</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>An error occurred while opening the database: </source>
        <translation>Во время открытия базы произошла ошибка:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>Use local database?</source>
        <translation>Использовать локальные базы данных?</translation>
    </message>
    <message>
        <location filename="main.cpp" line="94"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="main.cpp" line="95"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="main.cpp" line="100"/>
        <location filename="main.cpp" line="191"/>
        <source>Select department&apos;s database path</source>
        <translation>Укажите базу отдела</translation>
    </message>
    <message>
        <location filename="main.cpp" line="101"/>
        <location filename="main.cpp" line="219"/>
        <source>Select governance&apos;s database path</source>
        <translation>Укажите базу Управления</translation>
    </message>
    <message>
        <location filename="main.cpp" line="105"/>
        <source>Select common directory</source>
        <translation>Выберите общую директорию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="106"/>
        <source>Select directory for publishing documents</source>
        <translation>Выберите директорию для публикации документов</translation>
    </message>
    <message>
        <location filename="main.cpp" line="107"/>
        <source>Select update directory</source>
        <translation>Выберите директорию обновлений</translation>
    </message>
    <message>
        <location filename="main.cpp" line="164"/>
        <location filename="main.cpp" line="197"/>
        <location filename="main.cpp" line="224"/>
        <location filename="main.cpp" line="230"/>
        <source>Unable to open database</source>
        <translation>Невозможно открыть базу данных</translation>
    </message>
    <message>
        <location filename="main.cpp" line="165"/>
        <location filename="main.cpp" line="198"/>
        <location filename="main.cpp" line="225"/>
        <location filename="main.cpp" line="231"/>
        <source>An error occurred while opening the connection: </source>
        <translation>Ошибка при открытии базы данных:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="188"/>
        <source>Department&apos;s database not found</source>
        <translation>База данных отдела не найдена</translation>
    </message>
    <message>
        <location filename="main.cpp" line="189"/>
        <location filename="main.cpp" line="217"/>
        <source>Please specify the right path</source>
        <translation>Пожалуйста, укажите правильный путь</translation>
    </message>
    <message>
        <location filename="main.cpp" line="216"/>
        <source>Governance&apos;s database not found</source>
        <translation>База данных Управления не найдена</translation>
    </message>
</context>
<context>
    <name>mw</name>
    <message>
        <location filename="mw.ui" line="14"/>
        <source>Workflow accounting system</source>
        <translation>Система учета рабочего процесса</translation>
    </message>
    <message>
        <location filename="mw.ui" line="84"/>
        <source>Notifications</source>
        <translation>Уведомления</translation>
    </message>
    <message>
        <location filename="mw.ui" line="122"/>
        <source>Inbox Messages</source>
        <translation>Входящие сообщения</translation>
    </message>
    <message>
        <location filename="mw.ui" line="161"/>
        <source>Calendar</source>
        <translation>Календарь</translation>
    </message>
    <message>
        <location filename="mw.ui" line="209"/>
        <source>Synchronization</source>
        <translation>Синхронизация</translation>
    </message>
    <message>
        <location filename="mw.ui" line="242"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mw.ui" line="317"/>
        <location filename="mw.ui" line="320"/>
        <location filename="mw.cpp" line="557"/>
        <location filename="mw.cpp" line="768"/>
        <source>Inbox documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="mw.ui" line="348"/>
        <source>All incoming documents</source>
        <translation>Все входящие документы</translation>
    </message>
    <message>
        <location filename="mw.ui" line="358"/>
        <location filename="mw.cpp" line="792"/>
        <location filename="mw.cpp" line="900"/>
        <location filename="mw.cpp" line="914"/>
        <source>Documents for agreement</source>
        <oldsource>Document for agreement</oldsource>
        <translation>Документы на согласование</translation>
    </message>
    <message>
        <location filename="mw.ui" line="374"/>
        <source>18.1</source>
        <translation>18.1</translation>
    </message>
    <message>
        <location filename="mw.ui" line="377"/>
        <source>Eighteen one claims</source>
        <translation>Заявления 18.1</translation>
    </message>
    <message>
        <location filename="mw.ui" line="393"/>
        <source>Claims in point</source>
        <oldsource>Claims</oldsource>
        <translation>Рассматриваемые заявления</translation>
    </message>
    <message>
        <location filename="mw.ui" line="409"/>
        <source>Cases in point</source>
        <oldsource>Cases</oldsource>
        <translation>Рассматриваемые дела</translation>
    </message>
    <message>
        <location filename="mw.ui" line="428"/>
        <source>Administrative Cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="mw.ui" line="466"/>
        <source>Lawsuits</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="mw.ui" line="507"/>
        <source>Report</source>
        <translation>Отчет</translation>
    </message>
    <message>
        <location filename="mw.ui" line="523"/>
        <source>Archive</source>
        <translation>Архив</translation>
    </message>
    <message>
        <location filename="mw.ui" line="542"/>
        <source>Admin panel</source>
        <translation>Панель администратора</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1051"/>
        <location filename="mw.cpp" line="1090"/>
        <source>expires at </source>
        <translation>истекает срок рассмотрения </translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1057"/>
        <source>(1st month)</source>
        <translation>(1й месяц)</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1053"/>
        <location filename="mw.cpp" line="1092"/>
        <source>(after prolongation)</source>
        <translation>(после продления)</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1120"/>
        <source>committee</source>
        <oldsource>committee (</oldsource>
        <translation>заседание Комиссии</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="720"/>
        <location filename="mw.cpp" line="888"/>
        <source>current row: </source>
        <translation>текущая запись: </translation>
    </message>
    <message>
        <location filename="mw.cpp" line="722"/>
        <location filename="mw.cpp" line="890"/>
        <source>rows count: </source>
        <translation>всего записей: </translation>
    </message>
    <message>
        <location filename="mw.cpp" line="433"/>
        <source>User name</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="434"/>
        <source>Second name</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="435"/>
        <source>First name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="436"/>
        <source>Third name</source>
        <translation>Отчество</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="437"/>
        <source>Password hash</source>
        <translation>Хэш пароля</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="438"/>
        <source>Position</source>
        <translation>Должность</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="439"/>
        <source>Permissions</source>
        <oldsource>Permission</oldsource>
        <translation>Разрешения</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="442"/>
        <location filename="mw.cpp" line="1269"/>
        <source>Executor</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1096"/>
        <source>(3d month)</source>
        <translation>(3й месяц)</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1146"/>
        <source>judge:</source>
        <translation>судья:</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1195"/>
        <location filename="mw.cpp" line="1224"/>
        <source>Mon</source>
        <translation>Пн</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1198"/>
        <location filename="mw.cpp" line="1227"/>
        <source>Tue</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1201"/>
        <location filename="mw.cpp" line="1230"/>
        <source>Wed</source>
        <translation>Ср</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1204"/>
        <location filename="mw.cpp" line="1233"/>
        <source>Thu</source>
        <translation>Чт</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1207"/>
        <location filename="mw.cpp" line="1236"/>
        <source>Fri</source>
        <translation>Пт</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1210"/>
        <location filename="mw.cpp" line="1239"/>
        <source>Sat</source>
        <translation>Сб</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1213"/>
        <location filename="mw.cpp" line="1242"/>
        <source>Sun</source>
        <translation>Вс</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1277"/>
        <source>There&apos;s no sheduled events</source>
        <translation>Нет запланированных мероприятий</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1282"/>
        <source>Schedule</source>
        <translation>Расписание</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1333"/>
        <source>Period</source>
        <translation>Период</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1334"/>
        <source>Total new incoming documents</source>
        <oldsource>Total incoming documents</oldsource>
        <translation>Всего новых входящих документов</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1339"/>
        <source>Total new claims</source>
        <oldsource>Total claims</oldsource>
        <translation>Всего новых заявлений</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1344"/>
        <source>Total closed claims</source>
        <translation>Всего закрыто заявлений</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1345"/>
        <source>Reject</source>
        <translation>Отказ</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1345"/>
        <source>Redirect</source>
        <translation>Перенаправление</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1349"/>
        <source>Total cases</source>
        <translation>Всего дел</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1354"/>
        <source>Total closed cases</source>
        <translation>Всего закрытых дел</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1355"/>
        <source>Decision</source>
        <translation>Решение</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1360"/>
        <source>Total orders</source>
        <translation>Всего предписаний</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1361"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="445"/>
        <source>Attachment type</source>
        <translation>Тип вложения</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="464"/>
        <location filename="mw.cpp" line="520"/>
        <location filename="mw.cpp" line="521"/>
        <location filename="mw.cpp" line="532"/>
        <source>EntryDate</source>
        <translation>ДатаВнесИнф</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="465"/>
        <location filename="mw.cpp" line="507"/>
        <location filename="mw.cpp" line="516"/>
        <source>RegNum</source>
        <translation>ВходНомН</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="466"/>
        <location filename="mw.cpp" line="488"/>
        <location filename="mw.cpp" line="509"/>
        <location filename="mw.cpp" line="517"/>
        <source>RegDate</source>
        <translation>ВходДата</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="467"/>
        <location filename="mw.cpp" line="518"/>
        <source>CitizenAppeal</source>
        <translation>ОбращениеГраждан</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="468"/>
        <location filename="mw.cpp" line="522"/>
        <source>SendWay</source>
        <translation>СпособОтправкаПолуч</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="469"/>
        <location filename="mw.cpp" line="523"/>
        <source>OutcomeNum</source>
        <translation>НомПисьмаИсх</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="470"/>
        <location filename="mw.cpp" line="524"/>
        <location filename="mw.cpp" line="525"/>
        <location filename="mw.cpp" line="535"/>
        <source>OutcomeDate</source>
        <translation>Дата ПисьмаИсх</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="471"/>
        <location filename="mw.cpp" line="526"/>
        <source>Addressee</source>
        <translation>НазваниеАдресата</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="472"/>
        <location filename="mw.cpp" line="527"/>
        <source>PostIndex</source>
        <translation>Индекс</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="473"/>
        <location filename="mw.cpp" line="527"/>
        <source>Region</source>
        <translation>Регион</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="474"/>
        <location filename="mw.cpp" line="527"/>
        <source>Address</source>
        <translation>АдресВход</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="475"/>
        <location filename="mw.cpp" line="528"/>
        <source>DocType</source>
        <oldsource>SEDDocType</oldsource>
        <translation>ВидДок</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="476"/>
        <location filename="mw.cpp" line="529"/>
        <source>Summary</source>
        <translation>КраткоеСодержание</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="477"/>
        <location filename="mw.cpp" line="488"/>
        <source>DepNum1</source>
        <translation>НомОтдела1</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="478"/>
        <location filename="mw.cpp" line="488"/>
        <source>DepNum2</source>
        <translation>НомОтдела2</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="479"/>
        <location filename="mw.cpp" line="488"/>
        <source>DepNum3</source>
        <translation>НомОтдела3</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="498"/>
        <source>New incoming documents processing...</source>
        <translation>Обработка новых входящих документов...</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="547"/>
        <location filename="mw.cpp" line="592"/>
        <source>Wrong department</source>
        <translation>Попало в отдел ошибочно</translation>
    </message>
    <message>
        <source>IncomingList</source>
        <translation type="obsolete">СписокВходящих</translation>
    </message>
    <message>
        <source>FAS, prosecutor&apos;s office&apos;s letter</source>
        <translation type="obsolete">Письмо ФАС, прокуратуры</translation>
    </message>
</context>
<context>
    <name>wgtAMZCases</name>
    <message>
        <location filename="wgtamzcases.ui" line="14"/>
        <location filename="wgtamzcases.ui" line="911"/>
        <source>Cases</source>
        <oldsource>AML Cases</oldsource>
        <translation>Дела</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="191"/>
        <source>Case №:</source>
        <oldsource>№ дела:</oldsource>
        <translation>№ дела:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="121"/>
        <source>Handover date:</source>
        <translation>Дата росписи:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="231"/>
        <source>Claim №:</source>
        <translation>№ заявления:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="327"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="131"/>
        <source>Decree date:</source>
        <translation>Дата приказа:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="983"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="973"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1031"/>
        <source>Outcoming documents</source>
        <oldsource>Outcoming Documents</oldsource>
        <translation>Исходящие документы</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1163"/>
        <source>Submission date:</source>
        <translation>Дата представления документов:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1173"/>
        <source>Committee date:</source>
        <translation>Дата комиссии:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="771"/>
        <source>Decision information</source>
        <translation>Информация о решении</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="777"/>
        <source>Result:</source>
        <translation>Результат:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1062"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1082"/>
        <source>Send to agreement</source>
        <translation>Отправить на согласование</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1102"/>
        <source>Publish</source>
        <translation>Опубликовать</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1011"/>
        <source>Incoming documents</source>
        <oldsource>Incoming Documents</oldsource>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="82"/>
        <source>Case information</source>
        <translation>Информация о деле</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="317"/>
        <source>NLA:</source>
        <translation>НПА:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="440"/>
        <location filename="wgtamzcases.ui" line="493"/>
        <location filename="wgtamzcases.cpp" line="20"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="548"/>
        <source>Delete case</source>
        <translation>Удалить дело</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="596"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="631"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="666"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="701"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="736"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="810"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="817"/>
        <source>Order executed</source>
        <translation>Предписание исполнено</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="824"/>
        <source>Clauses:</source>
        <translation>Статьи:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="857"/>
        <source>Execution info:</source>
        <translation>Информация об исполнении:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1246"/>
        <source>dd.MM.yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1284"/>
        <source>Excitation date:</source>
        <translation>Дата возбуждения:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1143"/>
        <source>Summary:</source>
        <translation>Краткое содержание:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1277"/>
        <source>Prolong</source>
        <translation>Продление</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1153"/>
        <source>Till:</source>
        <translation>Продлено до:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="53"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="949"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="398"/>
        <source>Clear search string</source>
        <translation>Очистить строку поиска</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="274"/>
        <source>Clause:</source>
        <translation>Статья:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="284"/>
        <source>Decree №:</source>
        <translation>№ приказа:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="141"/>
        <source>Claim date:</source>
        <translation>Дата заявления:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="18"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="19"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="86"/>
        <source>You&apos;re going to delete the case. This will cause the removal of all attached documents. Are you sure?</source>
        <translation>Вы собираетесь удалить дело. Это повлечет удаление всех прикрепленных документов. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="694"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="695"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="306"/>
        <location filename="wgtamzcases.cpp" line="638"/>
        <source>Case</source>
        <oldsource>AMZ Case</oldsource>
        <translation>Дело</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="314"/>
        <source>Reg N</source>
        <translation>Вх.N</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="315"/>
        <location filename="wgtamzcases.cpp" line="340"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="316"/>
        <source>Addressee</source>
        <translation>Название адресата</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="317"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="318"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="337"/>
        <source>Document number</source>
        <translation>Номер документа</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="338"/>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="339"/>
        <source>Reg Num</source>
        <translation>Исх.№</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="341"/>
        <source>Document Type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="342"/>
        <source>Post ID</source>
        <translation>Почтовый идентификатор</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="343"/>
        <source>Execution date</source>
        <translation>Дата исполнения</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="471"/>
        <source>Delete document</source>
        <oldsource>Delete row</oldsource>
        <translation>Удалить документ</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="472"/>
        <source>You&apos;re going to delete selected document. Are you sure?</source>
        <oldsource>You&apos;re going to delete selected row. Are you sure?</oldsource>
        <translation>Вы собираетесь удалить выбранный документ. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="515"/>
        <location filename="wgtamzcases.cpp" line="578"/>
        <source>Award</source>
        <translation>Решение</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="515"/>
        <location filename="wgtamzcases.cpp" line="578"/>
        <source>Injunction</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="515"/>
        <location filename="wgtamzcases.cpp" line="578"/>
        <source>Decision</source>
        <translation>Определение</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="579"/>
        <location filename="wgtamzcases.cpp" line="586"/>
        <source>Archiving is not possible</source>
        <translation>Архивация невозможна</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="580"/>
        <location filename="wgtamzcases.cpp" line="587"/>
        <source>One or several outcoming documents are not agreed or not published. Archiving is not possible until all procedural documents are agreed and published.</source>
        <translation>Один или несколько исходящих документов не согласованы или не опубликованы. Архивация невозможна пока все процессуальные документы не будут согласованы и опубликованы.</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="599"/>
        <source>Archive claim</source>
        <translation>Архивировать жалобу</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="600"/>
        <source>You&apos;re going to archive current claim. Any information about claim will be freezed. Do you want to continue?</source>
        <translation>Вы собираетесь архивировать текущую жалобу. Вся информация по этой жалобе будет заморожена. Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="637"/>
        <source>Cases archive</source>
        <translation>Архив дел</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="102"/>
        <location filename="wgtamzcases.cpp" line="487"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="103"/>
        <location filename="wgtamzcases.cpp" line="488"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>wgtAMZClaims</name>
    <message>
        <location filename="wgtamzclaims.ui" line="14"/>
        <location filename="wgtamzclaims.ui" line="1018"/>
        <source>Claims</source>
        <oldsource>AML Claims</oldsource>
        <translation>Заявления</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="840"/>
        <source>Registration №:</source>
        <translation>Вх.№:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="850"/>
        <source>Handover date:</source>
        <translation>Дата росписи:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="939"/>
        <source>Registration date:</source>
        <translation>Дата регистрации:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="860"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="33"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="20"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="780"/>
        <source>Clause:</source>
        <translation>Статья:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="76"/>
        <source>Result:</source>
        <translation>Результат:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="102"/>
        <source>Case №:</source>
        <oldsource>№ дела:</oldsource>
        <translation>№ дела:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="126"/>
        <source>Incoming documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="146"/>
        <source>Outcoming documents</source>
        <translation>Исходящие документы</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="171"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="191"/>
        <source>Send to agreement</source>
        <translation>Отправить на согласование</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="211"/>
        <source>Publish</source>
        <translation>Опубликовать</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="949"/>
        <source>Till:</source>
        <translation>Продлено до:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="621"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="1056"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="959"/>
        <source>Prolong</source>
        <translation>Продление</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="307"/>
        <location filename="wgtamzclaims.ui" line="753"/>
        <location filename="wgtamzclaims.cpp" line="20"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="365"/>
        <source>Open case</source>
        <translation>Перейти к делу</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="419"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="454"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="489"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="524"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="559"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="705"/>
        <source>Clear search string</source>
        <translation>Очистить строку поиска</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="239"/>
        <source>NLA:</source>
        <translation>НПА:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="56"/>
        <source>Summary:</source>
        <translation>Краткое содержание:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="18"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="19"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="570"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="571"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="137"/>
        <source>There&apos;ll be created a new case &apos;</source>
        <oldsource>There&apos;ll be created a new case �</oldsource>
        <translation>Будет создано новое дело &apos;</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="137"/>
        <source>Do you want to continue?</source>
        <translation>Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="97"/>
        <location filename="wgtamzclaims.cpp" line="285"/>
        <location filename="wgtamzclaims.cpp" line="537"/>
        <source>Claim</source>
        <oldsource>AMZ Claim</oldsource>
        <translation>Заявление</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="293"/>
        <source>Reg N</source>
        <translation>Вх.N</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="294"/>
        <location filename="wgtamzclaims.cpp" line="319"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="295"/>
        <source>Addressee</source>
        <translation>Название адресата</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="296"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="297"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="316"/>
        <source>Document number</source>
        <translation>Номер документа</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="317"/>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="318"/>
        <source>Reg Num</source>
        <translation>Исх.№</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="320"/>
        <source>Document Type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="321"/>
        <source>Post ID</source>
        <translation>Почтовый идентификатор</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="322"/>
        <source>Execution date</source>
        <translation>Дата исполнения</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="396"/>
        <source>Delete document</source>
        <oldsource>Delete row</oldsource>
        <translation>Удалить документ</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="397"/>
        <source>You&apos;re going to delete selected document. Are you sure?</source>
        <oldsource>You&apos;re going to delete selected row. Are you sure?</oldsource>
        <translation>Вы собираетесь удалить выбранный документ. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="504"/>
        <source>One or several outcoming documents are not agreed. Archiving is not possible until all procedural documents are agreed.</source>
        <translation>Один или несколько исходящих документов не согласован. Архивация невозможна пока все процессуальные документы не будут согласованы.</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="503"/>
        <source>Archiving is not possible</source>
        <translation>Архивация невозможна</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="516"/>
        <source>Archive claim</source>
        <translation>Архивировать жалобу</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="517"/>
        <source>You&apos;re going to archive current claim. Any information about claim will be freezed. Do you want to continue?</source>
        <translation>Вы собираетесь архивировать текущую жалобу. Вся информация по этой жалобе будет заморожена. Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="536"/>
        <source>Claims archive</source>
        <oldsource>claims archive</oldsource>
        <translation>Архив заявлений</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="153"/>
        <location filename="wgtamzclaims.cpp" line="411"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="154"/>
        <location filename="wgtamzclaims.cpp" line="412"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>wgtAdmManagement</name>
    <message>
        <location filename="wgtadmmanagement.ui" line="14"/>
        <source>Administrator panel</source>
        <translation>Панель администратора</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="20"/>
        <source>Users</source>
        <oldsource>New user</oldsource>
        <translation>Пользователи</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="48"/>
        <source>Add new user</source>
        <translation>Добавить нового пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="61"/>
        <source>Edit user</source>
        <translation>Редактировать пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="74"/>
        <source>Flush user password</source>
        <translation>Сбросить пароль пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="87"/>
        <source>Delete user</source>
        <translation>Удалить пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="110"/>
        <source>Executors</source>
        <translation>Исполнители</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="138"/>
        <source>Add new executor</source>
        <translation>Добавить нового исполнителя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="151"/>
        <source>Delete executor</source>
        <translation>Удалить исполнителя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="174"/>
        <source>Lists of variables</source>
        <translation>Список переменных</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="184"/>
        <source>Types of attached documents</source>
        <oldsource>Attached to type</oldsource>
        <translation>Типы прикрепленных документов</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="210"/>
        <source>Decisions</source>
        <translation>Решения</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="233"/>
        <source>Outcoming documents types</source>
        <oldsource>External documents types</oldsource>
        <translation>Типы исходящих документов</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="256"/>
        <source>Instances</source>
        <translation>Инстанции</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="279"/>
        <location filename="wgtadmmanagement.cpp" line="364"/>
        <source>NLA</source>
        <translation>НПА</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="302"/>
        <source>Incoming documents types</source>
        <oldsource>Internal documents types</oldsource>
        <translation>Типы входящих документов</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="334"/>
        <source>Add item</source>
        <translation>Добавить элемент</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="347"/>
        <source>Edit item</source>
        <translation>Редактировать элемент</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="360"/>
        <source>Delete item</source>
        <translation>Удалить элемент</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="61"/>
        <source>Are you sure want to flush user&apos;s password? After that operation user would have to enter a new password in the next login&apos;</source>
        <translation>Вы действительно хотите сбросить пароль пользователя? После этой операции пользователю нужно будет ввести новый пароль при следующем входе</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="77"/>
        <location filename="wgtadmmanagement.cpp" line="116"/>
        <location filename="wgtadmmanagement.cpp" line="168"/>
        <location filename="wgtadmmanagement.cpp" line="290"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="78"/>
        <location filename="wgtadmmanagement.cpp" line="117"/>
        <location filename="wgtadmmanagement.cpp" line="169"/>
        <location filename="wgtadmmanagement.cpp" line="291"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="152"/>
        <source>Are you sure want to delete executor? This is irreversible operation!</source>
        <oldsource>Are you sure want to delete user? This is irreversible operation!&apos;</oldsource>
        <translation>Вы действительно хотите удалить исполнителя? Это необратимая операция!</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="135"/>
        <source>Enter executor:</source>
        <translation>Введите исполнителя:</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="100"/>
        <source>Are you sure want to delete user? This is irreversible operation!</source>
        <translation>Вы действительно хотите удалить пользователя? Это необратимая операция!</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="201"/>
        <location filename="wgtadmmanagement.cpp" line="226"/>
        <source>Enter document type:</source>
        <translation>Введите тип документа:</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="202"/>
        <location filename="wgtadmmanagement.cpp" line="227"/>
        <source>Enter instance:</source>
        <translation>Введите инстанцию:</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="274"/>
        <source>Are you sure want to delete selected item? This is irreversible operation!</source>
        <oldsource>Are you sure want to delete selected item? This is irreversible operation!&apos;</oldsource>
        <translation>Вы действительно хотите удалить выбранную строку? Это необратимая операция!</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="323"/>
        <source>Attachment type</source>
        <translation>Тип вложения</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="333"/>
        <source>Decision type</source>
        <translation>Тип решения</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="334"/>
        <location filename="wgtadmmanagement.cpp" line="345"/>
        <location filename="wgtadmmanagement.cpp" line="365"/>
        <source>Using form</source>
        <translation>Использующая форма</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="344"/>
        <location filename="wgtadmmanagement.cpp" line="374"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="354"/>
        <source>Instance</source>
        <translation>Инстанция</translation>
    </message>
</context>
<context>
    <name>wgtAdministrativeCases</name>
    <message>
        <location filename="wgtadministrativecases.ui" line="14"/>
        <location filename="wgtadministrativecases.ui" line="51"/>
        <source>Administrative cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="236"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="83"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="259"/>
        <source>Case info</source>
        <translation>Информация о деле</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="537"/>
        <source>Date of judgement:</source>
        <translation>Дата вынесения постановления:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="307"/>
        <source>Case number:</source>
        <translation>Номер дела:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="297"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="547"/>
        <source>Clause:</source>
        <translation>Статья:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="327"/>
        <source>Amount of penalty:</source>
        <translation>Сумма штрафа:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="402"/>
        <source>Defendant&apos;s address:</source>
        <translation>Адрес ответчика:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="435"/>
        <source>Excitation date:</source>
        <translation>Дата возбуждения:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="317"/>
        <source>Decision:</source>
        <translation>Решение:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="445"/>
        <source>Prolong</source>
        <translation>Продление</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="501"/>
        <source>Clause Part:</source>
        <oldsource>Clause Part</oldsource>
        <translation>Часть:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="575"/>
        <source>Till:</source>
        <translation>Продлено до:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="592"/>
        <source>Incoming documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="625"/>
        <source>Outcoming documents</source>
        <translation>Исходящие документы</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="650"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="663"/>
        <source>Send to agreement</source>
        <translation>Отправить на согласование</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="720"/>
        <location filename="wgtadministrativecases.cpp" line="19"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="772"/>
        <location filename="wgtadministrativecases.cpp" line="18"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="817"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="849"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="881"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="913"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="945"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="17"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="165"/>
        <source>You&apos;re going to delete the case. This will cause the removal of all attached documents. Are you sure?</source>
        <translation>Вы собираетесь удалить дело. Это повлечет удаление всех прикрепленных документов. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="181"/>
        <location filename="wgtadministrativecases.cpp" line="461"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="182"/>
        <location filename="wgtadministrativecases.cpp" line="462"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="605"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="606"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="320"/>
        <location filename="wgtadministrativecases.cpp" line="342"/>
        <location filename="wgtadministrativecases.cpp" line="578"/>
        <source>Administrative case</source>
        <translation>Административное дело</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="350"/>
        <source>Reg N</source>
        <translation>Вх.№</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="351"/>
        <location filename="wgtadministrativecases.cpp" line="376"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="352"/>
        <source>Addressee</source>
        <translation>Адресат</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="353"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="354"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="373"/>
        <source>Document number</source>
        <translation>Номер документа</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="374"/>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="375"/>
        <source>Reg Num</source>
        <translation>Исх.№</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="377"/>
        <source>Document Type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="378"/>
        <source>Post ID</source>
        <translation>Почтовый идентификатор</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="379"/>
        <source>Execution date</source>
        <translation>Дата исполнения</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="445"/>
        <source>Delete document</source>
        <translation>Удалить документ</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="446"/>
        <source>You&apos;re going to delete selected document. Are you sure?</source>
        <translation>Вы собираетесь удалить выбранный документ. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="507"/>
        <source>New administrative case</source>
        <translation>Новое административное дело</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="544"/>
        <source>Archiving is not possible</source>
        <translation>Архивация невозможна</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="545"/>
        <source>One or several outcoming documents are not agreed. Archiving is not possible until all procedural documents are agreed.</source>
        <oldsource>One or several outcoming documents are not agreed or not published. Archiving is not possible until all procedural documents are agreed and published.</oldsource>
        <translation>Один или несколько исходящих документов не согласованы. Архивация невозможна пока все процессуальные документы не будут согласованы.</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="557"/>
        <source>Archive case</source>
        <translation>Архивировать дело</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="558"/>
        <source>You&apos;re going to archive current case. Any information about case will be freezed. Do you want to continue?</source>
        <oldsource>You&apos;re going to archive current case. Any information about claim will be freezed. Do you want to continue?</oldsource>
        <translation>Вы собираетесь архивировать текущее дело. Вся информация о деле будет заморожена. Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="577"/>
        <source>Administrative cases archive</source>
        <translation>Архив административных дел</translation>
    </message>
</context>
<context>
    <name>wgtAuthorize</name>
    <message>
        <location filename="wgtauthorize.ui" line="32"/>
        <source>Authorization</source>
        <translation>Авторизация</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="67"/>
        <source>Reenter password:</source>
        <translation>Повторите пароль:</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="77"/>
        <source>Username:</source>
        <translation>Имя пользователя:</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="97"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="136"/>
        <source>Wrong password or username</source>
        <translation>Неверное имя пользователя или пароль</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="187"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtChangeConnection</name>
    <message>
        <location filename="wgtchangeconnection.ui" line="14"/>
        <source>Change connection</source>
        <translation>Изменить подключение</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="20"/>
        <source>Department&apos;s database:</source>
        <translation>База данных отдела:</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="46"/>
        <location filename="wgtchangeconnection.ui" line="255"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="56"/>
        <location filename="wgtchangeconnection.ui" line="245"/>
        <source>Login:</source>
        <translation>Логин:</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="78"/>
        <location filename="wgtchangeconnection.ui" line="222"/>
        <source>Create DB</source>
        <translation>Создать БД</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="117"/>
        <source>Office database:</source>
        <translation>База данных Управления:</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="143"/>
        <source>Server address:</source>
        <translation>Адрес сервера:</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="181"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="200"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="wgtchangeconnection.ui" line="306"/>
        <location filename="wgtchangeconnection.ui" line="341"/>
        <source>Check connection</source>
        <translation>Проверить подключение</translation>
    </message>
</context>
<context>
    <name>wgtEighteenOne</name>
    <message>
        <location filename="wgteighteenone.ui" line="14"/>
        <source>18.1</source>
        <translation>18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="808"/>
        <source>Handover date:</source>
        <translation>Дата росписи:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="851"/>
        <source>Registration date:</source>
        <translation>Дата регистрации:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="512"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="884"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="596"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="874"/>
        <source>Notification date and №:</source>
        <translation>Дата и номер уведомления:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="559"/>
        <source>Legal act:</source>
        <translation>НПА:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="961"/>
        <source>Execution info:</source>
        <translation>Информация об исполнении:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="954"/>
        <source>Order executed</source>
        <translation>Предписание исполнено</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="616"/>
        <source>Incoming documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="636"/>
        <source>Outcoming documents</source>
        <translation>Исходящие документы</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="974"/>
        <source>Submission date:</source>
        <translation>Дата представления документов:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="742"/>
        <source>Committee date:</source>
        <oldsource>Committee date::</oldsource>
        <translation>Дата комиссии:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="465"/>
        <source>Result:</source>
        <translation>Результат:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="661"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="681"/>
        <source>Send to agreement</source>
        <translation>Отправить на согласование</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="701"/>
        <source>Publish</source>
        <translation>Опубликовать</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="65"/>
        <location filename="wgteighteenone.ui" line="445"/>
        <location filename="wgteighteenone.cpp" line="25"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="117"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="152"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="187"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="222"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="257"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="397"/>
        <source>Clear search string</source>
        <translation>Очистить строку поиска</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="897"/>
        <source>Decision number:</source>
        <translation>Номер решения:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="944"/>
        <source>Decision date:</source>
        <translation>Дата решения:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="1048"/>
        <source>Eighteen one claims</source>
        <translation>Заявления 18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="313"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="1086"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="752"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="772"/>
        <source>Registration №:</source>
        <translation>Вх.№:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="23"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="24"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="117"/>
        <source>Eighteen one claim</source>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="125"/>
        <source>Reg N</source>
        <translation>Номер регистрации</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="126"/>
        <location filename="wgteighteenone.cpp" line="152"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="127"/>
        <source>Addressee</source>
        <translation>Название адресата</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="128"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="129"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="149"/>
        <source>Document number</source>
        <translation>Номер документа</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="150"/>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="151"/>
        <source>Reg Num</source>
        <translation>Исх.№</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="153"/>
        <source>Document Type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="154"/>
        <source>Post ID</source>
        <translation>Почтовый идентификатор</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="155"/>
        <source>Execution date</source>
        <translation>Дата исполнения</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="241"/>
        <source>Eighteenone claim</source>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <source>Save row</source>
        <translation type="obsolete">Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="634"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="464"/>
        <source>Delete document</source>
        <oldsource>Delete row</oldsource>
        <translation>Удалить документ</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="466"/>
        <source>You&apos;re going to delete selected document. Are you sure?</source>
        <oldsource>You&apos;re going to delete selected row. Are you sure?</oldsource>
        <translation>Вы собираетесь удалить выбранный документ. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="482"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="483"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="506"/>
        <location filename="wgteighteenone.cpp" line="567"/>
        <source>Award</source>
        <translation>Решение</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="506"/>
        <location filename="wgteighteenone.cpp" line="567"/>
        <source>Injunction</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="506"/>
        <location filename="wgteighteenone.cpp" line="567"/>
        <source>Notification</source>
        <translation>Уведомление</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="568"/>
        <location filename="wgteighteenone.cpp" line="575"/>
        <source>Archiving is not possible</source>
        <translation>Архивация невозможна</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="569"/>
        <location filename="wgteighteenone.cpp" line="576"/>
        <source>One or several outcoming documents are not agreed or not published. Archiving is not possible until all procedural documents are agreed and published.</source>
        <translation>Один или несколько исходящих документов не согласованы или не опубликованы. Архивация невозможна пока все процессуальные документы не будут согласованы и опубликованы.</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="588"/>
        <source>Archive claim</source>
        <translation>Архивировать жалобу</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="589"/>
        <source>You&apos;re going to archive current claim. Any information about claim will be freezed. Do you want to continue?</source>
        <translation>Вы собираетесь архивировать текущую жалобу. Вся информация по этой жалобе будет заморожена. Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="611"/>
        <source>eighteenone archive</source>
        <translation>Архив 18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="612"/>
        <source>Eighteenone Claim</source>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="94"/>
        <location filename="wgteighteenone.cpp" line="359"/>
        <source>DECISION</source>
        <oldsource>Decision</oldsource>
        <translation>РЕШЕНИЕ</translation>
    </message>
</context>
<context>
    <name>wgtGeneratedReport</name>
    <message>
        <location filename="wgtgeneratedreport.ui" line="14"/>
        <location filename="wgtgeneratedreport.ui" line="51"/>
        <source>View report</source>
        <translation>Просмотр отчета</translation>
    </message>
    <message>
        <location filename="wgtgeneratedreport.ui" line="105"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>wgtInboxClaims</name>
    <message>
        <location filename="wgtinboxclaims.ui" line="14"/>
        <location filename="wgtinboxclaims.ui" line="27"/>
        <source>Incoming documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="99"/>
        <source>Clear search string</source>
        <translation>Очистить строку поиска</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="150"/>
        <location filename="wgtinboxclaims.ui" line="706"/>
        <location filename="wgtinboxclaims.cpp" line="19"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="167"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="174"/>
        <source>Show their not attached documents</source>
        <translation>Показать свои не прикрепленные документы</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="181"/>
        <source>Document information</source>
        <translation>Информация о документе</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="187"/>
        <source>Registration №:</source>
        <translation>Вх.№:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="226"/>
        <source>Outcome №:</source>
        <translation>Исх.№:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="252"/>
        <source>Registration date:</source>
        <translation>Дата регистрации:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="308"/>
        <source>Outcome date:</source>
        <translation>Исх.дата:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="351"/>
        <source>Addressee:</source>
        <translation>Адресат:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="377"/>
        <source>Address:</source>
        <translation>Адрес:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="403"/>
        <source>Send way:</source>
        <translation>Способ отправки:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="442"/>
        <location filename="wgtinboxclaims.ui" line="573"/>
        <source>Document type:</source>
        <translation>Тип документа:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="474"/>
        <source>Citizen appeal</source>
        <translation>Обращения граждан</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="481"/>
        <source>Summary:</source>
        <translation>Краткое содержание:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="504"/>
        <source>Document settings</source>
        <translation>Настройки документа</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="510"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="533"/>
        <source>Handover date:</source>
        <translation>Дата росписи:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="612"/>
        <source>Attach to existed document or case</source>
        <translation>Прикрепить к существующему документу или делу</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="619"/>
        <source>Attached to:</source>
        <translation>Прикреплен к:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="629"/>
        <source>Delete attachment</source>
        <translation>Удалить связь</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="636"/>
        <location filename="wgtinboxclaims.cpp" line="92"/>
        <location filename="wgtinboxclaims.cpp" line="309"/>
        <source>Add attachment</source>
        <translation>Прикрепить</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="660"/>
        <source>Handover to department:</source>
        <translation>Передано в отдел:</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="91"/>
        <location filename="wgtinboxclaims.cpp" line="375"/>
        <source>Change attachment</source>
        <translation>Изменить связь</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="761"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="796"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="831"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="866"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.ui" line="901"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="94"/>
        <location filename="wgtinboxclaims.cpp" line="321"/>
        <location filename="wgtinboxclaims.cpp" line="422"/>
        <location filename="wgtinboxclaims.cpp" line="444"/>
        <location filename="wgtinboxclaims.cpp" line="485"/>
        <source>Wrong department</source>
        <translation>Попало в отдел ошибочно</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="136"/>
        <location filename="wgtinboxclaims.cpp" line="157"/>
        <location filename="wgtinboxclaims.cpp" line="273"/>
        <location filename="wgtinboxclaims.cpp" line="293"/>
        <location filename="wgtinboxclaims.cpp" line="394"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="137"/>
        <location filename="wgtinboxclaims.cpp" line="158"/>
        <location filename="wgtinboxclaims.cpp" line="274"/>
        <location filename="wgtinboxclaims.cpp" line="294"/>
        <location filename="wgtinboxclaims.cpp" line="395"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <source>A new document %1 %2 is reffered to you</source>
        <oldsource>A new document %1 %2 reffered</oldsource>
        <translation type="obsolete">Вам расписан новый документ %1 %2</translation>
    </message>
    <message>
        <source> from </source>
        <translation type="obsolete"> от </translation>
    </message>
    <message>
        <source>register number</source>
        <translation type="obsolete">вх.№</translation>
    </message>
    <message>
        <source>New document reffered</source>
        <translation type="obsolete">Расписан новый документ</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="178"/>
        <location filename="wgtinboxclaims.cpp" line="350"/>
        <location filename="wgtinboxclaims.cpp" line="360"/>
        <source>Eighteen one claim</source>
        <oldsource>Eighteen one Claim</oldsource>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="196"/>
        <source>Eighteenone claim</source>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="198"/>
        <location filename="wgtinboxclaims.cpp" line="216"/>
        <location filename="wgtinboxclaims.cpp" line="351"/>
        <location filename="wgtinboxclaims.cpp" line="361"/>
        <source>Claim</source>
        <oldsource>AMZ Claim</oldsource>
        <translation>Заявление</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="352"/>
        <location filename="wgtinboxclaims.cpp" line="362"/>
        <source>Case</source>
        <oldsource>AMZ Case</oldsource>
        <translation>Дело</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="353"/>
        <location filename="wgtinboxclaims.cpp" line="363"/>
        <source>Administrative case</source>
        <translation>Административное дело</translation>
    </message>
    <message>
        <location filename="wgtinboxclaims.cpp" line="354"/>
        <location filename="wgtinboxclaims.cpp" line="364"/>
        <source>Lawsuite</source>
        <translation>Судебное дело</translation>
    </message>
</context>
<context>
    <name>wgtLawsuites</name>
    <message>
        <location filename="wgtlawsuites.ui" line="14"/>
        <location filename="wgtlawsuites.ui" line="51"/>
        <location filename="wgtlawsuites.cpp" line="420"/>
        <source>Lawsuites</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="153"/>
        <source>Clear search string</source>
        <translation>Очистить строку поиска</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="201"/>
        <location filename="wgtlawsuites.ui" line="826"/>
        <location filename="wgtlawsuites.cpp" line="19"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="236"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="83"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="272"/>
        <source>Case info</source>
        <translation>Информация о деле</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="544"/>
        <source>Third persons:</source>
        <translation>Третьи лица:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="492"/>
        <source>Judge:</source>
        <translation>Судья:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="469"/>
        <source>Trial date/time:</source>
        <translation>Дата/время заседания:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="327"/>
        <source>Result:</source>
        <translation>Результат:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="518"/>
        <source>Other persons:</source>
        <translation>Иные лица:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="570"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="412"/>
        <source>Case card:</source>
        <translation>Карточка дела:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="665"/>
        <source>Case N:</source>
        <translation>Номер дела:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="596"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="388"/>
        <source>Add link</source>
        <translation>Добавить ссылку</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="362"/>
        <source>Remove link</source>
        <translation>Удалить ссылку</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="432"/>
        <source>Court address:</source>
        <translation>Адрес суда:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="639"/>
        <source>Claim date:</source>
        <translation>Дата заявления:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="301"/>
        <source>Instance:</source>
        <translation>Инстанция:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="721"/>
        <source>Incoming documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="731"/>
        <source>Outcoming documents</source>
        <translation>Исходящие документы</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="743"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="756"/>
        <source>Send to agreement</source>
        <translation>Отправить на согласование</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="862"/>
        <location filename="wgtlawsuites.cpp" line="18"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="907"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="939"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="971"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="1003"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="1035"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="17"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="213"/>
        <source>Add link to case card</source>
        <translation>Добавить ссылку на карточку дела</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="213"/>
        <source>Paste URL from kad.arbitr.ru</source>
        <translation>Вставьте адрес (URL) с kad.arbitr.ru</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="269"/>
        <source>You&apos;re going to delete the case. This will cause the removal of all attached documents. Are you sure?</source>
        <translation>Вы собираетесь удалить дело. Это повлечет удаление всех прикрепленных документов. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="285"/>
        <location filename="wgtlawsuites.cpp" line="638"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="286"/>
        <location filename="wgtlawsuites.cpp" line="639"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="154"/>
        <location filename="wgtlawsuites.cpp" line="446"/>
        <source>Lawsuite</source>
        <translation>Судебное дело</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="119"/>
        <source>Archiving is not possible</source>
        <translation>Архивация невозможна</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="120"/>
        <source>One or several outcoming documents are not agreed. Archiving is not possible until all procedural documents are agreed.</source>
        <translation>Один или несколько документов не согласованы. Архивация невозможна, пока все процедурные документы не будут согласованы.</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="133"/>
        <source>Archive case</source>
        <translation>Архивировать дело</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="134"/>
        <source>You&apos;re going to archive current case. Any information about case will be freezed. Do you want to continue?</source>
        <translation>Вы собираетесь архивировать текущее дело. Вся информация о деле будет заморожена. Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="153"/>
        <source>Lawsuites archive</source>
        <translation>Архив судебных дел</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="454"/>
        <source>Reg N</source>
        <translation>Вх.№</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="455"/>
        <location filename="wgtlawsuites.cpp" line="480"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="456"/>
        <source>Addressee</source>
        <translation>Адресат</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="457"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="458"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="477"/>
        <source>Document number</source>
        <translation>Номер документа</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="478"/>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="479"/>
        <source>Reg Num</source>
        <translation>Исх.№</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="481"/>
        <source>Document Type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="482"/>
        <source>Post ID</source>
        <translation>Почтовый идентификатор</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="483"/>
        <source>Execution date</source>
        <translation>Дата исполнения</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="554"/>
        <source>New lawsuite</source>
        <translation>Новое судебное дело</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="598"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="599"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="622"/>
        <source>Delete document</source>
        <translation>Удалить документ</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="623"/>
        <source>You&apos;re going to delete selected document. Are you sure?</source>
        <translation>Вы собираетесь удалить выбранный документ. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="116"/>
        <location filename="wgtlawsuites.cpp" line="656"/>
        <source>Decision</source>
        <translation>Определение</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="117"/>
        <location filename="wgtlawsuites.cpp" line="657"/>
        <source>Ruling</source>
        <translation>Постановление</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="118"/>
        <location filename="wgtlawsuites.cpp" line="658"/>
        <source>Judgement</source>
        <translation>Решение</translation>
    </message>
</context>
<context>
    <name>wgtReport</name>
    <message>
        <location filename="wgtreport.ui" line="14"/>
        <location filename="wgtreport.ui" line="271"/>
        <source>Report</source>
        <translation>Отчет</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="161"/>
        <source>Period</source>
        <translation>Период</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="173"/>
        <source>From:</source>
        <translation>С:</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="223"/>
        <source>To:</source>
        <translation>По:</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="20"/>
        <source>Show (and devide by)</source>
        <translation>Показать (и разделить на)</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="26"/>
        <source>All incoming</source>
        <translation>Все входящие</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="33"/>
        <source>Eighteen one</source>
        <translation>18.1</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="40"/>
        <source>Claims</source>
        <translation>Заявления</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="47"/>
        <source>Cases</source>
        <translation>Дела</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="54"/>
        <source>Administrative cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="61"/>
        <source>Lawsuites</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="68"/>
        <source>Open/closed</source>
        <translation>Открытые/закрытые</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="75"/>
        <source>Clauses</source>
        <translation>Статьи</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="82"/>
        <source>Executor</source>
        <oldsource>Exeecutor</oldsource>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="105"/>
        <source>Executors</source>
        <translation>Исполнители</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="117"/>
        <source>NLA</source>
        <translation>НПА</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="151"/>
        <source>Generate</source>
        <translation>Генерировать</translation>
    </message>
</context>
<context>
    <name>wgtTwoFieldsDialog</name>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="14"/>
        <source>Edit value</source>
        <translation>Изменение строки</translation>
    </message>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="20"/>
        <source>Value:</source>
        <translation>Значение:</translation>
    </message>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="40"/>
        <source>Using form:</source>
        <translation>Использующая форма:</translation>
    </message>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="82"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtTwoFieldsWCombo</name>
    <message>
        <location filename="wgttwofieldswcombo.ui" line="14"/>
        <source>Change record value</source>
        <translation>Изменить значение записи</translation>
    </message>
    <message>
        <location filename="wgttwofieldswcombo.ui" line="103"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="wgttwofieldswcombo.ui" line="123"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>wgtUserRights</name>
    <message>
        <location filename="wgtuserrights.ui" line="14"/>
        <location filename="wgtuserrights.ui" line="20"/>
        <source>User info</source>
        <translation>Информация о пользователе</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="26"/>
        <source>Username:</source>
        <translation>Имя пользователя:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="39"/>
        <source>Second name:</source>
        <translation>Фамилия:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="52"/>
        <source>First name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="65"/>
        <source>Third name:</source>
        <translation>Отчество:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="78"/>
        <source>Position:</source>
        <translation>Должность:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="94"/>
        <source>User rights</source>
        <translation>Права пользователя</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="100"/>
        <source>18.1 access</source>
        <translation>Доступ к форме 18.1</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="107"/>
        <source>Administrative cases archive access</source>
        <translation>Доступ к архиву заявлений</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="114"/>
        <source>Claims access</source>
        <translation>Доступ к форме заявления</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="121"/>
        <source>Lawsuites archive access</source>
        <translation>Доступ к архиву судебных дел</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="128"/>
        <source>Cases access</source>
        <translation>Доступ к форме дела</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="135"/>
        <source>FAS orders archive access</source>
        <translation>Доступ к архиву поручений ФАС</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="142"/>
        <source>Administrative cases access</source>
        <translation>Доступ к форме административные дела</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="149"/>
        <source>All incoming access</source>
        <translation>Доступ к форме все входящие документы</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="156"/>
        <source>Lawsuites access</source>
        <translation>Доступ к форме судебные дела</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="163"/>
        <source>Self report access</source>
        <translation>Доступ к собственному отчету</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="170"/>
        <source>FAS orders access</source>
        <translation>Доступ к форме поручения ФАС</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="177"/>
        <source>Full report access</source>
        <translation>Доступ к полному отчету</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="184"/>
        <source>18.1 archive access</source>
        <translation>Доступ к архиву 18.1</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="191"/>
        <source>Incoming handover</source>
        <translation>Передача расписанных документов</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="198"/>
        <source>Claims archive access</source>
        <translation>Доступ к архиву заявлений</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="205"/>
        <source>All incoming handover</source>
        <translation>Роспись входящих документов</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="212"/>
        <source>Cases archive access</source>
        <translation>Доступ к архиву дел</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="219"/>
        <source>User management</source>
        <translation>Управление пользователями</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="251"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtaddoutdoc</name>
    <message>
        <location filename="wgtaddoutdoc.ui" line="14"/>
        <source>Outcoming document</source>
        <oldsource>Add outcoming document</oldsource>
        <translation>Исходящий документ</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="237"/>
        <source>Post ID:</source>
        <translation>Почтовый идентификатор:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="284"/>
        <location filename="wgtaddoutdoc.ui" line="391"/>
        <source>View</source>
        <translation>Просмотр</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="310"/>
        <location filename="wgtaddoutdoc.ui" line="414"/>
        <source>Attach</source>
        <translation>Прикрепить</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="333"/>
        <location filename="wgtaddoutdoc.ui" line="437"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="250"/>
        <source>Editable version:</source>
        <translation>Редактируемая версия:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="20"/>
        <source>Document type:</source>
        <translation>Тип документа:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="151"/>
        <source>Outcome date:</source>
        <translation>Исходящая дата:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="360"/>
        <source>Scan copy:</source>
        <translation>Сканированная версия:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="527"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="194"/>
        <source>Execution date:</source>
        <translation>Дата исполнения:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="112"/>
        <source>Outcome num:</source>
        <translation>Исходящий номер:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="30"/>
        <source>Document num:</source>
        <translation>Номер документа:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="69"/>
        <source>Document date:</source>
        <translation>Дата документа:</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="26"/>
        <location filename="wgtaddoutdoc.cpp" line="128"/>
        <source>Select file</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="62"/>
        <location filename="wgtaddoutdoc.cpp" line="118"/>
        <location filename="wgtaddoutdoc.cpp" line="120"/>
        <location filename="wgtaddoutdoc.cpp" line="163"/>
        <location filename="wgtaddoutdoc.cpp" line="218"/>
        <location filename="wgtaddoutdoc.cpp" line="220"/>
        <location filename="wgtaddoutdoc.cpp" line="406"/>
        <location filename="wgtaddoutdoc.cpp" line="478"/>
        <location filename="wgtaddoutdoc.cpp" line="538"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="62"/>
        <location filename="wgtaddoutdoc.cpp" line="118"/>
        <location filename="wgtaddoutdoc.cpp" line="163"/>
        <location filename="wgtaddoutdoc.cpp" line="218"/>
        <source>Unable to copy attaching file</source>
        <translation>Невозможно скопировать прикрепляемый файл</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="70"/>
        <location filename="wgtaddoutdoc.cpp" line="171"/>
        <source>Another file is already attached. Do you want to replace it?</source>
        <oldsource>Another file is already attached. Do you want to delete it?</oldsource>
        <translation>Другой файл уже прикреплен. Вы хотите его заменить?</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="86"/>
        <location filename="wgtaddoutdoc.cpp" line="187"/>
        <location filename="wgtaddoutdoc.cpp" line="449"/>
        <location filename="wgtaddoutdoc.cpp" line="509"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="87"/>
        <location filename="wgtaddoutdoc.cpp" line="188"/>
        <location filename="wgtaddoutdoc.cpp" line="450"/>
        <location filename="wgtaddoutdoc.cpp" line="510"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="120"/>
        <location filename="wgtaddoutdoc.cpp" line="220"/>
        <location filename="wgtaddoutdoc.cpp" line="406"/>
        <location filename="wgtaddoutdoc.cpp" line="478"/>
        <location filename="wgtaddoutdoc.cpp" line="538"/>
        <source>Unable to delete attached file</source>
        <translation>Невозможно удалить прикрепленный файл</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.cpp" line="433"/>
        <location filename="wgtaddoutdoc.cpp" line="493"/>
        <source>You&apos;re going to delete attached file? Are you sure?</source>
        <translation>Вы собираетесь удалить прикрепленный файл. Вы уверены?</translation>
    </message>
</context>
<context>
    <name>wgtallinboxdocs</name>
    <message>
        <location filename="wgtallinboxdocs.ui" line="14"/>
        <location filename="wgtallinboxdocs.ui" line="27"/>
        <location filename="wgtallinboxdocs.cpp" line="291"/>
        <source>All incoming documents</source>
        <translation>Все входящие документы</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.ui" line="74"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="29"/>
        <source>Reg N</source>
        <oldsource>Reg �</oldsource>
        <translation>Вх.N</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="30"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="31"/>
        <source>Citizen Appeal</source>
        <translation>Обращение граждан</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="32"/>
        <source>Entry Date</source>
        <translation>Дата внесения</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="33"/>
        <source>Send Way</source>
        <translation>Способ отправки</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="34"/>
        <source>Outcome N</source>
        <oldsource>Outcome �</oldsource>
        <translation>Исх. N</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="35"/>
        <source>Outcome Date</source>
        <translation>Исх. дата</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="36"/>
        <source>Addressee Name</source>
        <translation>Адресат</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="37"/>
        <source>Destination Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="38"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="39"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="40"/>
        <source>Executor</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="41"/>
        <source>Handoverdate</source>
        <translation>Дата росписи</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="42"/>
        <source>Handovered to department</source>
        <translation>Передано в отдел</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="78"/>
        <source>Change executor</source>
        <translation>Изменить исполнителя</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="79"/>
        <source>Change type</source>
        <translation>Изменить тип</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="80"/>
        <location filename="wgtallinboxdocs.cpp" line="116"/>
        <source>Delete row</source>
        <translation>Удалить строку</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="97"/>
        <source>New executor:</source>
        <translation>Новый исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="97"/>
        <source>Current executor:</source>
        <translation>Текущий исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="106"/>
        <source>Current type:</source>
        <translation>Текущий тип:</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="106"/>
        <source>New type:</source>
        <translation>Новый тип:</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="117"/>
        <source>You are going to delete row. That operation might cause changes into other records. This is irreversible operation! Do you want to continue?</source>
        <translation>Вы собираетесь удалить строку. Эта операция может повлечь изменения в других записях. Это необратимая операция! Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="132"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="133"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="271"/>
        <source>Claim</source>
        <translation>Заявление</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="185"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="wgtallinboxdocs.cpp" line="185"/>
        <source>Unable to delete attached file</source>
        <translation>Невозможно удалить прикрепленный файл</translation>
    </message>
</context>
<context>
    <name>wgtarchive</name>
    <message>
        <location filename="wgtarchive.ui" line="14"/>
        <location filename="wgtarchive.ui" line="27"/>
        <source>Archive</source>
        <translation>Архив</translation>
    </message>
    <message>
        <location filename="wgtarchive.ui" line="47"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtarchive.ui" line="76"/>
        <source>Eighteen one claims</source>
        <translation>Заявления 18.1</translation>
    </message>
    <message>
        <location filename="wgtarchive.ui" line="96"/>
        <source>Claims</source>
        <translation>Заявления</translation>
    </message>
    <message>
        <location filename="wgtarchive.ui" line="116"/>
        <source>Cases</source>
        <translation>Дела</translation>
    </message>
    <message>
        <location filename="wgtarchive.ui" line="136"/>
        <source>Administrative cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="wgtarchive.ui" line="156"/>
        <source>Lawsuites</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="64"/>
        <location filename="wgtarchive.cpp" line="97"/>
        <source>Reg N</source>
        <translation>Вх.№</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="65"/>
        <location filename="wgtarchive.cpp" line="98"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="66"/>
        <location filename="wgtarchive.cpp" line="99"/>
        <location filename="wgtarchive.cpp" line="131"/>
        <source>Handover date</source>
        <translation>Дата росписи</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="67"/>
        <location filename="wgtarchive.cpp" line="100"/>
        <location filename="wgtarchive.cpp" line="132"/>
        <location filename="wgtarchive.cpp" line="178"/>
        <location filename="wgtarchive.cpp" line="204"/>
        <source>Executor</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="68"/>
        <location filename="wgtarchive.cpp" line="101"/>
        <location filename="wgtarchive.cpp" line="133"/>
        <location filename="wgtarchive.cpp" line="196"/>
        <source>Complainant</source>
        <translation>Заявитель</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="69"/>
        <location filename="wgtarchive.cpp" line="102"/>
        <location filename="wgtarchive.cpp" line="134"/>
        <location filename="wgtarchive.cpp" line="171"/>
        <location filename="wgtarchive.cpp" line="197"/>
        <source>Defendant</source>
        <translation>Ответчик</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="70"/>
        <source>Notification</source>
        <translation>Уведомление</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="71"/>
        <location filename="wgtarchive.cpp" line="104"/>
        <location filename="wgtarchive.cpp" line="138"/>
        <source>NLA</source>
        <oldsource>NPA</oldsource>
        <translation>НПА</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="72"/>
        <location filename="wgtarchive.cpp" line="139"/>
        <source>Submission date</source>
        <translation>Дата представления</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="73"/>
        <location filename="wgtarchive.cpp" line="140"/>
        <source>Committee date</source>
        <translation>Дата комиссии</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="74"/>
        <location filename="wgtarchive.cpp" line="107"/>
        <location filename="wgtarchive.cpp" line="141"/>
        <location filename="wgtarchive.cpp" line="202"/>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="75"/>
        <source>Decision num</source>
        <translation>№ решения</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="76"/>
        <source>Decision date</source>
        <translation>Дата решения</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="77"/>
        <location filename="wgtarchive.cpp" line="143"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="78"/>
        <source>Executed</source>
        <translation>Исполнено</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="79"/>
        <location filename="wgtarchive.cpp" line="145"/>
        <source>Execution info</source>
        <translation>Информация об исполнении</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="103"/>
        <location filename="wgtarchive.cpp" line="137"/>
        <location filename="wgtarchive.cpp" line="173"/>
        <source>Clause</source>
        <translation>Статья</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="128"/>
        <source>Case num</source>
        <translation>№ дела</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="126"/>
        <source>Claim N</source>
        <translation>№ заявления</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="127"/>
        <location filename="wgtarchive.cpp" line="195"/>
        <source>Claim Date</source>
        <translation>Дата заявления</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="129"/>
        <source>Decree num</source>
        <translation>№ приказа</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="130"/>
        <source>Decree date</source>
        <translation>Дата приказа</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="142"/>
        <source>Clauses</source>
        <translation>Статьи</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="144"/>
        <source>Order executed</source>
        <translation>Предписание исполнено</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="108"/>
        <location filename="wgtarchive.cpp" line="169"/>
        <location filename="wgtarchive.cpp" line="194"/>
        <source>Case N</source>
        <translation>Дело №</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="105"/>
        <location filename="wgtarchive.cpp" line="135"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="106"/>
        <location filename="wgtarchive.cpp" line="136"/>
        <source>Prolong date</source>
        <oldsource>Prolongdate</oldsource>
        <translation>Дата продления</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="109"/>
        <location filename="wgtarchive.cpp" line="146"/>
        <source>Prolong</source>
        <translation>Продление</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="147"/>
        <location filename="wgtarchive.cpp" line="170"/>
        <source>Excitation date</source>
        <translation>Дата возбуждения</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="172"/>
        <source>Defendant address</source>
        <translation>Адрес ответчика</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="174"/>
        <source>Clause part</source>
        <translation>Часть</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="175"/>
        <source>Date of judgement</source>
        <translation>Дата вынесения постановления</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="176"/>
        <source>Decision</source>
        <translation>Решение</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="177"/>
        <source>Amount</source>
        <translation>Сумма</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="198"/>
        <source>Third persons</source>
        <translation>Третьи лица</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="199"/>
        <source>Other persons</source>
        <translation>Иные лица</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="200"/>
        <source>Judge</source>
        <translation>Судья</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="201"/>
        <source>Case card url</source>
        <translation>Ссылка на карточку дела</translation>
    </message>
    <message>
        <location filename="wgtarchive.cpp" line="203"/>
        <source>Instance</source>
        <translation>Инстанция</translation>
    </message>
</context>
<context>
    <name>wgtarchivehistory</name>
    <message>
        <location filename="wgtarchivehistory.ui" line="14"/>
        <source>View history</source>
        <translation>Просмотр истории</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.ui" line="47"/>
        <source>Archive</source>
        <translation>Архив</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.ui" line="61"/>
        <source>&gt; View history</source>
        <translation>&gt; Просмотр истории</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.ui" line="89"/>
        <source>Incoming</source>
        <translation>Входящие</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.ui" line="109"/>
        <source>Outcoming</source>
        <translation>Исходящие</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="48"/>
        <location filename="wgtarchivehistory.cpp" line="70"/>
        <source>Reg N: </source>
        <oldsource>Reg N:</oldsource>
        <translation>Вх.№: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="56"/>
        <location filename="wgtarchivehistory.cpp" line="92"/>
        <source>Committee date: </source>
        <oldsource>Decision date:</oldsource>
        <translation>Дата комиссии: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="49"/>
        <location filename="wgtarchivehistory.cpp" line="71"/>
        <source>Reg date: </source>
        <translation>Дата регистрации: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="51"/>
        <location filename="wgtarchivehistory.cpp" line="73"/>
        <location filename="wgtarchivehistory.cpp" line="88"/>
        <source>Handover date: </source>
        <translation>Дата росписи: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="50"/>
        <location filename="wgtarchivehistory.cpp" line="72"/>
        <location filename="wgtarchivehistory.cpp" line="87"/>
        <location filename="wgtarchivehistory.cpp" line="108"/>
        <location filename="wgtarchivehistory.cpp" line="119"/>
        <source>Executor: </source>
        <translation>Исполнитель: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="52"/>
        <location filename="wgtarchivehistory.cpp" line="74"/>
        <location filename="wgtarchivehistory.cpp" line="89"/>
        <location filename="wgtarchivehistory.cpp" line="120"/>
        <source>Complainant: </source>
        <translation>Заявитель: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="53"/>
        <location filename="wgtarchivehistory.cpp" line="75"/>
        <location filename="wgtarchivehistory.cpp" line="90"/>
        <location filename="wgtarchivehistory.cpp" line="109"/>
        <source>Defendant: </source>
        <translation>Ответчик: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="54"/>
        <source>Notification: </source>
        <translation>Уведомление: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="55"/>
        <source>NLA: </source>
        <translation>НПА: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="57"/>
        <location filename="wgtarchivehistory.cpp" line="77"/>
        <location filename="wgtarchivehistory.cpp" line="93"/>
        <location filename="wgtarchivehistory.cpp" line="128"/>
        <source>Result: </source>
        <translation>Результат: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="58"/>
        <location filename="wgtarchivehistory.cpp" line="94"/>
        <source>AWARD</source>
        <oldsource>Award</oldsource>
        <translation>РЕШЕНИЕ</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="59"/>
        <location filename="wgtarchivehistory.cpp" line="79"/>
        <source>#</source>
        <translation>№</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="60"/>
        <location filename="wgtarchivehistory.cpp" line="86"/>
        <source>from </source>
        <oldsource>from: </oldsource>
        <translation>от </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="61"/>
        <location filename="wgtarchivehistory.cpp" line="97"/>
        <source>Order</source>
        <oldsource>Order: </oldsource>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="63"/>
        <location filename="wgtarchivehistory.cpp" line="99"/>
        <source>Order executed</source>
        <oldsource>Executed: </oldsource>
        <translation>Предписание исполнено</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="64"/>
        <location filename="wgtarchivehistory.cpp" line="100"/>
        <source>Execution info: </source>
        <translation>Информация об исполнении: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="66"/>
        <location filename="wgtarchivehistory.cpp" line="102"/>
        <source>Order not executed</source>
        <translation>Предписание не исполнено</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="76"/>
        <location filename="wgtarchivehistory.cpp" line="91"/>
        <location filename="wgtarchivehistory.cpp" line="110"/>
        <location filename="wgtarchivehistory.cpp" line="121"/>
        <source>Clause: </source>
        <translation>Статья: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="78"/>
        <source>CASE</source>
        <translation>ДЕЛО</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="82"/>
        <source>Claim N: </source>
        <oldsource>Claim N:</oldsource>
        <translation>Заявление №: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="83"/>
        <location filename="wgtarchivehistory.cpp" line="118"/>
        <source>Claim date: </source>
        <translation>Дата заявления: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="84"/>
        <location filename="wgtarchivehistory.cpp" line="106"/>
        <location filename="wgtarchivehistory.cpp" line="117"/>
        <source>Case N: </source>
        <translation>Номер дела: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="85"/>
        <source>Decree #</source>
        <oldsource>Decree N: </oldsource>
        <translation>Приказ №</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="107"/>
        <source>Excitation date: </source>
        <translation>Дата возбуждения: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="111"/>
        <source>Clause part: </source>
        <translation>Часть: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="112"/>
        <source>Date of judgement: </source>
        <translation>Дата вынесения постановления: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="113"/>
        <source>Decision: </source>
        <translation>Решение: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="114"/>
        <source>Amount: </source>
        <translation>Сумма: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="122"/>
        <source>Third persons: </source>
        <translation>Третьи лица: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="123"/>
        <source>Other persons: </source>
        <translation>Иные лица: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="124"/>
        <source>Judge: </source>
        <translation>Судья: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="125"/>
        <source>Trial date/time: </source>
        <translation>Дата/время заседания: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="126"/>
        <source>Court address: </source>
        <translation>Адрес суда: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="127"/>
        <source>Case card url: </source>
        <translation>Ссылка на карточку дела: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="129"/>
        <source>Instance: </source>
        <translation>Инстанция: </translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="136"/>
        <source>Eighteenone archive</source>
        <translation>Архив 18.1</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="137"/>
        <source>Claims archive</source>
        <translation>Архив заявлений</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="138"/>
        <source>Cases archive</source>
        <translation>Архив дел</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="139"/>
        <source>Administrative cases archive</source>
        <translation>Архив административных дел</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="140"/>
        <source>Lawsuites archive</source>
        <translation>Архив судебных дел</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="145"/>
        <source>Reg N</source>
        <translation>Вх.№</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="146"/>
        <location filename="wgtarchivehistory.cpp" line="174"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="147"/>
        <source>Addressee</source>
        <translation>Адресат</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="148"/>
        <source>Document type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="149"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="171"/>
        <source>Document number</source>
        <translation>Номер документа</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="172"/>
        <source>Document date</source>
        <translation>Дата документа</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="173"/>
        <source>Reg Num</source>
        <translation>Исх.№</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="175"/>
        <source>Document Type</source>
        <translation>Тип документа</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="176"/>
        <source>Post ID</source>
        <translation>Почтовый идентификатор</translation>
    </message>
    <message>
        <location filename="wgtarchivehistory.cpp" line="177"/>
        <source>Execution date</source>
        <translation>Дата исполнения</translation>
    </message>
</context>
<context>
    <name>wgtattachedto</name>
    <message>
        <location filename="wgtattachedto.ui" line="14"/>
        <source>Select attachment record</source>
        <translation>Выберите запись для прикрепления</translation>
    </message>
    <message>
        <location filename="wgtattachedto.ui" line="46"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="36"/>
        <location filename="wgtattachedto.cpp" line="59"/>
        <source>Reg N</source>
        <translation>Вх.N</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="37"/>
        <location filename="wgtattachedto.cpp" line="60"/>
        <source>Reg date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="39"/>
        <location filename="wgtattachedto.cpp" line="62"/>
        <location filename="wgtattachedto.cpp" line="85"/>
        <source>Handover date</source>
        <translation>Дата росписи</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="40"/>
        <location filename="wgtattachedto.cpp" line="63"/>
        <location filename="wgtattachedto.cpp" line="86"/>
        <location filename="wgtattachedto.cpp" line="121"/>
        <location filename="wgtattachedto.cpp" line="141"/>
        <source>Executor</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="42"/>
        <location filename="wgtattachedto.cpp" line="65"/>
        <location filename="wgtattachedto.cpp" line="88"/>
        <location filename="wgtattachedto.cpp" line="128"/>
        <source>Complainant</source>
        <translation>Заявитель</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="43"/>
        <location filename="wgtattachedto.cpp" line="66"/>
        <location filename="wgtattachedto.cpp" line="89"/>
        <location filename="wgtattachedto.cpp" line="111"/>
        <location filename="wgtattachedto.cpp" line="129"/>
        <source>Defendant</source>
        <translation>Ответчик</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="44"/>
        <source>Notification</source>
        <translation>Уведомление</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="45"/>
        <location filename="wgtattachedto.cpp" line="68"/>
        <location filename="wgtattachedto.cpp" line="95"/>
        <source>NLA</source>
        <translation>НПА</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="47"/>
        <location filename="wgtattachedto.cpp" line="71"/>
        <location filename="wgtattachedto.cpp" line="98"/>
        <source>Submission date</source>
        <translation>Дата представления</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="48"/>
        <location filename="wgtattachedto.cpp" line="99"/>
        <source>Committee date</source>
        <translation>Дата комиссии</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="52"/>
        <location filename="wgtattachedto.cpp" line="91"/>
        <source>Decision num</source>
        <translation>№ решения</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="54"/>
        <location filename="wgtattachedto.cpp" line="104"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="55"/>
        <source>Executed</source>
        <translation>Исполнено</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="56"/>
        <location filename="wgtattachedto.cpp" line="106"/>
        <source>Execution info</source>
        <translation>Информация об исполнении</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="67"/>
        <location filename="wgtattachedto.cpp" line="94"/>
        <location filename="wgtattachedto.cpp" line="115"/>
        <source>Clause</source>
        <translation>Статья</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="70"/>
        <location filename="wgtattachedto.cpp" line="97"/>
        <source>Request</source>
        <translation>Запрос</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="75"/>
        <source>Case num</source>
        <translation>№ дела</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="78"/>
        <source>Claim number</source>
        <translation>Номер жалобы</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="79"/>
        <location filename="wgtattachedto.cpp" line="126"/>
        <source>Claim date</source>
        <translation>Дата жалобы</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="83"/>
        <source>Decree N</source>
        <translation>Приказ №</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="84"/>
        <source>Decree date</source>
        <translation>Дата приказа</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="103"/>
        <source>Clauses</source>
        <translation>Статьи</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="105"/>
        <source>Order executed</source>
        <translation>Предписание исполнено</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="110"/>
        <source>Excitation date</source>
        <translation>Дата возбуждения</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="113"/>
        <source>Defendant&apos;s address</source>
        <translation>Адрес ответчика</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="116"/>
        <source>Clause part</source>
        <translation>Часть</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="118"/>
        <source>Date of judgement</source>
        <translation>Дата вынесения постановления</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="119"/>
        <source>Decision</source>
        <translation>Решение</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="120"/>
        <source>Amount</source>
        <translation>Сумма</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="131"/>
        <source>Third persons</source>
        <translation>Третьи лица</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="132"/>
        <source>Other persons</source>
        <translation>Иные лица</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="133"/>
        <source>Judge</source>
        <translation>Судья</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="134"/>
        <source>Trial date/time</source>
        <translation>Дата/время заседания</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="135"/>
        <source>Court address</source>
        <translation>Адрес суда</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="136"/>
        <source>Case card url</source>
        <translation>Ссылка на карточку дела</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="140"/>
        <source>Instance</source>
        <translation>Инстанция</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="50"/>
        <location filename="wgtattachedto.cpp" line="73"/>
        <location filename="wgtattachedto.cpp" line="101"/>
        <location filename="wgtattachedto.cpp" line="138"/>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="53"/>
        <location filename="wgtattachedto.cpp" line="92"/>
        <source>Decision date</source>
        <translation>Дата решения</translation>
    </message>
    <message>
        <location filename="wgtattachedto.cpp" line="81"/>
        <location filename="wgtattachedto.cpp" line="109"/>
        <location filename="wgtattachedto.cpp" line="124"/>
        <source>Case number</source>
        <translation>№ дела</translation>
    </message>
</context>
<context>
    <name>wgtdocsforagreement</name>
    <message>
        <location filename="wgtdocsforagreement.ui" line="556"/>
        <source>View attached file</source>
        <translation>Просмотр прикрепленного файла</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="591"/>
        <source>Return to executor</source>
        <translation>Вернуть исполнителю</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="626"/>
        <source>Agree</source>
        <translation>Согласовать</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="264"/>
        <source>Attached to claim (case) №:</source>
        <oldsource>Attached to claim (case):</oldsource>
        <translation>Прикреплен к заявлению (делу) №:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="241"/>
        <source>Attached to:</source>
        <translation>Прикреплен к:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="300"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="138"/>
        <source>Outcome date:</source>
        <translation>Исходящая дата:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="340"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="14"/>
        <location filename="wgtdocsforagreement.ui" line="663"/>
        <source>Documents for agreement</source>
        <translation>Документы на согласование</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="27"/>
        <source>Document Info</source>
        <translation>Информация о документе</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="323"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="33"/>
        <source>Document number:</source>
        <translation>Номер документа:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="92"/>
        <source>Document date:</source>
        <translation>Дата документа:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="69"/>
        <source>Outcome number:</source>
        <translation>Исходящий номер:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="178"/>
        <source>Document type:</source>
        <translation>Тип документа:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.ui" line="201"/>
        <source>Execution date:</source>
        <translation>Дата исполнения:</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.cpp" line="31"/>
        <source>Eighteen one claim</source>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.cpp" line="35"/>
        <source>Claim</source>
        <translation>Заявление</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.cpp" line="39"/>
        <source>Case</source>
        <translation>Дело</translation>
    </message>
    <message>
        <location filename="wgtdocsforagreement.cpp" line="43"/>
        <source>Administrative case</source>
        <translation>Административное дело</translation>
    </message>
</context>
<context>
    <name>wgtmessages</name>
    <message>
        <location filename="wgtmessages.ui" line="14"/>
        <source>Messages</source>
        <oldsource>messages</oldsource>
        <translation>Сообщения</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="41"/>
        <source>Write</source>
        <translation>Написать</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="52"/>
        <source>Reply</source>
        <translation>Ответить</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="63"/>
        <source>Forward</source>
        <translation>Перенаправить</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="87"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="105"/>
        <source>Inbox</source>
        <translation>Входящие</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="131"/>
        <source>Outbox</source>
        <translation>Исходящие</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="191"/>
        <source>From:</source>
        <translation>От кого:</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="214"/>
        <source>Subject:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="39"/>
        <location filename="wgtmessages.cpp" line="60"/>
        <source>Original Message</source>
        <translation>Оригинальное сообщение</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="40"/>
        <location filename="wgtmessages.cpp" line="61"/>
        <location filename="wgtmessages.cpp" line="138"/>
        <location filename="wgtmessages.cpp" line="146"/>
        <source>Date, time</source>
        <oldsource>Date, time:</oldsource>
        <translation>Дата, время</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="41"/>
        <location filename="wgtmessages.cpp" line="63"/>
        <location filename="wgtmessages.cpp" line="70"/>
        <location filename="wgtmessages.cpp" line="139"/>
        <location filename="wgtmessages.cpp" line="147"/>
        <source>From</source>
        <translation>От</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="65"/>
        <location filename="wgtmessages.cpp" line="72"/>
        <location filename="wgtmessages.cpp" line="140"/>
        <location filename="wgtmessages.cpp" line="148"/>
        <source>To</source>
        <translation>Кому</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="43"/>
        <location filename="wgtmessages.cpp" line="67"/>
        <location filename="wgtmessages.cpp" line="74"/>
        <location filename="wgtmessages.cpp" line="141"/>
        <location filename="wgtmessages.cpp" line="149"/>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="96"/>
        <source>Delete message</source>
        <translation>Удалить сообщение</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="97"/>
        <source>You&apos;re going to delete selected message. Are you sure?</source>
        <translation>Вы собираетесь удалить выбранное сообщение. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="142"/>
        <location filename="wgtmessages.cpp" line="150"/>
        <source>Message</source>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="143"/>
        <location filename="wgtmessages.cpp" line="151"/>
        <source>Attachment type</source>
        <translation>Тип вложения</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="144"/>
        <location filename="wgtmessages.cpp" line="152"/>
        <source>Read</source>
        <translation>Прочтено</translation>
    </message>
</context>
<context>
    <name>wgtnewmessage</name>
    <message>
        <location filename="wgtnewmessage.ui" line="14"/>
        <source>New message</source>
        <translation>Новое сообщение</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="27"/>
        <source>To:</source>
        <translation>Кому:</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="40"/>
        <source>Subject:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="56"/>
        <source>Message</source>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="84"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtreadmessage</name>
    <message>
        <location filename="wgtreadmessage.ui" line="14"/>
        <location filename="wgtreadmessage.ui" line="89"/>
        <source>Message</source>
        <oldsource>wgtreadmessage</oldsource>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="26"/>
        <source>From:</source>
        <translation>От кого:</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="49"/>
        <source>To:</source>
        <translation>Кому:</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="72"/>
        <source>Subject:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="120"/>
        <source>Open attachement</source>
        <translation>Открыть вложение</translation>
    </message>
</context>
<context>
    <name>wgtwelcome</name>
    <message>
        <location filename="wgtwelcome.ui" line="14"/>
        <source>Welcome</source>
        <translation>Приветствие</translation>
    </message>
    <message>
        <location filename="wgtwelcome.ui" line="27"/>
        <source>Shedule</source>
        <translation>Расписание</translation>
    </message>
    <message>
        <location filename="wgtwelcome.ui" line="77"/>
        <source>Hide expiring records</source>
        <translation>Не показывать истекающие сроки</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="35"/>
        <source>K</source>
        <translation>К</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="37"/>
        <source>R</source>
        <translation>Р</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="39"/>
        <source>S</source>
        <translation>Ш</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="41"/>
        <source>A</source>
        <translation>А</translation>
    </message>
</context>
</TS>
