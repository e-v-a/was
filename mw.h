#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include <QTimer>
#include <QPrinter>
#include <QPrintPreviewDialog>
#include "wgtdocsforagreement.h"
#include "wgteighteenone.h"
#include "wgtamzclaims.h"
#include "wgtamzcases.h"
#include "wgtinboxclaims.h"
#include "wgtallinboxdocs.h"
#include "wgtmessages.h"
#include "wgtarchive.h"
#include "wgtadministrativecases.h"
#include "wgtlawsuites.h"
#include "wgtadmmanagement.h"
#include "wgtwelcome.h"
#include "wgtreport.h"
#include "qevent.h"

namespace Ui {
class mw;
}

class mw : public QMainWindow
{
    Q_OBJECT
public:
    explicit mw(QWidget *parent = 0);
    ~mw();
	DBOperations *dboper;

public slots:
	void initMainForm();
	void StartUpdateTimer();
	void SendInternalMessage(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage, QString strAttachedType, int iAttachedDocId);
	void setStatusText(int iCurrentRow, int iRowsCount);
	void UpdateClaimsNum(int iClaimsNum);
	void on_tmrUpdateMessages();
	void GoToCase(QString casenum);
	void LoadAdded(QString admcasenum);
	void LoadAddedLS(QString lscasenum);
	void closeIncoming();
	void FillShedule(bool bShort, bool bPrint);
	void PrintHTML(const QString strHTML);
	void PrintPreview(QPrinter *prntr);

private slots:
    void on_pbEighteenOne_clicked();
	void on_pbAMZClaims_clicked();
	void on_pbAMZCases_clicked();
	void on_pbAdmCases_clicked();
	void on_pbLawsuites_clicked();
	void on_pbReport_clicked();
	void on_pbAdminPanel_clicked();
	void on_tmrUpdateIncoming();
	void on_pbIncoming_clicked();
	void on_pbAllIncoming_clicked();
	void on_pbNotifications_clicked();
	void on_pbMailBox_clicked();
	void on_pbCalendar_clicked();
	void on_pbSettings_clicked();
	void on_pbArchive_clicked();
	void changeEvent(QEvent *e);
	void resizeEvent(QResizeEvent* e);
	void on_pbDocsForAgreement_clicked();
	void UpdateDocsNum(int iRowsCount);
	void closeEvent(QCloseEvent *event);
    void on_pbAddAdmCase_clicked();
    void on_pbAddLawSuite_clicked();
    void on_pbRefresh_clicked();
    void on_pbSync_toggled(bool checked);
	void generateReport(QString sFilter,QDate &dtFrom,QDate &dtTo);
	
signals:
	//database operations
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
	void MainTableAssign(QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
	void setFilterMB(QString sFilter);
	
	void SetFormFields(int recordnum);
	void SetClaimsFormFields(int recordnum);
	void SetCasesFormFields(int recordnum);
	void SetAdmCasesFormFields(int recordnum);
	void SetInboxFields(int recordnum);
	void SetD4AFormFields(int recordnum);
	void SetLSFormFields(int recordnum);
	void clearD4AFields();
	void ConnectModelToView();
	void ConnectMessagesModelToView();
	void ConnectArchiveModelToView();
	void setDBOperLink();
	void setClaimsDBOperLink();
	void setCasesDBOperLink();
	void setACDBOperLink();
	void setLSOperLink();
	void setAdmPanelDBOperLink();
	void setAttachedOperLink();
	void clearInboxSearch();
	void clearEOSearch();
	void clearACLSearch();
	void clearACSSearch();
	void clearADMSearch();
	void clearLSSearch();
	void ConnectSlots();
	void SetArchiveHistoryLinks();
	void EnableBlankFields();
	void EnableLSBlankFields();
	void initAdminPanel();
	void setShedule(const QString strSheduleHTML, bool bShort);
	void setUsername(const QString strUsername);
	void initEditActions();
	void showGeneratedReport(QString sHTML);

private:
    Ui::mw *ui;
	wgtdocsforagreement *wDocsForAgreement;
    wgtEighteenOne *wEO;
	wgtAMZClaims *wClaims;
	wgtAMZCases *wCases;
	wgtInboxClaims *wInboxClaims;
	wgtallinboxdocs *wAllInboxClaims;
	wgtarchive *wArchive;
	wgtAdministrativeCases *wAdmCases;
	wgtLawsuites *wLawsuites;
	wgtarchivehistory *wArchiveHistory;
	wgtAdmManagement *wAdminPanel;
	wgtmessages *wMessages;
	wgtwelcome *wWelcome;
	wgtReport *wReport;
    wgtGeneratedReport *wGenReport;
	bool db05;
	QSqlQuery query;
	int iQuerySize;
	QTimer *tmrUpdateIncoming;
	QTimer *tmrUpdateMessages;
	QSqlTableModel *sqmIncoming;
	QFont fntBold;
	QFont fntNormal;
	QString strCaseNum;
	QString strAdmCaseNum;
	QString strLSCaseNum;
	void SetMenuControls(QString buttonname);
	void LoadFormTable(QString sForm,QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
	QStringList stlDecisionTypes;
	QStringList stlNLA;
	QStringList stlExecutors;
	QStringList stlInstances;
	QPrinter *printerDevice;
	QPrintPreviewDialog *prevDlg;
	QTextDocument textDocument;
};

#endif // MW_H
