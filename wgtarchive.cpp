#include "wgtarchive.h"
#include "datedelegate.h"
#include "ui_wgtarchive.h"

wgtarchive::wgtarchive(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtarchive)
{
    ui->setupUi(this);
}

wgtarchive::~wgtarchive()
{
    delete ui;
}

void wgtarchive::on_tvEO_doubleClicked(const QModelIndex &index)
{
	setVisible(false);
	wArcHistory->iMode=0;
	emit SetArcHistoryFields(index.row());
	//wArcHistory->setVisible(true);
}

void wgtarchive::on_tvClaims_doubleClicked(const QModelIndex &index)
{
	setVisible(false);
	wArcHistory->iMode=1;
	emit SetArcHistoryFields(index.row());
	//wArcHistory->setVisible(true);
}

void wgtarchive::on_leSearch_textChanged(const QString &arg1)
{
	QString strFilterString;
	if(ui->twArchive->currentIndex()==0)
		strFilterString="[regnum] LIKE '%"+arg1+"%' OR [complainant] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1+"%'";
	else if(ui->twArchive->currentIndex()==0)
		strFilterString="[regnum] LIKE '%"+arg1+"%' OR [complainant] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1+"%' OR [casenum] LIKE '%"+arg1+"%'";
	else if(ui->twArchive->currentIndex()==0)
		strFilterString="[claimnum] LIKE '%"+arg1+"%' OR [complainant] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1+"%' OR [casenum] LIKE '%"+arg1+"%' OR [decreenum] LIKE'%"+arg1+"%'";
    dboper->mdlDB05->setFilter(QString(strFilterString));
    dboper->mdlDB05->select();
}

void wgtarchive::ConnectArchiveModelToView() {
	//wArcHistory=wArcHist;
	int iType;
	if(dboper->UsrSettings.stkUserPermissions.bArchiveEighteenOne) iType=0;
	else if(dboper->UsrSettings.stkUserPermissions.bArchiveClaims) iType=1;
	else if(dboper->UsrSettings.stkUserPermissions.bArchiveCases) iType=3;
	else if(dboper->UsrSettings.stkUserPermissions.bArchiveLawSuites) iType=4;
	iType=ui->twArchive->currentIndex();
	if(iType==0) dboper->mdlDB05->setTable("eighteenone_arc");
	else if(iType==1) dboper->mdlDB05->setTable("amzclaims_arc");
	else if(iType==2) dboper->mdlDB05->setTable("amzcases_arc");
	else if(iType==3) dboper->mdlDB05->setTable("admcases_arc");
	else if(iType==4) dboper->mdlDB05->setTable("lawsuites_arc");
	
	dboper->mdlDB05->setSort(1,Qt::AscendingOrder);
	dboper->mdlDB05->select();

	if(iType==0) { //18.1
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("Handover date"));
		dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Executor"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Complainant"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Defendant"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Notification"));
		dboper->mdlDB05->setHeaderData(8, Qt::Horizontal, tr("NLA"));
		dboper->mdlDB05->setHeaderData(9, Qt::Horizontal, tr("Submission date"));
		dboper->mdlDB05->setHeaderData(10, Qt::Horizontal, tr("Committee date"));
		dboper->mdlDB05->setHeaderData(11, Qt::Horizontal, tr("Result"));
		dboper->mdlDB05->setHeaderData(12, Qt::Horizontal, tr("Decision num"));
		dboper->mdlDB05->setHeaderData(13, Qt::Horizontal, tr("Decision date"));
		dboper->mdlDB05->setHeaderData(14, Qt::Horizontal, tr("Order"));
		dboper->mdlDB05->setHeaderData(15, Qt::Horizontal, tr("Executed"));
		dboper->mdlDB05->setHeaderData(16, Qt::Horizontal, tr("Execution info"));
	
		ui->tvEO->setModel(dboper->mdlDB05);
	
		ui->tvEO->hideColumn(0);
		ui->tvEO->setItemDelegateForColumn(2, new DataDelegate());
		ui->tvEO->setItemDelegateForColumn(3, new DataDelegate());
		ui->tvEO->setItemDelegateForColumn(9, new DataDelegate());
		ui->tvEO->setItemDelegateForColumn(13, new DataDelegate());

		ui->tvEO->resizeColumnsToContents();
		ui->tvEO->horizontalHeader()->setStretchLastSection(true);
		ui->tvEO->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

		ui->tvEO->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
		ui->tvEO->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	}
	else if(iType==1) { //Claims
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("Handover date"));
		dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Executor"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Complainant"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Defendant"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Clause"));
		dboper->mdlDB05->setHeaderData(8, Qt::Horizontal, tr("NLA"));
		dboper->mdlDB05->setHeaderData(9, Qt::Horizontal, tr("Summary"));
		dboper->mdlDB05->setHeaderData(10, Qt::Horizontal, tr("Prolong date"));
		dboper->mdlDB05->setHeaderData(11, Qt::Horizontal, tr("Result"));
		dboper->mdlDB05->setHeaderData(12, Qt::Horizontal, tr("Case N"));
		dboper->mdlDB05->setHeaderData(13, Qt::Horizontal, tr("Prolong"));
	
		ui->tvClaims->setModel(dboper->mdlDB05);
	
		ui->tvClaims->hideColumn(0);
		ui->tvClaims->setItemDelegateForColumn(2, new DataDelegate());
		ui->tvClaims->setItemDelegateForColumn(3, new DataDelegate());
		ui->tvClaims->setItemDelegateForColumn(10, new DataDelegate());

		ui->tvClaims->resizeColumnsToContents();
		ui->tvClaims->horizontalHeader()->setStretchLastSection(true);
		ui->tvClaims->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

		ui->tvClaims->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
		ui->tvClaims->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	}
	else if(iType==2) { //Cases
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Claim N"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Claim Date"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("Case num"));
		dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Decree num"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Decree date"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Handover date"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Executor"));
		dboper->mdlDB05->setHeaderData(8, Qt::Horizontal, tr("Complainant"));
		dboper->mdlDB05->setHeaderData(9, Qt::Horizontal, tr("Defendant"));
		dboper->mdlDB05->setHeaderData(10, Qt::Horizontal, tr("Summary"));
		dboper->mdlDB05->setHeaderData(11, Qt::Horizontal, tr("Prolong date"));
		dboper->mdlDB05->setHeaderData(12, Qt::Horizontal, tr("Clause"));
		dboper->mdlDB05->setHeaderData(13, Qt::Horizontal, tr("NLA"));
		dboper->mdlDB05->setHeaderData(15, Qt::Horizontal, tr("Submission date"));
		dboper->mdlDB05->setHeaderData(16, Qt::Horizontal, tr("Committee date"));
		dboper->mdlDB05->setHeaderData(17, Qt::Horizontal, tr("Result"));
		dboper->mdlDB05->setHeaderData(18, Qt::Horizontal, tr("Clauses"));
		dboper->mdlDB05->setHeaderData(19, Qt::Horizontal, tr("Order"));
		dboper->mdlDB05->setHeaderData(20, Qt::Horizontal, tr("Order executed"));
		dboper->mdlDB05->setHeaderData(21, Qt::Horizontal, tr("Execution info"));
		dboper->mdlDB05->setHeaderData(22, Qt::Horizontal, tr("Prolong"));
		dboper->mdlDB05->setHeaderData(23, Qt::Horizontal, tr("Excitation date"));
	
		ui->tvCases->setModel(dboper->mdlDB05);
	
		ui->tvCases->hideColumn(0);
		ui->tvCases->hideColumn(10);
		ui->tvCases->hideColumn(11);
		ui->tvCases->hideColumn(14);
		ui->tvCases->setItemDelegateForColumn(2, new DataDelegate());
		ui->tvCases->setItemDelegateForColumn(5, new DataDelegate());
		ui->tvCases->setItemDelegateForColumn(6, new DataDelegate());
		ui->tvCases->setItemDelegateForColumn(11, new DataDelegate());
		ui->tvCases->setItemDelegateForColumn(14, new DataDelegate());
		
		ui->tvCases->resizeColumnsToContents();
		ui->tvCases->horizontalHeader()->setStretchLastSection(true);
		ui->tvCases->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

		ui->tvCases->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
		ui->tvCases->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	}
	else if(iType==3) { //Adm cases
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Case N"));
        dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Excitation date"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("Defendant")); //
		dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Defendant address"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Clause"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Clause part"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Date of judgement"));
		dboper->mdlDB05->setHeaderData(8, Qt::Horizontal, tr("Decision"));
		dboper->mdlDB05->setHeaderData(9, Qt::Horizontal, tr("Amount"));
		dboper->mdlDB05->setHeaderData(10, Qt::Horizontal, tr("Executor"));
	
		ui->tvAdmCases->setModel(dboper->mdlDB05);
	
		ui->tvAdmCases->hideColumn(0);
		ui->tvAdmCases->setItemDelegateForColumn(2, new DataDelegate());
		
		ui->tvAdmCases->resizeColumnsToContents();
		ui->tvAdmCases->horizontalHeader()->setStretchLastSection(true);
		ui->tvAdmCases->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

		ui->tvAdmCases->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
		ui->tvAdmCases->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	}

	else if(iType==4) { //Lawsuites
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Case N"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Claim Date"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("Complainant"));
		dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Defendant"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Third persons"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Other persons"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Judge"));
		dboper->mdlDB05->setHeaderData(10, Qt::Horizontal, tr("Case card url"));
		dboper->mdlDB05->setHeaderData(11, Qt::Horizontal, tr("Result"));
		dboper->mdlDB05->setHeaderData(12, Qt::Horizontal, tr("Instance"));
		dboper->mdlDB05->setHeaderData(13, Qt::Horizontal, tr("Executor"));
	
		ui->tvLawsuites->setModel(dboper->mdlDB05);
	
		ui->tvLawsuites->hideColumn(0);
		ui->tvLawsuites->hideColumn(8);
		ui->tvLawsuites->hideColumn(9);
		ui->tvLawsuites->setItemDelegateForColumn(2, new DataDelegate());
		
		ui->tvLawsuites->resizeColumnsToContents();
		ui->tvLawsuites->horizontalHeader()->setStretchLastSection(true);
		ui->tvLawsuites->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

		ui->tvLawsuites->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
		ui->tvLawsuites->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	}
	ui->twArchive->setTabEnabled(0,dboper->UsrSettings.stkUserPermissions.bArchiveEighteenOne);
	ui->twArchive->setTabEnabled(1,dboper->UsrSettings.stkUserPermissions.bArchiveClaims);
	ui->twArchive->setTabEnabled(2,dboper->UsrSettings.stkUserPermissions.bArchiveCases);
	ui->twArchive->setTabEnabled(3,dboper->UsrSettings.stkUserPermissions.bArchiveLawSuites);
}

void wgtarchive::on_twArchive_currentChanged(int index)
{
	if(index==0) dboper->mdlDB05->setTable("eighteenone_arc");
	else if(index==1) dboper->mdlDB05->setTable("amzclaims_arc");
	else if(index==2) dboper->mdlDB05->setTable("amzcases_arc");
	else if(index==3) dboper->mdlDB05->setTable("admcases_arc");
	else if(index==4) dboper->mdlDB05->setTable("lawsuites_arc");
	dboper->mdlDB05->setSort(1,Qt::AscendingOrder);
	dboper->mdlDB05->select();
	ConnectArchiveModelToView();
}

void wgtarchive::on_tvCases_doubleClicked(const QModelIndex &index)
{
    setVisible(false);
	wArcHistory->iMode=2;
	emit SetArcHistoryFields(index.row());
    //wArcHistory->setVisible(true);
}

void wgtarchive::BackToArchive(int iMode) {
	ui->twArchive->setCurrentIndex(iMode);
	setVisible(true);
}

void wgtarchive::ConnectSlots() {
	connect(this,SIGNAL(SetArcHistoryFields(int)),wArcHistory,SLOT(SetArcHistoryFields(int)));
	connect(wArcHistory,SIGNAL(BackToArchive(int)),this,SLOT(BackToArchive(int)));
}

void wgtarchive::on_tvAdmCases_doubleClicked(const QModelIndex &index)
{
    setVisible(false);
    wArcHistory->iMode=3;
    emit SetArcHistoryFields(index.row());
}

void wgtarchive::on_tvLawsuites_doubleClicked(const QModelIndex &index)
{
    setVisible(false);
    wArcHistory->iMode=4;
    emit SetArcHistoryFields(index.row());
}
