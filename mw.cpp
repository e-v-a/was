#include "mw.h"
#include "ui_mw.h"

mw::mw(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mw)
{
    ui->setupUi(this);
	
	wInboxClaims = new wgtInboxClaims();
	ui->vlForm->addWidget(wInboxClaims);
	wInboxClaims->setVisible(false);
	connect(this,SIGNAL(SetInboxFields(int)),wInboxClaims, SLOT(SetInboxFields(int)));
	connect(wInboxClaims,SIGNAL(SendInternalMessage(QDateTime &, QString, QString, QString, QString, int)),this,
		SLOT(SendInternalMessage(QDateTime &,QString, QString, QString, QString, int)));
	connect(wInboxClaims,SIGNAL(UpdateClaimsNum(int)),this,SLOT(UpdateClaimsNum(int)));
	connect(wInboxClaims,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(wInboxClaims,SIGNAL(closeIncoming()),this,SLOT(closeIncoming()));
	connect(this,SIGNAL(clearInboxSearch()),wInboxClaims,SLOT(clearInboxSearch()));
	connect(this,SIGNAL(setAttachedOperLink()),wInboxClaims,SLOT(setAttachedOperLink()));

	wAllInboxClaims=new wgtallinboxdocs();
	ui->vlForm->addWidget(wAllInboxClaims);
	wAllInboxClaims->setVisible(false);
	connect(this,SIGNAL(ConnectModelToView()), wAllInboxClaims, SLOT(ConnectModelToView()));
	connect(this,SIGNAL(initEditActions()), wAllInboxClaims, SLOT(initEditActions()));
	connect(wAllInboxClaims,SIGNAL(PrintHTML(QString)),this,SLOT(PrintHTML(QString)));

	wDocsForAgreement = new wgtdocsforagreement();
	ui->vlForm->addWidget(wDocsForAgreement);
	wDocsForAgreement->setVisible(false);
	connect(this,SIGNAL(SetD4AFormFields(int)), wDocsForAgreement, SLOT(SetD4AFormFields(int)));
	connect(wDocsForAgreement,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(wDocsForAgreement,SIGNAL(UpdateDocsNum(int)),this,SLOT(UpdateDocsNum(int)));
	connect(this,SIGNAL(clearD4AFields()),wDocsForAgreement,SLOT(clearD4AFields()));
	connect(wDocsForAgreement,SIGNAL(closeD4Awgt()),this,SLOT(ui->pbDocsForAgreement->click()));
	
    wEO = new wgtEighteenOne();
	ui->vlForm->addWidget(wEO);
	wEO->setVisible(false);
	connect(this,SIGNAL(SetFormFields(int)), wEO, SLOT(SetFormFields(int)));
	connect(this,SIGNAL(setDBOperLink()),wEO, SLOT(setDBOperLink()));
	connect(wEO,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(this,SIGNAL(clearEOSearch()),wEO,SLOT(clearEOSearch()));

	wClaims = new wgtAMZClaims();
	ui->vlForm->addWidget(wClaims);
	wClaims->setVisible(false);
	connect(this,SIGNAL(SetClaimsFormFields(int)),wClaims,SLOT(SetClaimsFormFields(int)));
	connect(this,SIGNAL(setClaimsDBOperLink()), wClaims, SLOT(setClaimsDBOperLink()));
	connect(wClaims,SIGNAL(GoToCase(QString)),this,SLOT(GoToCase(QString)));
	connect(wClaims,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(this,SIGNAL(clearACLSearch()),wClaims,SLOT(clearACLSearch()));

	wCases = new wgtAMZCases();
	ui->vlForm->addWidget(wCases);
	wCases->setVisible(false);
	connect(this,SIGNAL(SetCasesFormFields(int)),wCases,SLOT(SetCasesFormFields(int)));
	connect(this,SIGNAL(setCasesDBOperLink()), wCases, SLOT(setCasesDBOperLink()));
	connect(wCases,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(this,SIGNAL(clearACSSearch()), wCases, SLOT(clearACSSearch()));
	connect(wCases,SIGNAL(LoadAdded(QString)),this,SLOT(LoadAdded(QString)));

	wAdmCases = new wgtAdministrativeCases();
	ui->vlForm->addWidget(wAdmCases);
	wAdmCases->setVisible(false);
	connect(this,SIGNAL(SetAdmCasesFormFields(int)),wAdmCases,SLOT(SetAdmCasesFormFields(int)));
	connect(this,SIGNAL(setACDBOperLink()), wAdmCases, SLOT(setACDBOperLink()));
	connect(wAdmCases,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(this,SIGNAL(clearADCSearch()), wAdmCases, SLOT(clearADCSearch()));
	connect(this,SIGNAL(EnableBlankFields()),wAdmCases,SLOT(EnableBlankFields()));
	connect(wAdmCases,SIGNAL(LoadAdded(QString)),this,SLOT(LoadAdded(QString)));

	wLawsuites = new wgtLawsuites();
	ui->vlForm->addWidget(wLawsuites);
	wLawsuites->setVisible(false);
	connect(this,SIGNAL(SetLSFormFields(int)),wLawsuites,SLOT(SetLSFormFields(int)));
	connect(this,SIGNAL(setLSOperLink()), wLawsuites, SLOT(setLSOperLink()));
	connect(wLawsuites,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(this,SIGNAL(clearLSSearch()), wLawsuites, SLOT(clearLSSearch()));
	connect(this,SIGNAL(EnableLSBlankFields()),wLawsuites,SLOT(EnableLSBlankFields()));
	connect(wLawsuites,SIGNAL(LoadAddedLS(QString)),this,SLOT(LoadAddedLS(QString)));

	wArchive = new wgtarchive();
	ui->vlForm->addWidget(wArchive);
	wArchive->setVisible(false);
	connect(this,SIGNAL(ConnectArchiveModelToView()), wArchive, SLOT(ConnectArchiveModelToView()));
	connect(this,SIGNAL(ConnectSlots()), wArchive, SLOT(ConnectSlots()));

	wArchiveHistory = new wgtarchivehistory();
	ui->vlForm->addWidget(wArchiveHistory);
	wArchiveHistory->setVisible(false);
	connect(this,SIGNAL(SetArchiveHistoryLinks()), wArchiveHistory,SLOT(SetArchiveHistoryLinks()));
	iQuerySize=0;
	tmrUpdateIncoming=new QTimer(this);
	tmrUpdateMessages=new QTimer(this);
	connect(tmrUpdateIncoming,SIGNAL(timeout()),SLOT(on_tmrUpdateIncoming()));

	wAdminPanel = new wgtAdmManagement();
	ui->vlForm->addWidget(wAdminPanel);
	wAdminPanel->setVisible(false);
	connect(this,SIGNAL(initAdminPanel()),wAdminPanel,SLOT(initAdminPanel()));
	connect(this,SIGNAL(setAdmPanelDBOperLink()),wAdminPanel, SLOT(setAdmPanelDBOperLink()));

	wMessages = new wgtmessages();
	ui->vlForm->addWidget(wMessages);
	wMessages->setVisible(false);
	connect(this,SIGNAL(ConnectMessagesModelToView()),wMessages,SLOT(ConnectMessagesModelToView()));
	connect(wMessages,SIGNAL(UpdateMessagesNum()),this,SLOT(on_tmrUpdateMessages()));
    connect(wMessages,SIGNAL(SendInternalMessage(QDateTime &, QString, QString, QString, QString, int)),this,
        SLOT(SendInternalMessage(QDateTime &,QString, QString, QString, QString, int)));

	fntBold.setFamily(ui->pbEighteenOne->font().family());
	fntBold.setPointSize(ui->pbEighteenOne->font().pointSize());
	fntBold.setBold(true);
	fntNormal.setFamily(ui->pbEighteenOne->font().family());
	fntNormal.setPointSize(ui->pbEighteenOne->font().pointSize());
	fntNormal.setBold(false);
	strCaseNum="";
	strAdmCaseNum="";

	QFile file(qApp->applicationDirPath() + "/styles/default/mw.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.setFileName(qApp->applicationDirPath() + "/styles/default/mw-toppanel.css");
	file.open(QFile::ReadOnly);
	styleSheet = QLatin1String(file.readAll());
	ui->wgtControl->setStyleSheet(styleSheet);

	wWelcome = new wgtwelcome();
	ui->vlForm->addWidget(wWelcome);
	wWelcome->setVisible(true);
	connect(this,SIGNAL(setUsername(QString)),wWelcome,SLOT(setUsername(QString)));
	connect(this,SIGNAL(setShedule(QString,bool)),wWelcome,SLOT(setShedule(QString,bool)));
	connect(wWelcome,SIGNAL(FillShedule(bool,bool)),this,SLOT(FillShedule(bool,bool)));

	wReport = new wgtReport();
	//ui->vlForm->addWidget(wReport);
	wReport->setVisible(false);
	connect(wReport,SIGNAL(generateReport(QString,QDate &,QDate &)),this,SLOT(generateReport(QString,QDate &,QDate &)));

    wGenReport = new wgtGeneratedReport();
    wGenReport->setVisible(false);
	connect(this,SIGNAL(showGeneratedReport(QString)),wGenReport,SLOT(showGeneratedReport(QString)));
}

mw::~mw()
{
    delete ui;
}

void mw::initMainForm() {
	dboper->mdlDB05=new QSqlTableModel(0,QSqlDatabase::database("DB05"));
	dboper->stmdlEOincomingDocs=new QSqlTableModel(0,QSqlDatabase::database("DB05"));
	dboper->stmdlEOoutcomingDocs=new QSqlTableModel(0,QSqlDatabase::database("DB05"));

	connect(this,SIGNAL(ExecQuery(const int,QString, int, QString, QString, QString, bool, QString)),dboper,SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));
	connect(this,SIGNAL(MainTableAssign(QSqlTableModel::EditStrategy, QString, QString, QString)),dboper,SLOT(MainTableAssign(QSqlTableModel::EditStrategy, QString, QString, QString)));
	connect(this,SIGNAL(setFilterMB(QString)),dboper,SLOT(setFilterMB(QString)));

	ui->pbIncoming->setVisible(true);
	ui->pbAllIncoming->setVisible(true);
	ui->pbEighteenOne->setVisible(dboper->UsrSettings.stkUserPermissions.bEighteenone);
	ui->pbAMZClaims->setVisible(dboper->UsrSettings.stkUserPermissions.bClaims);
	ui->pbAMZCases->setVisible(dboper->UsrSettings.stkUserPermissions.bCases);
	ui->pbAdmCases->setVisible(dboper->UsrSettings.stkUserPermissions.bAdmCases);
	ui->pbAddAdmCase->setVisible(dboper->UsrSettings.stkUserPermissions.bAdmCases);
	ui->pbLawsuites->setVisible(dboper->UsrSettings.stkUserPermissions.bLawSuites);
	ui->pbAddLawSuite->setVisible(dboper->UsrSettings.stkUserPermissions.bLawSuites);
	ui->pbArchive->setVisible(dboper->UsrSettings.stkUserPermissions.bArchiveEighteenOne || dboper->UsrSettings.stkUserPermissions.bArchiveClaims || dboper->UsrSettings.stkUserPermissions.bArchiveCases || dboper->UsrSettings.stkUserPermissions.bArchiveAdmCases || dboper->UsrSettings.stkUserPermissions.bArchiveLawSuites || dboper->UsrSettings.stkUserPermissions.bArchiveFASOrders);
	ui->pbReport->setVisible(dboper->UsrSettings.stkUserPermissions.bSelfReport || dboper->UsrSettings.stkUserPermissions.bFullReport);
	ui->pbDocsForAgreement->setVisible(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement);
	ui->pbAdminPanel->setVisible(dboper->UsrSettings.stkUserPermissions.bUserManagement);
	
	if(dboper->EnvSettings.bSync) {
		ui->pbSync->setChecked(true);
		StartUpdateTimer();
	}
	else ui->pbSync->setChecked(false);

	if(dboper->EnvSettings.bMWState) setWindowState(Qt::WindowMaximized);
	else resize(dboper->EnvSettings.iMWWidth,dboper->EnvSettings.iMWHeight);

	wAllInboxClaims->dboper=dboper;
	if(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming)	emit initEditActions();
	connect(wAllInboxClaims,SIGNAL(setFilterMB(QString)),dboper,SLOT(setFilterMB(QString)));
	connect(wAllInboxClaims,SIGNAL(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)),dboper,SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));

	wInboxClaims->dboper=dboper;
	emit setAttachedOperLink();
	connect(wInboxClaims,SIGNAL(setFilterMB(QString)),dboper,SLOT(setFilterMB(QString)));
	connect(wInboxClaims,SIGNAL(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)),dboper,SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));

	wEO->dboper=dboper;
	emit setDBOperLink();

	wClaims->dboper=dboper;
	emit setClaimsDBOperLink();
	connect(wClaims,SIGNAL(setFilterMB(QString)),dboper,SLOT(setFilterMB(QString)));
	connect(wClaims,SIGNAL(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)),dboper,SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));

	wCases->dboper=dboper;
	emit setCasesDBOperLink();

	wAdmCases->dboper=dboper;
	emit setACDBOperLink();

	wLawsuites->dboper=dboper;
	emit setLSOperLink();

	wAdminPanel->dboper=dboper;
	emit setAdmPanelDBOperLink();

	wDocsForAgreement->dboper=dboper;

	wArchive->dboper=dboper;
	wArchiveHistory->dboper=dboper;
	wArchive->wArcHistory=wArchiveHistory;
	emit ConnectSlots();
	emit SetArchiveHistoryLinks();

	wReport->dboper=dboper;

	dboper->mdlDB05->setTable("users");
	dboper->mdlDB05->setFilter("username='"+dboper->UsrSettings.sUserName+"'");
	dboper->mdlDB05->select();
	
	QString strBuffer;
	strBuffer=dboper->mdlDB05->record(0).value("secondname").toString();
	strBuffer+=" "+dboper->mdlDB05->record(0).value("firstname").toString();
	strBuffer+=" "+dboper->mdlDB05->record(0).value("thirdname").toString();
	
	emit setUsername(strBuffer);

	FillShedule(false,false);
	
	setVisible(true);
}

void mw::on_pbEighteenOne_clicked()
{
	if(ui->pbEighteenOne->isChecked()) {
		SetMenuControls("eo");

		QString strFilter=" ORDER BY [regdate] DESC, [regnum] DESC";
		QString strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";		
		LoadFormTable("eo",QSqlTableModel::OnRowChange, "eighteenone", strFilter, strFilterByUser);

		wEO->stlDecisionTypes.clear();
		wEO->stlDecisionTypes=stlDecisionTypes;
		wEO->stlExecutors.clear();
		wEO->stlExecutors=stlExecutors;

		wEO->iRowCount=dboper->mdlDB05->rowCount();
		wEO->iCurrentRow=0;

		if(!dboper->mdlDB05->record(0).isEmpty()) emit SetFormFields(wEO->iCurrentRow);

		wEO->setVisible(true);
	}
	else {
		ui->pbEighteenOne->setFont(fntNormal);
		wEO->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbAMZClaims_clicked()
{
	if(ui->pbAMZClaims->isChecked()) {
		SetMenuControls("amzclaims");

		QString strFilter=" ORDER BY [regdate] DESC, [regnum] DESC";
		QString strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";		
		LoadFormTable("acl",QSqlTableModel::OnRowChange, "amzclaims", strFilter, strFilterByUser);

		wClaims->stlDecisionTypes.clear();
		wClaims->stlDecisionTypes=stlDecisionTypes;
		wClaims->stlNLA.clear();
		wClaims->stlNLA=stlNLA;
		wClaims->stlExecutors.clear();
		wClaims->stlExecutors=stlExecutors;

		wClaims->iRowCount=dboper->mdlDB05->rowCount();
		wClaims->iCurrentRow=0;

		if(!dboper->mdlDB05->record(0).isEmpty()) emit SetClaimsFormFields(wClaims->iCurrentRow);

		wClaims->setVisible(true);
	}
	else {
		ui->pbAMZClaims->setFont(fntNormal);
		wClaims->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}	
}

void mw::on_pbAMZCases_clicked()
{
	if(ui->pbAMZCases->isChecked()) {
		SetMenuControls("amzcases");

		QString strFilter=" ORDER BY [claimdate] DESC, [claimnum] DESC";
		QString strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";		
		LoadFormTable("acs",QSqlTableModel::OnRowChange, "amzcases", strFilter, strFilterByUser);

		wCases->stlDecisionTypes.clear();
		wCases->stlDecisionTypes=stlDecisionTypes;
		wCases->stlNLA.clear();
		wCases->stlNLA=stlNLA;
		//wCases->stlExecutors.clear();
		//wCases->stlExecutors=stlExecutors;

		wCases->iRowCount=dboper->mdlDB05->rowCount();
		wCases->iCurrentRow=0;

		if(strCaseNum!="") {
			QString strbuffer;
			for(int i=0;i<wCases->iRowCount;i++) {
				strbuffer=dboper->mdlDB05->record(i).value("casenum").toString();
				if(dboper->mdlDB05->record(i).value("casenum").toString()==strCaseNum) {
					wCases->iCurrentRow=i;
					break;
				}
			}
		}

		if(!dboper->mdlDB05->record(0).isEmpty()) emit SetCasesFormFields(wCases->iCurrentRow);

		wCases->setVisible(true);
	}
	else {
		ui->pbAMZCases->setFont(fntNormal);
		wCases->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbAdmCases_clicked()
{
	if(ui->pbAdmCases->isChecked()) {
		SetMenuControls("admcases");

		QString strFilter=" ORDER BY [exdate] DESC, [casenum] DESC";
		QString strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";		
		LoadFormTable("adc",QSqlTableModel::OnRowChange, "admcases", strFilter, strFilterByUser);

		wAdmCases->stlDecisionTypes.clear();
		wAdmCases->stlDecisionTypes=stlDecisionTypes;
		//wCases->stlExecutors.clear();
		//wCases->stlExecutors=stlExecutors;

		wAdmCases->iRowCount=dboper->mdlDB05->rowCount();
		wAdmCases->iCurrentRow=0;

		if(strAdmCaseNum!="") {
			QString strbuffer;
			for(int i=0;i<wAdmCases->iRowCount;i++) {
				strbuffer=dboper->mdlDB05->record(i).value("casenum").toString();
				if(dboper->mdlDB05->record(i).value("casenum").toString()==strAdmCaseNum) {
					wAdmCases->iCurrentRow=i;
					break;
				}
			}
		}
		if(!dboper->mdlDB05->record(0).isEmpty()) emit SetAdmCasesFormFields(wAdmCases->iCurrentRow);
	}
	else {
		ui->pbAdmCases->setFont(fntNormal);
		wAdmCases->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbLawsuites_clicked()
{
	if(ui->pbLawsuites->isChecked()) {
		SetMenuControls("lawsuites");

		QString strFilter=" ORDER BY [claimdate] DESC, [casenum] DESC";
		QString strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";		
		LoadFormTable("ls",QSqlTableModel::OnRowChange, "lawsuites", strFilter, strFilterByUser);

		wLawsuites->stlDecisionTypes.clear();
		wLawsuites->stlDecisionTypes=stlDecisionTypes;
		wLawsuites->stlInstances.clear();
		wLawsuites->stlInstances=stlInstances;
//		wLawsuites->stlExecutors.clear();
//		wLawsuites->stlExecutors=stlExecutors;

		wLawsuites->iRowCount=dboper->mdlDB05->rowCount();
		wLawsuites->iCurrentRow=0;

		if(strLSCaseNum!="") {
			QString strbuffer;
			for(int i=0;i<wLawsuites->iRowCount;i++) {
				strbuffer=dboper->mdlDB05->record(i).value("casenum").toString();
				if(dboper->mdlDB05->record(i).value("casenum").toString()==strLSCaseNum) {
					wLawsuites->iCurrentRow=i;
					break;
				}
			}
		}
		if(!dboper->mdlDB05->record(0).isEmpty()) emit SetLSFormFields(wLawsuites->iCurrentRow);
	}
	else {
		ui->pbLawsuites->setFont(fntNormal);
		wLawsuites->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbReport_clicked()
{
	//if(ui->pbReport->isChecked()) {
		wReport->setVisible(true);
		//SetMenuControls("report");
	//}
}

void mw::on_pbAdminPanel_clicked()
{
	if(ui->pbAdminPanel->isChecked()) {
		SetMenuControls("adminpanel");
		dboper->mdlDB05->setTable("users");
		dboper->mdlDB05->select();
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("User name"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Second name"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("First name"));
		dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Third name"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Password hash"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Position"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Permissions"));
		dboper->stmdlEOincomingDocs->setTable("executors");
		dboper->stmdlEOincomingDocs->select();
		dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Executor"));
		dboper->stmdlEOoutcomingDocs->setTable("attachedtotype");
		dboper->stmdlEOoutcomingDocs->select();
		dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Attachment type"));
		emit initAdminPanel();
	}
	else {
		ui->pbAdminPanel->setFont(fntNormal);
		wAdminPanel->setVisible(false);
		SetMenuControls("shedule");
	}
}

void mw::on_tmrUpdateIncoming() {
	QString strBuffer;
	QString strFilterString;
	if(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement) {
		QString strFields;
		QString strFilter;

		int iProgress=0;

		strFields="["+tr("EntryDate")+"],";
		strFields+="["+tr("RegNum")+"],";
		strFields+="["+tr("RegDate")+"],";
		strFields+="["+tr("CitizenAppeal")+"],";
		strFields+="["+tr("SendWay")+"],";
		strFields+="["+tr("OutcomeNum")+"],";
		strFields+="["+tr("OutcomeDate")+"],";
		strFields+="["+tr("Addressee")+"],";
		strFields+="["+tr("PostIndex")+"],";
		strFields+="["+tr("Region")+"],";
		strFields+="["+tr("Address")+"],";
		strFields+="["+tr("DocType")+"],";
		strFields+="["+tr("Summary")+"],";
		strFields+="["+tr("DepNum1")+"],";
		strFields+="["+tr("DepNum2")+"],";
		strFields+="["+tr("DepNum3")+"]";
	
		QString strStartDate;
		if(QDate::currentDate().month()>2) strStartDate=QDate(QDate::currentDate().year(),QDate::currentDate().month()-2,1).toString("yyyyMMdd");
		else if(QDate::currentDate().month()==1) strStartDate=QDate(QDate::currentDate().year()-1,11,1).toString("yyyyMMdd");
		else if(QDate::currentDate().month()==2) strStartDate=QDate(QDate::currentDate().year()-1,12,1).toString("yyyyMMdd");
		
		QString strEndDate=QDate::currentDate().toString("yyyyMMdd");

		strFilter="(["+tr("DepNum1")+"]='01'" + " OR [" + tr("DepNum2")+"]='01'" + " OR [" + tr("DepNum3")+"]='01') AND ([" + tr("RegDate") + "] BETWEEN '" + strStartDate + "' AND '" + strEndDate +"')";

		dboper->dbQuery(TIMER_QUERY,"officeDB",0,"count(*)","","IncomingList",true,strFilter);
		dboper->TimerQuery.next();

		QProgressDialog pdIncomingAffected(this);
		pdIncomingAffected.setWindowModality(Qt::WindowModal);
		pdIncomingAffected.setCancelButton(0);

		int iNewCount=dboper->TimerQuery.value(0).toInt();
		pdIncomingAffected.setLabelText(tr("New incoming documents processing..."));
		pdIncomingAffected.setValue(iProgress);
		pdIncomingAffected.setMaximum(iNewCount);

		dboper->dbQuery(TIMER_QUERY,"officeDB",0,strFields,"","IncomingList",true,strFilter);
		QDate dtBuffer;
				
		while(dboper->TimerQuery.next()) {
			strFilterString="[regnum]=";
			strFilterString+=dboper->TimerQuery.value(tr("RegNum")).toString();
			strFilterString+=" AND [regdate]=#";
			dtBuffer=dboper->TimerQuery.value(tr("RegDate")).toDate();
			strFilterString+=dtBuffer.toString("yyyy-MM-dd");
			strFilterString+="#";
			
			dboper->dbQuery(TIMER_LOCAL_QUERY,"DB05",0,"count(*)","","incoming",true,strFilterString);
			dboper->TimerLocalQuery.next();
			if(dboper->TimerLocalQuery.value(0).toInt() == 0) {
				strBuffer=dboper->TimerQuery.value(tr("RegNum")).toString()+","; //regnum	str
				strBuffer+="#"+dboper->TimerQuery.value(tr("RegDate")).toDate().toString("yyyy-MM-dd") + "#,"; // regdate	date
				if (dboper->TimerQuery.value(tr("CitizenAppeal")).toBool()==true) strBuffer+="1,"; // citizenappeal	bool
				else strBuffer+="0,"; // citizenappeal	bool
				if(dboper->TimerQuery.value(tr("EntryDate")).toDate().toString("yyyy-MM-dd")!="")
					strBuffer+="#"+dboper->TimerQuery.value(tr("EntryDate")).toDate().toString("yyyy-MM-dd")+"#,"; // entrydate		date
				strBuffer+="'"+dboper->TimerQuery.value(tr("SendWay")).toString()+"',"; // sendway	str
				strBuffer+="'"+dboper->TimerQuery.value(tr("OutcomeNum")).toString()+"',"; // outcomenum	str
				if(dboper->TimerQuery.value(tr("OutcomeDate")).toDate().toString("yyyy-MM-dd")!="")
					strBuffer+="#"+dboper->TimerQuery.value(tr("OutcomeDate")).toDate().toString("yyyy-MM-dd")+"#,"; // outcomedate		date
				strBuffer+="'"+dboper->TimerQuery.value(tr("Addressee")).toString()+"',"; //addressee	str
				strBuffer+="'"+dboper->TimerQuery.value(tr("PostIndex")).toString()+", " + dboper->TimerQuery.value(tr("Region")).toString() +", " + dboper->TimerQuery.value(tr("Address")).toString()+"',"; // destination		str
				strBuffer+="'"+dboper->TimerQuery.value(tr("DocType")).toString()+"',"; // doctype	str
				strBuffer+="'"+dboper->TimerQuery.value(tr("Summary")).toString()+"'"; // summary	str

				strFields="[regnum],[regdate],[citizenappeal],";
				if(dboper->TimerQuery.value(tr("EntryDate")).toDate().toString("yyyy-MM-dd")!="")
					strFields+="[entrydate],";
				strFields+="[sendway],[outcomenum]";
				if(dboper->TimerQuery.value(tr("OutcomeDate")).toDate().toString("yyyy-MM-dd")!="") 
					strFields+=",[outcomedate]";
				strFields+=",[addressee],[destination],[doctype],[summary]";
				dboper->dbQuery(PRIVATE_QUERY,"DB05",1, strFields, strBuffer,"incoming",false,"");
			}
			dboper->TimerLocalQuery.finish();
			dboper->TimerLocalQuery=QSqlQuery();
			iProgress++;
			pdIncomingAffected.setValue(iProgress);
		}
		dboper->TimerQuery.finish();
		dboper->TimerQuery=QSqlQuery();
		strFilterString="([executor] is null OR [executor] = '') AND ([seddoctype] is null OR [seddoctype] NOT LIKE '" + tr("Wrong department")+"')";// OR ([executor] = '" + usrCurrentUser->sName + "' AND [attachedtodocid] = 0)";
		//strFilterString="[executor] is null OR [executor] = '" + usrCurrentUser->sName + "' AND [attachedtodocid] is null";
	}
	else strFilterString="[executor] = '"+dboper->UsrSettings.sUserName+"' AND [attachedtodocid] = 0";

	dboper->dbQuery(TIMER_LOCAL_QUERY,"DB05",0,"count(*)","","incoming",true,strFilterString);
	dboper->TimerLocalQuery.next();
	strFilterString.setNum(dboper->TimerLocalQuery.value(0).toInt());
	dboper->TimerLocalQuery.finish();
	dboper->TimerLocalQuery=QSqlQuery();
	strBuffer=QString(tr("Inbox documents") + " (%1)").arg(strFilterString);
	ui->pbIncoming->setText(strBuffer);
}

void mw::StartUpdateTimer() {
	on_tmrUpdateIncoming();
	if(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement) tmrUpdateIncoming->start(3600000);
	else tmrUpdateIncoming->start(600000);

	on_tmrUpdateMessages();
	tmrUpdateMessages->start(300000);
}

void mw::on_pbIncoming_clicked()
{
	if(ui->pbIncoming->isChecked()) {
		SetMenuControls("incoming");

		QString strFilter;
		QString strFilterByUser;

		wInboxClaims->stlDoctype.clear();
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","seddoctype",false,"");
		while(dboper->query.next()) wInboxClaims->stlDoctype.append(dboper->query.value("seddoctype").toString());
		dboper->query.finish();

		wInboxClaims->stlExecutor.clear();
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","executors",false,"");
		while(dboper->query.next()) wInboxClaims->stlExecutor.append(dboper->query.value("executor").toString());
		
		wInboxClaims->stlAttachedtotype.clear();
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","attachedtotype",false,"");
		while(dboper->query.next()) wInboxClaims->stlAttachedtotype.append(dboper->query.value("attachedtotype").toString());

		if(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming) {
			strFilterByUser="([executor] is null OR [executor] = '') AND ([seddoctype] is null OR [seddoctype] NOT LIKE '" + tr("Wrong department")+"')";
		}
		else strFilterByUser="[executor] = '" + dboper->UsrSettings.sUserName + "' AND [attachedtodocid] = 0";
		strFilter=" ORDER BY [regdate] DESC, [regnum] DESC";

		emit MainTableAssign(QSqlTableModel::OnRowChange, "incoming", strFilter, strFilterByUser);

		wInboxClaims->iRowCount=dboper->mdlDB05->rowCount();
		setStatusText(1,dboper->mdlDB05->rowCount());
		wInboxClaims->iCurrentRow=0;

		if(!dboper->mdlDB05->record(0).isEmpty()) {
			emit SetInboxFields(wInboxClaims->iCurrentRow);
			setStatusText(wInboxClaims->iCurrentRow+1, wInboxClaims->iRowCount);
		}
	
		wInboxClaims->setVisible(true);
	}
	else {
		ui->pbIncoming->setFont(fntNormal);
		wInboxClaims->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbAllIncoming_clicked()
{
	if(ui->pbAllIncoming->isChecked()) {
		SetMenuControls("allincoming");
		dboper->mdlDB05->setTable("incoming");
		dboper->mdlDB05->setSort(1,Qt::AscendingOrder);
		if(!dboper->UsrSettings.stkUserPermissions.bAllIncoming) {
			QString strFilter="[executor] = '" + dboper->UsrSettings.sUserName + "'";
			dboper->mdlDB05->setFilter(strFilter);
		}
		dboper->mdlDB05->select();

		emit ConnectModelToView();
		wAllInboxClaims->setVisible(true);
	}
	else {
		ui->pbAllIncoming->setFont(fntNormal);
		wAllInboxClaims->setVisible(false);
		SetMenuControls("shedule");
	}
}

void mw::SendInternalMessage(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage, QString strAttachedType, int iAttachedDocId) 
{
		
/*	QString strBuffer;
	QString strBuffer2;
	
	strBuffer="#";
	strBuffer+=dtSendDateTime.toString("yyyy-MM-dd hh:mm:ss");
	strBuffer+="#,'";
	strBuffer+=usrCurrentUser->sName;
	strBuffer+="','";
	strBuffer+=strTo;
	strBuffer+="','";
	strBuffer+=strSubject;
	strBuffer+="','";
	strBuffer+=strMessage;
	strBuffer+="','";
	strBuffer+=strAttachedType;
	strBuffer+="',";
	strBuffer2.setNum(iAttachedDocId);
	strBuffer+=strBuffer2;
			
	dbQuery("DB05",1,"[senddatetime],[from],[to],[subject],[message],[attachmenttype],[attachmentid]", strBuffer,"messages",false,"");
	*/

}

void mw::on_pbNotifications_clicked()
{

}

void mw::on_pbMailBox_clicked()
{
/*	if(ui->pbMailBox->isChecked()) {
		SetMenuControls("mailbox");

		wMessages->strCurrentUser=dboper->UsrSettings.sUserName;

		wMessages->stlUsers.clear();
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","users",false,"");
		while(dboper->query.next()) wMessages->stlUsers.append(query.value("username").toString());
		dboper->query.finish();

		emit ConnectMessagesModelToView();
	}
	else wMessages->setVisible(false);
	*/
}

void mw::on_pbCalendar_clicked()
{

}

void mw::on_pbSettings_clicked()
{

}

void mw::on_pbArchive_clicked()
{
	if(ui->pbArchive->isChecked()) {
		SetMenuControls("archive");

		emit ConnectArchiveModelToView();
		wArchive->setVisible(true);
	}
	else {
		ui->pbArchive->setFont(fntNormal);
		wArchive->setVisible(false);
		wArchiveHistory->setVisible(false);
		SetMenuControls("shedule");
	}
}

void mw::setStatusText(int iCurrentRow, int iRowsCount) {
	QString sBuffer;
	QString sStatusMessage;
	sBuffer.setNum(iCurrentRow);
	sStatusMessage=tr("current row: ");
	sStatusMessage+=sBuffer;
	sStatusMessage+=" / "+tr("rows count: ");
	sBuffer.setNum(iRowsCount);
	sStatusMessage+=sBuffer;
	ui->sbStatus->showMessage(sStatusMessage);
}

void mw::changeEvent(QEvent* e)
{
	QWidget::changeEvent(e);
	QString strBuffer;
	QSqlQuery query;
    switch (e->type()) {
	case QEvent::WindowStateChange:
		if(windowState()==Qt::WindowMaximized) {
			dboper->EnvSettings.bMWState=true;
			strBuffer="1";
		}
		else {
			dboper->EnvSettings.bMWState=false;
			strBuffer="0";
		}
		dboper->dbQuery(PRIVATE_QUERY,"dbsettings",2,"mwstate",strBuffer,"main",false,"");
        break;
    default:
        break;
    }
}

void mw::resizeEvent(QResizeEvent* e) 
{
	QString strBuffer;
	QString strBuffer2;
	QSqlQuery query;
	int iWidth=0;
	int iHeight=0;
	iWidth=width();
	iHeight=height();
	strBuffer.setNum(iWidth);
	strBuffer2.setNum(iHeight);
	strBuffer+="$"+strBuffer2;
	dboper->dbQuery(PRIVATE_QUERY,"dbsettings",2,"mwwidth$mwheight",strBuffer,"main",false,"");
}

void mw::UpdateClaimsNum(int iClaimsNum) {
	QString strBuffer;
	strBuffer.setNum(iClaimsNum);
	ui->pbIncoming->setText(QString(tr("Inbox documents") + " (%1)").arg(strBuffer));
}

void mw::on_tmrUpdateMessages() {
	QString strFilterString;

	strFilterString="[to] = '"+dboper->UsrSettings.sUserName+"' AND [read] = false";

	dboper->dbQuery(TIMER_MESSAGES_QUERY,"DB05",0,"count(*)","","messages",true,strFilterString);
	dboper->TimerMessagesQuery.next();
	if(dboper->TimerMessagesQuery.value(0).toInt()>0) {
		strFilterString.setNum(dboper->TimerMessagesQuery.value(0).toInt());
		ui->pbMailBox->setText(strFilterString);
	}
	else ui->pbMailBox->setText("");
	dboper->TimerMessagesQuery.finish();
	dboper->TimerMessagesQuery=QSqlQuery();
	//docs 4 agreement
	if(dboper->UsrSettings.iUserPermissions>0) {
		strFilterString="[senttoagreement] = true AND [agreed] = false";
		dboper->dbQuery(TIMER_MESSAGES_QUERY,"DB05",0,"count(*)","","documents",true,strFilterString);
		dboper->TimerMessagesQuery.next();

		strFilterString.setNum(dboper->TimerMessagesQuery.value(0).toInt());
		ui->pbDocsForAgreement->setText(QString(tr("Documents for agreement") +" (%1)").arg(strFilterString));

		dboper->TimerMessagesQuery.finish();
		dboper->TimerMessagesQuery=QSqlQuery();
	}
}

void mw::GoToCase(QString casenum) {
	strCaseNum=casenum;
	ui->pbAMZCases->click();
}

void mw::SetMenuControls(QString buttonname) {
	ui->pbDocsForAgreement->setChecked(buttonname=="d4a");
	ui->pbEighteenOne->setChecked(buttonname=="eo");
	ui->pbAMZClaims->setChecked(buttonname=="amzclaims");
	ui->pbAMZCases->setChecked(buttonname=="amzcases");
	ui->pbIncoming->setChecked(buttonname=="incoming");
	ui->pbAllIncoming->setChecked(buttonname=="allincoming");
	ui->pbArchive->setChecked(buttonname=="archive");
	ui->pbMailBox->setChecked(buttonname=="mailbox");
	ui->pbAdmCases->setChecked(buttonname=="admcases");
	ui->pbLawsuites->setChecked(buttonname=="lawsuites");
	ui->pbAdminPanel->setChecked(buttonname=="adminpanel");
	//ui->pbReport->setChecked(buttonname=="report");

	(ui->pbIncoming->isChecked())?ui->pbIncoming->setFont(fntBold):ui->pbIncoming->setFont(fntNormal);
	(ui->pbAllIncoming->isChecked())?ui->pbAllIncoming->setFont(fntBold):ui->pbAllIncoming->setFont(fntNormal);
	(ui->pbArchive->isChecked())?ui->pbArchive->setFont(fntBold):ui->pbArchive->setFont(fntNormal);
	(ui->pbDocsForAgreement->isChecked())?ui->pbDocsForAgreement->setFont(fntBold):ui->pbDocsForAgreement->setFont(fntNormal);
	(ui->pbEighteenOne->isChecked())?ui->pbEighteenOne->setFont(fntBold):ui->pbEighteenOne->setFont(fntNormal);
	(ui->pbAMZClaims->isChecked())?ui->pbAMZClaims->setFont(fntBold):ui->pbAMZClaims->setFont(fntNormal);
	(ui->pbAMZCases->isChecked())?ui->pbAMZCases->setFont(fntBold):ui->pbAMZCases->setFont(fntNormal);
	(ui->pbAdmCases->isChecked())?ui->pbAdmCases->setFont(fntBold):ui->pbAdmCases->setFont(fntNormal);
	(ui->pbLawsuites->isChecked())?ui->pbLawsuites->setFont(fntBold):ui->pbLawsuites->setFont(fntNormal);
	(ui->pbAdminPanel->isChecked())?ui->pbAdminPanel->setFont(fntBold):ui->pbAdminPanel->setFont(fntNormal);
	//(ui->pbReport->isChecked())?ui->pbReport->setFont(fntBold):ui->pbReport->setFont(fntNormal);

	ui->sbStatus->clearMessage();

	wDocsForAgreement->setVisible(false);
	wEO->setVisible(false);
	wClaims->setVisible(false);
	wCases->setVisible(false);
	wInboxClaims->setVisible(false);
	wAllInboxClaims->setVisible(false);
	wMessages->setVisible(false);
	wArchive->setVisible(false);
	wArchiveHistory->setVisible(false);
	wAdmCases->setVisible(false);
	wLawsuites->setVisible(false);
	wAdminPanel->setVisible(false);
	wWelcome->setVisible(false);
	//wReport->setVisible(false);

	emit clearInboxSearch();
	emit clearEOSearch();
	emit clearACLSearch();
	emit clearACSSearch();
	emit clearADMSearch();
	emit clearLSSearch();

	wDocsForAgreement->setVisible(ui->pbDocsForAgreement->isChecked());
	wEO->setVisible(ui->pbEighteenOne->isChecked());
	wClaims->setVisible(ui->pbAMZClaims->isChecked());
	wCases->setVisible(ui->pbAMZCases->isChecked());
	wInboxClaims->setVisible(ui->pbIncoming->isChecked());
	wAllInboxClaims->setVisible(ui->pbAllIncoming->isChecked());
	wArchive->setVisible(ui->pbArchive->isChecked());
	wMessages->setVisible(ui->pbMailBox->isChecked());
	wAdmCases->setVisible(ui->pbAdmCases->isChecked());
	wLawsuites->setVisible(ui->pbLawsuites->isChecked());
	wAdminPanel->setVisible(ui->pbAdminPanel->isChecked());
	//wReport->setVisible(ui->pbReport->isChecked());
	wWelcome->setVisible(!ui->pbDocsForAgreement->isChecked() && !ui->pbEighteenOne->isChecked() && !ui->pbAMZClaims->isChecked() && !ui->pbAMZCases->isChecked() && !ui->pbIncoming->isChecked() && !ui->pbAllIncoming->isChecked() && !ui->pbArchive->isChecked() && !ui->pbMailBox->isChecked() && !ui->pbAdmCases->isChecked() && !ui->pbLawsuites->isChecked() && !ui->pbAdminPanel->isChecked() /*&& !ui->pbReport->isChecked()*/ && buttonname!="none");
	if(!ui->pbDocsForAgreement->isChecked() && !ui->pbEighteenOne->isChecked() && !ui->pbAMZClaims->isChecked() && !ui->pbAMZCases->isChecked() && !ui->pbIncoming->isChecked() && !ui->pbAllIncoming->isChecked() && !ui->pbArchive->isChecked() && !ui->pbMailBox->isChecked() && !ui->pbAdmCases->isChecked() && !ui->pbLawsuites->isChecked()&& !ui->pbAdminPanel->isChecked() /*&& !ui->pbReport->isChecked()*/ && buttonname!="none") emit FillShedule(false,false);
}

void mw::closeIncoming() {
	ui->pbIncoming->click();
}

void mw::on_pbDocsForAgreement_clicked()
{
	if(ui->pbDocsForAgreement->isChecked()) {
		SetMenuControls("d4a");

		dboper->stmdlEOoutcomingDocs->setTable("documents");
		dboper->stmdlEOoutcomingDocs->setFilter("[senttoagreement] = true AND [agreed] = false");
		dboper->stmdlEOoutcomingDocs->select();

		wDocsForAgreement->iRowCount=dboper->stmdlEOoutcomingDocs->rowCount();

		QString sBuffer;
		QString sStatusMessage;
		sBuffer.setNum(1);
		sStatusMessage=tr("current row: ");
		sStatusMessage+=sBuffer;
		sStatusMessage+=" / "+tr("rows count: ");
		sBuffer.setNum(wDocsForAgreement->iRowCount);
		sStatusMessage+=sBuffer;
		ui->sbStatus->showMessage(sStatusMessage);

		wDocsForAgreement->iCurrentRow=0;

		if(!dboper->stmdlEOoutcomingDocs->record(0).isEmpty()) emit SetD4AFormFields(wDocsForAgreement->iCurrentRow);

		sBuffer.setNum(wDocsForAgreement->iRowCount);
		ui->pbDocsForAgreement->setText(QString(tr("Documents for agreement") +" (%1)").arg(sBuffer));
	}
	else {
		emit clearD4AFields();
		ui->pbDocsForAgreement->setFont(fntNormal);
		wDocsForAgreement->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::UpdateDocsNum(int iRowsCount) {
	QString strBuffer;
	strBuffer.setNum(iRowsCount);
	ui->pbIncoming->setText(QString(tr("Documents for agreement") + " (%1)").arg(strBuffer));
}

void mw::closeEvent(QCloseEvent *event)
 {
	wDocsForAgreement->setVisible(false);
	wEO->setVisible(false);
	wClaims->setVisible(false);
	wCases->setVisible(false);
	wInboxClaims->setVisible(false);
	wAllInboxClaims->setVisible(false);
	wMessages->setVisible(false);

	tmrUpdateIncoming->stop();
	tmrUpdateMessages->stop();

	dboper->query=QSqlQuery();
	dboper->TimerQuery=QSqlQuery();
	dboper->TimerMessagesQuery=QSqlQuery();
	dboper->TimerLocalQuery=QSqlQuery();
	dboper->mdlSettings->clear();
	dboper->mdlDB05->clear();
	dboper->stmdlEOincomingDocs->clear();
	dboper->stmdlEOoutcomingDocs->clear();
	dboper->dbCloseConn("UPDATEDB");
	dboper->dbCloseConn("DB05");
	dboper->dbCloseConn("officeDB");
	dboper->dbCloseConn("dbsettings");

    event->accept();
 }

void mw::LoadAdded(QString admcasenum) {
	strAdmCaseNum=admcasenum;
	ui->pbAdmCases->click();
}

void mw::on_pbAddAdmCase_clicked()
{
	SetMenuControls("none");
	wAdmCases->stlDecisionTypes.clear();
	dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","decisions",true,"form='adc'");
	while(dboper->query.next()) wAdmCases->stlDecisionTypes.append(dboper->query.value("decisiontype").toString());
	dboper->query.finish();
	
	dboper->mdlDB05->setEditStrategy(QSqlTableModel::OnRowChange);
	dboper->mdlDB05->setTable("admcases");
		
	emit EnableBlankFields();
	wAdmCases->bIsNew=true;
	wAdmCases->setVisible(true);
}

void mw::on_pbAddLawSuite_clicked()
{
	SetMenuControls("none");
	wLawsuites->stlDecisionTypes.clear();
	dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","decisions",true,"form='ls'");
	while(dboper->query.next()) wLawsuites->stlDecisionTypes.append(dboper->query.value("decisiontype").toString());
	dboper->query.finish();

	wLawsuites->stlInstances.clear();
	dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","instances",false,"");
	while(dboper->query.next()) wLawsuites->stlInstances.append(dboper->query.value("instance").toString());
	dboper->query.finish();

	dboper->mdlDB05->setEditStrategy(QSqlTableModel::OnRowChange);
	dboper->mdlDB05->setTable("lawsuites");

	emit EnableLSBlankFields();
	wLawsuites->bIsNew=true;
	wLawsuites->setVisible(true);
}

void mw::LoadAddedLS(QString lscasenum) {
	strLSCaseNum=lscasenum;
	ui->pbLawsuites->click();
}

void mw::on_pbRefresh_clicked()
{
    on_tmrUpdateIncoming();
}

void mw::on_pbSync_toggled(bool checked)
{
	QString strBuffer;
    dboper->EnvSettings.bSync=checked;
    if(checked) {
		strBuffer="1";
		StartUpdateTimer();
	}
    else {
		strBuffer="0";
        tmrUpdateIncoming->stop();
        tmrUpdateMessages->stop();
    }
	dboper->dbQuery(PRIVATE_QUERY,"dbsettings",2,"sync",strBuffer,"main",false,"");
}

void mw::FillShedule(bool bShort,bool bPrint) {
// ������ �������� ��������� ����������
	QString strBuffer;
	QStringList stlCommettees;
	QStringList stlLawsuites;
	QStringList stlExpiringClaims;
	QStringList stlExpiringCases;
	QStringList stlExpiringAdmCases;
	
	QList<stkShedule> lstNotifications;
	stkShedule shedNotificationBuffer;
	QString strFilter;

	int iCountOfForwardDays=21;
	if(!bShort) {
		strFilter="(((regdate "; 
		strFilter+="BETWEEN #"+QDate().currentDate().addMonths(-1).toString("MM/dd/yyyy")+"# AND #"+QDate().currentDate().addMonths(-1).addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#)";
		strFilter+=" AND (prolong=False)) OR ((prolongdate BETWEEN #"+ QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#) AND (prolong=True))) ";
		if(!(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement))
			strFilter+="AND ([executor] = '"+dboper->UsrSettings.sUserName+"')";
	
		dboper->mdlDB05->setTable("amzclaims");

		dboper->mdlDB05->setFilter(strFilter);
		dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount()>0)  {
			for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
				shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("regnum").toString();
				shedNotificationBuffer.bExpired=true;
				shedNotificationBuffer.strNotificationText="";
				if(dboper->mdlDB05->record(i).value("complainant").toString()!="")
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("complainant").toString()+" ";
				if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
					shedNotificationBuffer.strNotificationText+="/ ";
				if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("defendant").toString()+" ";
				shedNotificationBuffer.strNotificationText+=tr("expires at ");
				if(dboper->mdlDB05->record(i).value("prolong").toBool()) {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("prolongdate").toDate().toString("dd.MM.yyyy")+" "+tr("(after prolongation)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("prolongdate").toDate();
				}
				else {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("regdate").toDate().addMonths(1).toString("dd.MM.yyyy")+" " + tr("(1st month)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("regdate").toDate().addMonths(1);
				}
				shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
				lstNotifications.append(shedNotificationBuffer);
			}
		}
	}
	dboper->mdlDB05->setTable("amzcases");
	
	if(!bShort) {
		strFilter="(((excitationdate ";
		strFilter+="BETWEEN #"+QDate().currentDate().addMonths(-3).toString("MM/dd/yyyy")+"# AND #"+QDate().currentDate().addMonths(-3).addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#)";
		strFilter+=" AND (prolong=False)) OR ((prolongdate  BETWEEN #"+ QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#) AND (prolong=True))) ";
			
		if(!(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement))
			strFilter+="AND ([executor] = '"+dboper->UsrSettings.sUserName+"')";

		dboper->mdlDB05->setFilter(strFilter);
		dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount()>0)  {
			for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
				shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
				shedNotificationBuffer.bExpired=true;
				shedNotificationBuffer.strNotificationText="";
				if(dboper->mdlDB05->record(i).value("complainant").toString()!="")
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("complainant").toString()+" ";
				if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
					shedNotificationBuffer.strNotificationText+="/ ";
				if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("defendant").toString()+" ";

				shedNotificationBuffer.strNotificationText+=tr("expires at ");
				if(dboper->mdlDB05->record(i).value("prolong").toBool()) {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("prolongdate").toDate().toString("dd.MM.yyyy")+" "+tr("(after prolongation)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("prolongdate").toDate();
				}
				else {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("excitationdate").toDate().addMonths(3).toString("dd.MM.yyyy")+" " + tr("(3d month)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("excitationdate").toDate().addMonths(3);
				}
				shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
				lstNotifications.append(shedNotificationBuffer);
			}
		}
	}
	strFilter="committeedate BETWEEN #"+QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#";

	dboper->mdlDB05->setFilter(strFilter);
	dboper->mdlDB05->select();
		
	if(dboper->mdlDB05->rowCount()>0)  {
		for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
			shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
			shedNotificationBuffer.bExpired=false;
			shedNotificationBuffer.strNotificationText="";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="")
				shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("complainant").toString()+" ";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+="/ ";
			if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("defendant").toString()+" ";
			shedNotificationBuffer.strNotificationText+=tr("committee")+" ("+dboper->mdlDB05->record(i).value("nla").toString()+").";
			shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("committeedate").toDateTime().date();
			shedNotificationBuffer.tmTimeOfEvent=dboper->mdlDB05->record(i).value("committeedate").toDateTime().time();
			
			shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
			lstNotifications.append(shedNotificationBuffer);
		}
	}

	dboper->mdlDB05->setTable("lawsuites");
	strFilter="trialdatetime BETWEEN #"+QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#";

	dboper->mdlDB05->setFilter(strFilter);
	dboper->mdlDB05->select();
		
	if(dboper->mdlDB05->rowCount()>0)  {
		for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
			shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
			shedNotificationBuffer.bExpired=false;
			shedNotificationBuffer.strNotificationText="";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="")
				shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("complainant").toString()+" ";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+="/ ";
			if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("defendant").toString()+" ";
			shedNotificationBuffer.strNotificationText+="("+tr("judge:")+" "+dboper->mdlDB05->record(i).value("judge").toString()+").";
			shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("trialdatetime").toDateTime().date();
			shedNotificationBuffer.tmTimeOfEvent=dboper->mdlDB05->record(i).value("trialdatetime").toDateTime().time();
			
			shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
			lstNotifications.append(shedNotificationBuffer);
		}
	}
	
	/*dboper->mdlDB05->setTable("admcases");
	strFilter="dateofjudg BETWEEN #"+QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#";

	dboper->mdlDB05->setFilter(strFilter);
	dboper->mdlDB05->select();
		
	if(dboper->mdlDB05->rowCount()>0)  {
		for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
			shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
			shedNotificationBuffer.bExpired=false;
			shedNotificationBuffer.strNotificationText="";
			if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("defendant").toString()+" ";
			shedNotificationBuffer.strNotificationText+="("+tr("clause:")+dboper->mdlDB05->record(i).value("clause").toString();
			shedNotificationBuffer.strNotificationText+=tr("clause part:")+dboper->mdlDB05->record(i).value("clausepart").toString()+").";
			shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("dateofjudg").toDateTime().date();
			shedNotificationBuffer.tmTimeOfEvent=dboper->mdlDB05->record(i).value("dateofjudg").toDateTime().time();
			
			shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
			lstNotifications.append(shedNotificationBuffer);
		}
	}
	*/

	if(!lstNotifications.isEmpty()) {
		for(int i=0;i<lstNotifications.size();i++) {
			for(int l=i;l<lstNotifications.size();l++) {
				if(lstNotifications.at(i).dtDateOfEvent>lstNotifications.at(l).dtDateOfEvent) lstNotifications.move(l,i);
				else if(lstNotifications.at(i).dtDateOfEvent==lstNotifications.at(l).dtDateOfEvent)
					if(lstNotifications.at(i).tmTimeOfEvent==lstNotifications.at(l).tmTimeOfEvent) lstNotifications.move(l,i);
			}
		}
	}

	QString strSheduleHtml="";
	
	if(!lstNotifications.isEmpty()) {
		strSheduleHtml+="<div style='font-size: 15pt; font-weight: bold; color:#55779B'>" + lstNotifications.at(0).dtDateOfEvent.toString("MMMM") +" "+ lstNotifications.at(0).dtDateOfEvent.toString("yyyy") +"</div><p /><table style='border-spacing:0;border-collapse:collapse;width:100%' cellpadding=0 cellspacing=0>"; //������ cellpadding/cellspacing ����� ����� ��������� ��������� ��������������� ������ css
		switch (lstNotifications.at(0).dtDateOfEvent.dayOfWeek()) {
			case 1:
				strBuffer=tr("Mon");
			break;
			case 2:
				strBuffer=tr("Tue");
			break;
			case 3:
				strBuffer=tr("Wed");
			break;
			case 4:
				strBuffer=tr("Thu");
			break;
			case 5:
				strBuffer=tr("Fri");
			break;
			case 6:
				strBuffer=tr("Sat");
			break;
			case 7:
				strBuffer=tr("Sun");
			break;
		}
		strSheduleHtml+="<tbody><tr><th style='padding: 5px 5px;background-color: #55779B;color:white;font-size:11pt;font-weight: bold'>"+lstNotifications.at(0).dtDateOfEvent.toString("dd")+" "+"<br /><span style='font-size:9pt; font-weight: normal;'>"+strBuffer+"</span></th>";
		bool bOdd=false;
		for(int i=0;i<lstNotifications.size();i++) {
			if(i>0 && lstNotifications.at(i).dtDateOfEvent.toString("MMMM")!=lstNotifications.at(i-1).dtDateOfEvent.toString("MMMM"))
				strSheduleHtml+="</tbody></table><div style='font-size: 15pt; font-weight: bold; color:#55779B'>" + lstNotifications.at(i).dtDateOfEvent.toString("MMMM")+" " + lstNotifications.at(i).dtDateOfEvent.toString("yyyy") +"</div><p /><table style='border-spacing:0;border-collapse:collapse;width:100%'>";
			if(i>0 && lstNotifications.at(i).dtDateOfEvent!=lstNotifications.at(i-1).dtDateOfEvent) {
				switch (lstNotifications.at(i).dtDateOfEvent.dayOfWeek()) {
				case 1:
					strBuffer=tr("Mon");
				break;
				case 2:
					strBuffer=tr("Tue");
				break;
				case 3:
					strBuffer=tr("Wed");
				break;
				case 4:
					strBuffer=tr("Thu");
				break;
				case 5:
					strBuffer=tr("Fri");
				break;
				case 6:
					strBuffer=tr("Sat");
				break;
				case 7:
					strBuffer=tr("Sun");
				break;
				}

				strSheduleHtml+="<tr><th style='padding: 5px 5px;background-color: #55779B;color:white;font-size:11pt;font-weight: bold'>"+lstNotifications.at(i).dtDateOfEvent.toString("dd")+"<br /><span style='font-size:9pt; font-weight: normal;'>"+strBuffer+"</span></th>";
			}
			if(i>0 && lstNotifications.at(i).dtDateOfEvent==lstNotifications.at(i-1).dtDateOfEvent)
				strSheduleHtml+="<tr><th style='padding: 5px 5px;background: none;color:white;font-size:15pt;font-weight: bold'></th>";

			if(bOdd) {
				strBuffer="background-color:#f7f7f7;";
				bOdd=false;
			}
			else {
				strBuffer="";
				bOdd=true;
			}

			strSheduleHtml+="<td style='padding: 5px;"+strBuffer+"' /><td style='padding: 5px;"+strBuffer+"'>";
				
			strSheduleHtml+="<span>"+lstNotifications.at(i).tmTimeOfEvent.toString("hh:mm")+"</span>";
			strSheduleHtml+="</td><td style='padding: 5px;"+strBuffer+"'><div /><div>";

			if(lstNotifications.at(i).bExpired)	strSheduleHtml+="<span style='color:red;'>";
			else strSheduleHtml+="<span style='color:black;'>";
			strSheduleHtml+="<a href='#"+lstNotifications.at(i).strLinkId+"'>"+lstNotifications.at(i).strLinkId+"</a> ";
			strSheduleHtml+=lstNotifications.at(i).strNotificationText+"<br />";
			strSheduleHtml+=tr("Executor")+": "+lstNotifications.at(i).strExecutor+"</span></div></td></tr>";
			strSheduleHtml+="<tr><th /><td /><td /><td /></tr>";

			if(i>0 && lstNotifications.at(i).dtDateOfEvent.toString("MMMM")!=lstNotifications.at(i-1).dtDateOfEvent.toString("MMMM"))
				strSheduleHtml+="</tbody></table>";
		}
		strSheduleHtml+="</tbody></table>";
	}
	else strSheduleHtml="<span style='font-size: 11pt; font-weight: bold;'>"+tr("There's no sheduled events")+"</span>";
	//dboper->mdlDB05->setTable("lawsuites");
	
	//qDebug()<<strSheduleHtml;
	if(!bPrint)	emit setShedule(strSheduleHtml,bShort); //��������� �������� ��� ������ �������� �������
	else PrintHTML("<h3><center>"+tr("Schedule")+"</center></h3><br />"+strSheduleHtml);

	// ����� ������� ����������
}

void mw::PrintHTML(const QString strHTML) {
	textDocument.setHtml(strHTML);
	printerDevice = new QPrinter();
	prevDlg = new QPrintPreviewDialog(printerDevice,this,Qt::Window);
	connect(prevDlg, SIGNAL(paintRequested(QPrinter*)), this, SLOT(PrintPreview(QPrinter*)));
	prevDlg->printer()->setPaperSize(QPrinter::A4);
	prevDlg->printer()->setOrientation(QPrinter::Portrait);
	prevDlg->exec();
	delete prevDlg;	
	delete printerDevice;
}

void mw::PrintPreview(QPrinter *prntr) {
	textDocument.print(prntr);
}

void mw::LoadFormTable(QString sForm,QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser) {
	stlDecisionTypes.clear();
	stlNLA.clear();
	stlExecutors.clear();

	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"*","","decisions",true,"form='"+sForm+"'");
	while(dboper->query.next()) stlDecisionTypes.append(dboper->query.value("decisiontype").toString());
	dboper->query.finish();
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"*","","npa",true,"form='"+sForm+"'");
	while(dboper->query.next()) stlNLA.append(dboper->query.value("npa").toString());
	dboper->query.finish();
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"*","","executors",false,"");
	while(dboper->query.next()) stlExecutors.append(dboper->query.value("executor").toString());
	dboper->query.finish();
	
	if(sForm=="ls") {
		stlInstances.clear();
		emit ExecQuery(PUBLIC_QUERY,"DB05",0,"*","","instances",false,"");
		while(dboper->query.next()) stlInstances.append(dboper->query.value("instance").toString());
		dboper->query.finish();
	}

	emit MainTableAssign(qSqlTMES, sTableName, sFilter, sFilterByUser);
	setStatusText(1,dboper->mdlDB05->rowCount());
}

void mw::generateReport(QString sFilter,QDate &dtFrom,QDate &dtTo) {
	QString sHTML;
	QString strFilter="BETWEEN #" + dtFrom.toString("MM/dd/yyyy") + "# AND #" + dtTo.toString("MM/dd/yyyy") +"#";
	QString sBuffer;
	sHTML="<h4>"+tr("Period")+": "+dtFrom.toString("dd.MM.yyyy")+"-"+dtTo.toString("dd.MM.yyyy")+"</h4><hr />";
	sHTML+="<span>"+tr("Total new incoming documents")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"count(*)","","incoming",true,"[regdate] "+strFilter);
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total new claims")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"count(*)","","amzclaims",true,"[regdate] "+strFilter);
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total closed claims")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"count(*)","","documents",true,"([regdate] "+strFilter+") AND ([attachedtodoctype]='acl' OR [attachedtodoctype]='acl_a') AND ([doctype]='"+tr("Reject")+"' OR "+"[doctype]='"+tr("Redirect")+"') AND [agreed]=true");
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total cases")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"count(*)","","amzcases",true,"[decreedate] "+strFilter);
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total closed cases")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"count(*)","","documents",true,"([regdate] "+strFilter+") AND ([attachedtodoctype]='acs' OR [attachedtodoctype]='acs_a') AND ([doctype]='"+tr("Decision")+"') AND [agreed]=true");
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	//sHTML+=tr("Total violations")+": ";
	sHTML+=tr("Total orders")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DB05",0,"count(*)","","documents",true,"([regdate] "+strFilter+") AND ([attachedtodoctype]='acs' OR [attachedtodoctype]='acs_a') AND ([doctype]='"+tr("Order")+"') AND [agreed]=true");
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer;
	sHTML+="</span>";
	emit showGeneratedReport(sHTML);
	wGenReport->setVisible(true);
}