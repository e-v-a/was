#include "wgtadministrativecases.h"
#include "ui_wgtadministrativecases.h"
#include "datedelegate.h"

wgtAdministrativeCases::wgtAdministrativeCases(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAdministrativeCases)
{
    ui->setupUi(this);
	iRowIndex=-1;
	wAddOutDoc = new wgtaddoutdoc(this);
	wAddOutDoc->setVisible(false);
    wAddOutDoc->setWindowFlags(Qt::Dialog);
    wAddOutDoc->setWindowFlags(wAddOutDoc->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wAddOutDoc->setWindowModality(Qt::WindowModal);
	connect(wAddOutDoc,SIGNAL(updateACTable()),this,SLOT(updateACTable()));
    actEdit=new QAction(tr("Edit"),this);
    actDelete=new QAction(tr("Delete"),this);
	actSearch=new QAction(tr("Search"),this);
	actSearch->setCheckable(true);
	QList<QKeySequence> scList;
	scList.append(QKeySequence(Qt::CTRL + Qt::Key_F));
	scList.append(QKeySequence(Qt::Key_F3));
	actSearch->setShortcuts(scList);
    connect(actEdit, SIGNAL(triggered()), this, SLOT(EditAction()));
    connect(actDelete, SIGNAL(triggered()), this, SLOT(DeleteAction()));
	connect(actSearch, SIGNAL(toggled(bool)), this, SLOT(on_pbShowSearch_toggled(bool)));
	addAction(actSearch);
	ui->tvOutcoming->addAction(actEdit);
	ui->tvOutcoming->addAction(actDelete);
	ui->tvOutcoming->setContextMenuPolicy(Qt::ActionsContextMenu);
	ui->pbSend2Agreement->setVisible(false);
    bIsSearch=false;
	bIsNew=false;
	ui->lblNoMatches->setVisible(false);
	ui->wgtArcNotify->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtAdministrativeCases::~wgtAdministrativeCases()
{
    delete ui;
}

void wgtAdministrativeCases::on_leSearch_returnPressed()
{
	ui->pbSearch->click();
}

void wgtAdministrativeCases::on_pbClear_clicked()
{
    QString strFilter;
    ui->leSearch->clear();
    strFilter="[executor] = '"+dboper->UsrSettings.sUserName+"' ORDER BY [exdate] DESC, [casenum] DESC";
    dboper->mdlDB05->setFilter(strFilter);
    dboper->mdlDB05->select();

    if(dboper->mdlDB05->rowCount() > 0) {
        iRowCount=dboper->mdlDB05->rowCount();
        iCurrentRow=0;
        emit setStatusText(iCurrentRow+1, iRowCount);
        SetAdmCasesFormFields(iCurrentRow);
    }
	ui->lblNoMatches->setVisible(false);
}

void wgtAdministrativeCases::on_pbSearch_clicked()
{
    QString strSearchString;
    if(ui->leSearch->text()!="") {
        bIsSearch=true;
        QString arg1=ui->leSearch->text();
        strSearchString="([casenum] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1+"%') AND ([executor]='";
        strSearchString+=dboper->UsrSettings.sUserName+"') ORDER BY [exdate] DESC, [casenum] DESC";

        dboper->mdlDB05->setFilter(strSearchString);
        dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount() == 0) {
			ui->wgtArcNotify->setVisible(true);
			ui->lblNoMatches->setVisible(true);
		}
        else if(dboper->mdlDB05->rowCount() > 0) {
			ui->lblNoMatches->setVisible(false);
			ui->wgtArcNotify->setVisible(false);
            iRowCount=dboper->mdlDB05->rowCount();
            iCurrentRow=0;
            emit setStatusText(iCurrentRow+1, iRowCount);
            SetAdmCasesFormFields(iCurrentRow);
        }
    }
}

void wgtAdministrativeCases::on_leCaseNum_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_deExcitDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_leDefendant_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_leAddress_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_leClause_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_dteJudgementDate_dateTimeChanged(const QDateTime &dateTime)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_cbDecision_currentTextChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_leAmount_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAdministrativeCases::on_tvOutcoming_clicked(const QModelIndex &index)
{
	if(!dboper->stmdlEOoutcomingDocs->record(index.row()).value("senttoagreement").toBool()) ui->pbSend2Agreement->setVisible(true);
	else ui->pbSend2Agreement->setVisible(false);
}

void wgtAdministrativeCases::on_tvOutcoming_doubleClicked(const QModelIndex &index)
{
	EditAction();
}

void wgtAdministrativeCases::on_pbDelete_clicked()
{
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setWindowTitle("was");
	msgQuestion.setText(tr("You're going to delete the case. This will cause the removal of all attached documents. Are you sure?"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAddDoc->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);
	
	if(msgQuestion.exec()==QMessageBox::Yes) {
		QSqlQuery query;
		QString strBuffer;
		QString strTextFile;
		QString strScanFile;
		strBuffer.setNum(iRowIndex);
		QFile flAttachment;
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","documents",true,"[attachedtodoctype]='adc' AND [attachedtodocid]="+strBuffer);
		strBuffer=dboper->EnvSettings.strCommonFolder+"/";
		while(dboper->query.next()) {
			strTextFile=dboper->query.value("textfile").toString();
			if(strTextFile!="") {
				flAttachment.setFileName(strBuffer+strTextFile);
				flAttachment.remove();
			}
			strScanFile=dboper->query.value("scanfile").toString();
			if(strScanFile!="") {
				flAttachment.setFileName(strBuffer+strScanFile);
				flAttachment.remove();
			}
		}
		strBuffer.setNum(iRowIndex);
		dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,"[attachedtodoctype]='adc' AND [attachedtodocid]="+strBuffer);
		dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","admcases",true,"[id]="+strBuffer);
	}
	ui->pbFirst->click();
}

void wgtAdministrativeCases::on_pbFirst_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=0;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetAdmCasesFormFields(iCurrentRow);
}

void wgtAdministrativeCases::on_pbPrev_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow--;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetAdmCasesFormFields(iCurrentRow);
}

void wgtAdministrativeCases::on_pbSave_clicked()
{
	QString strBuffer;
	QStringList stlFields;
	QList<QVariant> stlValues;

	stlFields.append("1");//casenum
	stlValues.append(ui->leCaseNum->text());
	stlFields.append("2");//exdate
	stlValues.append(ui->deExcitDate->date());
	stlFields.append("3");//defendant
	stlValues.append(ui->leDefendant->text());
	stlFields.append("4");//defaddress
	stlValues.append(ui->leAddress->text());
	stlFields.append("5");//clause
	stlValues.append(ui->leClause->text());
	stlFields.append("6");//clausepart
	stlValues.append(ui->leClausePart->text());
	stlFields.append("7");//dateofjudg
	stlValues.append(ui->dteJudgementDate->dateTime());
	stlFields.append("8");//decision
	stlValues.append(ui->cbDecision->currentText());
	stlFields.append("9");//amount
	stlValues.append(ui->leAmount->text().toInt());
	if(bIsNew) {
		stlFields.append("10");//executor
		stlValues.append(dboper->UsrSettings.sUserName);
		iCurrentRow=0;
	}
	stlFields.append("11");//prolong
	stlValues.append(ui->cbProlong->isChecked());
	if(ui->cbProlong->isChecked()) {
		stlFields.append("12");//prolongdate
		stlValues.append(ui->deTill->date());
	}
	ui->pbSave->setEnabled(!dboper->dbWriteData(bIsNew,true,iCurrentRow,stlFields,stlValues));
	bFormChanged=ui->pbSave->isEnabled();

	if(bIsNew) {
		bIsNew=false;
		emit LoadAdded(ui->leCaseNum->text());
	}
}

void wgtAdministrativeCases::on_pbNext_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow++;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetAdmCasesFormFields(iCurrentRow);
}

void wgtAdministrativeCases::on_pbLast_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=iRowCount-1;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetAdmCasesFormFields(iCurrentRow);
}

void wgtAdministrativeCases::on_pbShowSearch_toggled(bool checked)
{
	if(ui->pbSave->isEnabled()) AskSaving();
    bIsSearch=checked;
	ui->wgtSearch->setVisible(checked);
    ui->leSearch->clear();
    (checked)?ui->leSearch->setFocus():ui->pbClear->click();
	ui->pbShowSearch->setChecked(checked);
	actSearch->setChecked(checked);
    ui->lblNoMatches->setVisible(false);
	ui->pbClear->setVisible(false);
}

void wgtAdministrativeCases::SetAdmCasesFormFields(int recordnum) {
	ui->leCaseNum->clear();
	ui->deExcitDate->clear();
	ui->leDefendant->clear();
	ui->leAddress->clear();
	ui->leClause->clear();
	ui->dteJudgementDate->clear();
	ui->cbDecision->clear();
	ui->leAmount->clear();
	ui->cbProlong->setChecked(false);
	ui->deExcitDate->setDate(QDate(2013,1,1));
	ui->dteJudgementDate->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	ui->wgtSearch->setVisible(bIsSearch);
	if(ui->lblTitle->text()!=tr("Administrative case")) ui->lblTitle->setText(tr("Administrative case"));
	
	iRowIndex=dboper->mdlDB05->record(recordnum).value("id").toInt();
	ui->leCaseNum->setText(dboper->mdlDB05->record(recordnum).value("casenum").toString());
	ui->deExcitDate->setDate(dboper->mdlDB05->record(recordnum).value("exdate").toDate());
	ui->leDefendant->setText(dboper->mdlDB05->record(recordnum).value("defendant").toString());
	ui->leAddress->setText(dboper->mdlDB05->record(recordnum).value("defaddress").toString());
	ui->leClause->setText(dboper->mdlDB05->record(recordnum).value("clause").toString());
	ui->leClausePart->setText(dboper->mdlDB05->record(recordnum).value("clausepart").toString());
	ui->dteJudgementDate->setDateTime(dboper->mdlDB05->record(recordnum).value("dateofjudg").toDateTime());
	ui->leAmount->setText(dboper->mdlDB05->record(recordnum).value("amount").toString());
	ui->cbProlong->setChecked(dboper->mdlDB05->record(recordnum).value("prolong").toBool());
	if(ui->cbProlong->isChecked()) ui->deTill->setDate(dboper->mdlDB05->record(recordnum).value("prolongdate").toDate());
	ui->lblTill->setVisible(ui->cbProlong->isChecked());
	ui->deTill->setVisible(ui->cbProlong->isChecked());

	if(ui->cbDecision->count()==0) ui->cbDecision->addItems(stlDecisionTypes);
	ui->cbDecision->setCurrentIndex(ui->cbDecision->findText(dboper->mdlDB05->record(recordnum).value("decision").toString(),Qt::MatchExactly));

	dboper->stmdlEOincomingDocs->setTable("incoming");
	QString strFilter;
	strFilter="[attachedto] = '";
	strFilter+=tr("Administrative case");
	strFilter+="' AND [attachedtodocid]=";
	QString strBuffer;
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;
	
	dboper->stmdlEOincomingDocs->setFilter(strFilter);
	dboper->stmdlEOincomingDocs->select();
	dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
	dboper->stmdlEOincomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOincomingDocs->setHeaderData(8, Qt::Horizontal, tr("Addressee"));
	dboper->stmdlEOincomingDocs->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->stmdlEOincomingDocs->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	ui->tvIncoming->setModel(dboper->stmdlEOincomingDocs);
	ui->tvIncoming->hideColumn(0);
	int i;
	for(i=3;i<8;i++) ui->tvIncoming->hideColumn(i);
	ui->tvIncoming->hideColumn(9);
	for(i=12;i<20;i++) ui->tvIncoming->hideColumn(i);
	
	ui->tvIncoming->resizeColumnsToContents();
	ui->tvIncoming->horizontalHeader()->setStretchLastSection(true);

	dboper->stmdlEOoutcomingDocs->setTable("documents");
	strFilter="[attachedtodoctype] = 'adc'";
	strFilter+=" AND [attachedtodocid]=";
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;

	dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
	dboper->stmdlEOoutcomingDocs->select();
	dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document number"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Document date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Reg Num"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Document Type"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Post ID"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Execution date"));
	ui->tvOutcoming->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvOutcoming->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvOutcoming->setItemDelegateForColumn(7, new DataDelegate());
	ui->tvOutcoming->setModel(dboper->stmdlEOoutcomingDocs);
	ui->tvOutcoming->hideColumn(0);
	
	for(i=8;i<16;i++) ui->tvOutcoming->hideColumn(i);
	ui->tvOutcoming->resizeColumnsToContents();
	ui->tvOutcoming->horizontalHeader()->setStretchLastSection(true);
	ui->pbSave->setEnabled(false);
	bFormChanged=false;

	ui->leCaseNum->setEnabled(!iRowCount<1);
	ui->deExcitDate->setEnabled(!iRowCount<1);
	ui->leDefendant->setEnabled(!iRowCount<1);
	ui->leAddress->setEnabled(!iRowCount<1);
	ui->leClause->setEnabled(!iRowCount<1);
	ui->leClausePart->setEnabled(!iRowCount<1);
	ui->dteJudgementDate->setEnabled(!iRowCount<1);
	ui->cbDecision->setEnabled(!iRowCount<1);
	ui->leAmount->setEnabled(!iRowCount<1);
	ui->pbShowSearch->setEnabled(!iRowCount<1);
	ui->pbDelete->setEnabled(!iRowCount<1);

	ui->twDocuments->setEnabled(!iRowCount<1);
	ui->pbFirst->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbPrev->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbNext->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	ui->pbLast->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
}

void wgtAdministrativeCases::updateACTable() {
	dboper->stmdlEOoutcomingDocs->select();
}

void wgtAdministrativeCases::setACDBOperLink() {
	wAddOutDoc->dboper=dboper;
}

void wgtAdministrativeCases::clearADCSearch() {
	bIsSearch=false;
	ui->leSearch->clear();
	ui->pbClear->setVisible(false);
	ui->pbShowSearch->setChecked(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
}

void wgtAdministrativeCases::EditAction() {
	if(ui->tvOutcoming->currentIndex().isValid()) {
		wAddOutDoc->setAODVars("adc", iRowIndex,true,dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("id").toInt());
		wAddOutDoc->setVisible(true);
	}
}

void wgtAdministrativeCases::DeleteAction() {
	if(ui->tvOutcoming->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("agreed").toBool()) {
		QString strID;
		QString strSelectString;
		QMessageBox msgQuestion;
		
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Delete document"));
		msgQuestion.setText(tr("You're going to delete selected document. Are you sure?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		
		msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
		
		msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
		msgQuestion.button(QMessageBox::No)->setText(tr("No"));

		QFont fntBold;
		fntBold.setFamily(ui->pbAddDoc->font().family());
		fntBold.setPointSize(8);
		fntBold.setBold(true);
		msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);
		if(msgQuestion.exec()==QMessageBox::Yes)
			wAddOutDoc->deleteDocument(dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("id").toInt());

		dboper->stmdlEOoutcomingDocs->select();
	}
}

void wgtAdministrativeCases::EnableBlankFields() {
	ui->leCaseNum->clear();
	ui->leCaseNum->setEnabled(true);
	ui->deExcitDate->clear();
	ui->deExcitDate->setEnabled(true);
	ui->leDefendant->clear();
	ui->leDefendant->setEnabled(true);
	ui->leAddress->clear();
	ui->leAddress->setEnabled(true);
	ui->leClause->clear();
	ui->leClause->setEnabled(true);
	ui->leClausePart->clear();
	ui->leClausePart->setEnabled(true);
	ui->dteJudgementDate->clear();
	ui->dteJudgementDate->setEnabled(true);
	ui->cbDecision->clear();
	ui->cbDecision->setEnabled(true);
	ui->leAmount->clear();
	ui->leAmount->setEnabled(true);
	ui->deExcitDate->setDate(QDate(2013,1,1));
	ui->dteJudgementDate->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	if(ui->cbDecision->count()==0) ui->cbDecision->addItems(stlDecisionTypes);

	ui->pbFirst->setEnabled(false);
	ui->pbPrev->setEnabled(false);
	ui->pbNext->setEnabled(false);
	ui->pbLast->setEnabled(false);
	ui->pbShowSearch->setEnabled(false);
	ui->pbDelete->setEnabled(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
	ui->lblTitle->setText(tr("New administrative case"));
}

void wgtAdministrativeCases::on_pbAddDoc_clicked()
{
	wAddOutDoc->setAODVars("adc", iRowIndex);
    wAddOutDoc->setVisible(true);
}

void wgtAdministrativeCases::on_pbSend2Agreement_clicked()
{
	if(ui->tvOutcoming->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row()).value("agreed").toBool()) {
		/*QSqlRecord record=dboper->stmdlEOoutcomingDocs->record(ui->tvOutcoming->currentIndex().row());
		record.setValue("senttoagreement",true);
		record.setValue("returned",false);
		dboper->stmdlEOoutcomingDocs->setRecord(iCurrentRow,record);
		dboper->stmdlEOoutcomingDocs->submitAll();
		record.clear();
		ui->pbSend2Agreement->setVisible(false);*/

		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvOutcoming->currentIndex().row(),8),true);
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvOutcoming->currentIndex().row(),9),false);
		dboper->stmdlEOoutcomingDocs->submitAll();
		ui->pbSend2Agreement->setVisible(false);
//		ui->pbPublish->setVisible(false);
	}
}

void wgtAdministrativeCases::on_pbArc_clicked()
{
	QString strFields;
	QString strValues;
	QString strBuffer;
	bool bNotAgreed=false;
	if(dboper->stmdlEOoutcomingDocs->rowCount()>0) {
		for(int i=0;i<dboper->stmdlEOoutcomingDocs->rowCount();i++) {
			if(!dboper->stmdlEOoutcomingDocs->record(i).value("agreed").toBool()) {
				QMessageBox::warning(0, tr("Archiving is not possible"),	
					tr("One or several outcoming documents are not agreed. Archiving is not possible until all procedural documents are agreed."));
				bNotAgreed=true;
				break;
			}
		}
	}
	if(!bNotAgreed) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Archive case"));
		msgQuestion.setText(tr("You're going to archive current case. Any information about case will be freezed. Do you want to continue?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) {
			strFields="[casenum],[exdate],[defendant],[defaddress],[clause],[clausepart],[dateofjudg],[decision],[amount],[executor],[prolong]";
			if(ui->cbProlong->isChecked()) strFields+=",[prolongdate]";
			strValues="'"+ui->leCaseNum->text()+"',#"+ui->deExcitDate->date().toString("yyyy-MM-dd")+"#,'";
			strValues+=ui->leDefendant->text()+"','"+ui->leAddress->text()+"','"+ui->leClause->text()+"','";
			strValues+=ui->leClausePart->text()+"',#";
			strValues+=ui->dteJudgementDate->dateTime().toString("yyyy-MM-dd hh:mm:ss")+"#,'"+ui->cbDecision->currentText()+"',";
			strValues+=ui->leAmount->text().toInt()+",'"+dboper->UsrSettings.sUserName+"'";
			if(ui->cbProlong->isChecked()) strValues+="1,#"+ui->deTill->date().toString("MM/dd/yyyy")+"#";
			else strValues+="0";

			dboper->dbQuery(PRIVATE_QUERY,"DB05",1,strFields,strValues,"admcases_arc",false,"");
			dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","admcases_arc",true,"[casenum]='"+ui->leCaseNum->text()+"'");
			dboper->query.next();
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedto]$[attachedtodocid]";
			strValues="'"+tr("Administrative cases archive")+"'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"incoming",true,"[attachedto]='"+tr("Administrative case")+"' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedtodoctype]$[attachedtodocid]";
			strValues="'adc_a'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,"[attachedtodoctype]='adc' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","admcases",true,"[casenum]='"+ui->leCaseNum->text()+"'");
			dboper->mdlDB05->select();
			iRowCount=dboper->mdlDB05->rowCount();
			iCurrentRow=0;
			emit setStatusText(iCurrentRow, iRowCount);
			SetAdmCasesFormFields(iCurrentRow);
		}
	}
}

void wgtAdministrativeCases::on_leSearch_textChanged(const QString &arg1)
{
    if(arg1!="") ui->pbClear->setVisible(true);
    else ui->pbClear->setVisible(false);
}

void wgtAdministrativeCases::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}
void wgtAdministrativeCases::on_cbProlong_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAdministrativeCases::on_deTill_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}
