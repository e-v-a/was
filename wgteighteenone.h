#ifndef WGTEIGHTEENONE_H
#define WGTEIGHTEENONE_H

#include <QWidget>
#include "wgtaddoutdoc.h"
//#include "mw.h"

namespace Ui {
class wgtEighteenOne;
}

class wgtEighteenOne : public QWidget
{
    Q_OBJECT

public:
    explicit wgtEighteenOne(QWidget *parent = 0);
    ~wgtEighteenOne();
	QStringList stlDecisionTypes;
	QStringList stlNPA;
	QStringList stlExecutors;
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	DBOperations *dboper;

public slots:
	void SetFormFields(int recordnum);
	void updateEOTable();
	void setDBOperLink();
	void clearEOSearch();
	
signals:
	void setStatusText(int iCurrentRow, int iRowsCount);

private slots:
    void on_pbLast_clicked();
	void on_pbNext_clicked();
	void on_pbSave_clicked();
	void on_pbPrev_clicked();
	void on_pbFirst_clicked();
	void on_pbSearch_clicked();
	void on_teComplainant_textChanged();
	void on_teDefendant_textChanged();
	void on_leNotifyDateNum_textChanged(const QString &arg1);
	void on_cbNPA_currentIndexChanged(const QString &arg1);
	void on_dteSubmissionDate_dateTimeChanged(const QDateTime &dateTime);
	void on_dteCommitteedate_dateTimeChanged(const QDateTime &dateTime);
	void on_cbResult_currentIndexChanged(const QString &arg1);
	void on_cbOrder_stateChanged(int arg1);
	void on_leDecNum_textChanged(const QString &arg1);
	void on_deDecDate_dateChanged(const QDate &date);
	void on_leExeInfo_textChanged(const QString &arg1);
	void on_cbOrderExe_stateChanged(int arg1);
	void on_pbAdd_clicked();
	void on_pbSendToAgreement_clicked();
	void on_pbPublish_clicked();
	void on_tvDocuments_doubleClicked(const QModelIndex &index);
	void EditAction();
	void DeleteAction();
	void on_tvDocuments_clicked(const QModelIndex &index);
	void on_pbShowSearch_toggled(bool checked);
	void on_pbClear_clicked();
	void on_leSearch_textChanged(const QString &arg1);
	void on_leSearch_returnPressed();
	void on_pbArc_clicked();

    void on_cbExecutor_currentIndexChanged(const QString &arg1);

private:
    Ui::wgtEighteenOne *ui;
	bool bFormChanged;
	bool bChangedTableSize;
    bool bIsSearch;
	wgtaddoutdoc *wAddOutDoc;
    QAction *actEdit;
    QAction *actDelete;
	QAction *actSearch;
	void AskSaving();
};

#endif // WGTEIGHTEENONE_H
