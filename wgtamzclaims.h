#ifndef WGTAMZCLAIMS_H
#define WGTAMZCLAIMS_H

#include <QWidget>
#include "wgtaddoutdoc.h"
//#include "mw.h"

namespace Ui {
class wgtAMZClaims;
}

class wgtAMZClaims : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAMZClaims(QWidget *parent = 0);
    ~wgtAMZClaims();
	QStringList stlDecisionTypes;
	QStringList stlNLA;
	QStringList stlExecutors;
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	DBOperations *dboper;

public slots:
	void SetClaimsFormFields(int recordnum);
	void updateACLTable();
	void setClaimsDBOperLink();
	void clearACLSearch();

signals:
	void GoToCase(QString casenum);
	void setStatusText(int iCurrentRow, int iRowsCount);
	void setFilterMB(QString sFilter);
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);

private slots:
    void on_pbFirst_clicked();
	void on_pbPrev_clicked();
	void on_pbSave_clicked();
	void on_pbNext_clicked();
	void on_pbLast_clicked();
	void on_pbGoToCase_clicked();
	void on_pbSearch_clicked();
	void on_pbAdd_clicked();
	void on_pbSend2Agreement_clicked();
	void on_pbPublish_clicked();
	void on_teComplainant_textChanged();
	void on_teDefendant_textChanged();
	void on_cbResult_currentIndexChanged(const QString &arg1);
	void on_leCaseNum_textChanged(const QString &arg1);
	void EditAction();
	void DeleteAction();
	void on_tvDocuments_doubleClicked(const QModelIndex &index);
	void on_tvDocuments_clicked(const QModelIndex &index);
	void on_leSearch_returnPressed();
	void on_pbClear_clicked();
	void on_pbShowSearch_toggled(bool checked);
    void on_leSearch_textChanged(const QString &arg1);
    void on_pbArc_clicked();
	void on_leClause_textChanged(const QString &arg1);
    void on_cbNLA_currentIndexChanged(const QString &arg1);
    void on_cbExecutor_currentIndexChanged(const QString &arg1);
    void on_teSummary_textChanged();
    void on_cbProlong_toggled(bool checked);
    void on_deTill_dateChanged(const QDate &date);

private:
    Ui::wgtAMZClaims *ui;
	bool bFormChanged;
	bool bIsSearch;
	wgtaddoutdoc *wAddOutDoc;
    QAction *actEdit;
    QAction *actDelete;
	QString strSearchString;
	QAction *actSearch;
	void AskSaving();
};

#endif // WGTAMZCLAIMS_H
