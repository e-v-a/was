#include "udatabase.h"

DBOperations::DBOperations(QObject *parent)
	: QObject(parent) {
	bFilterByUser=false;
}

DBOperations::~DBOperations() {

}

bool DBOperations::dbConn(const bool bLocalDB, const QString sDBName, const QString sConnectionName,const QString sServAddr, const QString sUsername, const QString sPassword) {
	QString sDBNameString;
	if(bLocalDB) sDBNameString="DRIVER={Microsoft Access Driver (*.mdb)};DSN='';DBQ=";
	else {
		sDBNameString="DRIVER={SQL Server};Server=";
		sDBNameString+=sServAddr+";Trusted_Connection=No;Database=";
	}
	sDBNameString+=sDBName;
	//if(bLocalDB) sDBNameString+="';";
	sDBNameString+=";";//Uid="+sUsername+";Pwd="+sPassword;

	QSqlDatabase db;
	if(bLocalDB) db = QSqlDatabase::addDatabase("QODBC",sConnectionName);
	else db = QSqlDatabase::addDatabase("QODBC");

	db.setDatabaseName(sDBNameString);
	if(!bLocalDB) {
		db.setUserName(sUsername);
		db.setPassword(sPassword);
	}
	
    /*
    db.setDatabaseName("Driver={SQL SERVER};Server="+sServAddr+";Database="+sDBName+";Trusted_Connection=no;");
    db.setUserName("admindb");
    db.setPassword("admin");

    db.setDatabaseName("Driver={SQL SERVER};Server=192.168.1.101;Database=testdb;Trusted_Connection=no;");
    */

	if(!db.open()) {
		dbConnectionError=db.lastError();
		return false;
	}
	return true;
}

bool DBOperations::dbConnLocal(const QString sDBName, const QString sConnectionName) {
	QString sDBNameString;
	sDBNameString=sDBName;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",sConnectionName);
    db.setDatabaseName(sDBNameString);
    if(!db.open()) {
		dbLocalConnectionError=db.lastError();
		return false;
	}
    return true;
}

void DBOperations::dbCloseConn(const QString sConnectionName) {
	QSqlDatabase sqlDB = QSqlDatabase::database(sConnectionName);
	if(sqlDB.isOpen()) sqlDB.close();
	sqlDB=QSqlDatabase();
	QSqlDatabase::removeDatabase(sConnectionName);
}

void DBOperations::dbQuery(const int querymode, const QString &sConnectionName, const int iAccessType, const QString &sFields, const QString &sValues, const QString &sTable, const bool &bSelect, const QString &sSelectString) {
    QSqlDatabase sqlDB = QSqlDatabase::database(sConnectionName);
	QSqlQuery sqlQueryRt(sqlDB);
    QString sQueryString;
    QStringList stlFieldsList;
    QStringList stlValuesList;
    int iIterator=0;
	
    switch(iAccessType) {
        case 0: //Read
            sQueryString = "SELECT " + sFields + " FROM " + sTable;
            if(bSelect) sQueryString+=" WHERE " + sSelectString;
            sQueryString += ";";
        break;
        case 1: //Append
            sQueryString = "INSERT INTO " + sTable + "(" + sFields + ") values(" + sValues + ");";
        break;
        case 2: //Edit
            sQueryString = "UPDATE " + sTable + " SET ";
            stlFieldsList = sFields.split("$");
            stlValuesList = sValues.split("$");

            while(iIterator<stlFieldsList.count()) {
                if(iIterator>0 && iIterator!=stlFieldsList.count()) sQueryString +=", ";
                sQueryString += stlFieldsList.at(iIterator) + "=";
                sQueryString += stlValuesList.at(iIterator);
                iIterator++;
            }
            if(bSelect) sQueryString+=" WHERE " + sSelectString + ";";
            else sQueryString+=";";
        break;
        case 3: //Delete
            sQueryString = "DELETE FROM " + sTable + " WHERE " + sSelectString;
            sQueryString += ";";
        break;
        case 4: //create table
            sQueryString = "CREATE TABLE " + sTable + " (" + sFields + ")";
            sQueryString += ";";
        break;
    }
	qDebug()<<sQueryString;
	if(querymode==PRIVATE_QUERY) sqlQueryRt.exec(sQueryString);
	else if(querymode==PUBLIC_QUERY) {
		query=sqlQueryRt;
		query.exec(sQueryString);
		sqlQueryRt.finish();
	}
	else if(querymode==TIMER_QUERY) {
		TimerQuery=sqlQueryRt;
		TimerQuery.exec(sQueryString);
		sqlQueryRt.finish();
		//qDebug()<<sQueryString;
	}
	else if(querymode==TIMER_LOCAL_QUERY) {
		TimerLocalQuery=sqlQueryRt;
		TimerLocalQuery.exec(sQueryString);
		sqlQueryRt.finish();
	}
	else if(querymode==TIMER_MESSAGES_QUERY) {
		TimerMessagesQuery=sqlQueryRt;
		TimerMessagesQuery.exec(sQueryString);
		sqlQueryRt.finish();
	}
	
	if(sqlQueryRt.lastError().isValid()) {
           QString qErrNum;
           qErrNum.setNum(sqlQueryRt.lastError().number());
           if(sqlQueryRt.lastError().number()!=1) {
                QMessageBox::critical(0, QObject::tr("EDMS05"),QObject::tr("Error Number: ")+qErrNum+": "+sqlQueryRt.lastError().text(), QMessageBox::Ok);
                qApp->exit(171);
           }
    }

	sqlQueryRt.finish();
}

bool DBOperations::dbWriteData(bool bIsNew,bool bIsDB05Model,const int iRow,const QStringList stlFields, const QList<QVariant> stlValues) {
	if(bIsNew) {
		if(bIsDB05Model) mdlDB05->insertRow(0);
		else stmdlEOoutcomingDocs->insertRow(0);
	}
	for(int i=0;i<stlFields.size();++i) {
		if(bIsDB05Model) mdlDB05->setData(mdlDB05->index(iRow,stlFields.at(i).toInt()),stlValues.at(i));
		else stmdlEOoutcomingDocs->setData(stmdlEOoutcomingDocs->index(iRow,stlFields.at(i).toInt()),stlValues.at(i));
	}
	
	if(bIsDB05Model) return mdlDB05->submitAll();
	else return stmdlEOoutcomingDocs->submitAll();
}

void DBOperations::ExecQuery(const int querymode,QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString) {
	dbQuery(querymode,sConnectionName, iAccessType, sFields, sValues, sTable, bSelect, sSelectString);
}

void DBOperations::MainTableAssign(QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser) {
	bFilterByUser=(sFilterByUser!="");
	bFilter=(sFilter!="");

	mdlDB05->setEditStrategy(qSqlTMES);
	mdlDB05->setTable(sTableName);
	mdlDB05->setFilter(sFilterByUser+sFilter);
	mdlDB05->select();
}

bool DBOperations::IsFiteredByUser() {
	return bFilterByUser;
}

bool DBOperations::IsFitered() {
	return bFilter;
}

void DBOperations::setFilterMB(QString sFilter) {
	mdlDB05->setFilter(sFilter);
    mdlDB05->select();
}