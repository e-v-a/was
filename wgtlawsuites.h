#ifndef WGTLAWSUITES_H
#define WGTLAWSUITES_H

#include <QWidget>
#include "wgtaddoutdoc.h"

namespace Ui {
class wgtLawsuites;
}

class wgtLawsuites : public QWidget
{
    Q_OBJECT

public:
    explicit wgtLawsuites(QWidget *parent = 0);
    ~wgtLawsuites();
	QStringList stlDecisionTypes;
	QStringList stlInstances;
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	DBOperations *dboper;
	bool bIsNew;

public slots:
	void SetLSFormFields(int recordnum);
	void EnableLSBlankFields();
	void updateLSTable();
	void setLSOperLink();
	void clearLSSearch();

signals:
	void setStatusText(int iCurrentRow, int iRowsCount);
	void LoadAddedLS(QString strLSCaseNum);

private slots:
    void on_leSearch_returnPressed();
	void on_leSearch_textChanged(const QString &arg1);
	void on_pbClear_clicked();
	void on_pbSearch_clicked();
	void on_pbArc_clicked();
	void on_leCaseNum_textChanged(const QString &arg1);
	void on_deClaimDate_dateChanged(const QDate &date);
	void on_teComplainant_textChanged();
	void on_teDefendant_textChanged();
	void on_teThirdPersons_textChanged();
	void on_teOtherPersons_textChanged();
	void on_pbAddCaseCard_clicked();
	void on_pbDeleteCaseCard_clicked();
	void on_cbResult_currentIndexChanged(const QString &arg1);
	void on_pbAdd_clicked();
	void on_pbSendToAgreement_clicked();
	void on_pbShowSearch_toggled(bool checked);
	void on_pbDelete_clicked();
	void on_pbFirst_clicked();
	void on_pbPrev_clicked();
	void on_pbSave_clicked();
	void on_pbNext_clicked();
	void on_pbLast_clicked();
	void EditAction();
	void DeleteAction();
	void on_cbInstance_currentIndexChanged(const QString &arg1);
    void on_leJudge_textChanged(const QString &arg1);
    void on_dteTrialDateTime_dateTimeChanged(const QDateTime &dateTime);
    void on_leCourtAddress_textChanged(const QString &arg1);
	void on_tvOutcoming_clicked(const QModelIndex &index);
    void on_tvOutcoming_doubleClicked(const QModelIndex &index);

private:
    Ui::wgtLawsuites *ui;
	wgtaddoutdoc *wAddOutDoc;
	bool bFormChanged;
	bool bIsSearch;
	QAction *actEdit;
    QAction *actDelete;
	QAction *actSearch;
	int iLSID;
	void AskSaving();
};

#endif // WGTLAWSUITES_H
