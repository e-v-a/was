#ifndef WGTAMZCASES_H
#define WGTAMZCASES_H

#include <QWidget>
#include "wgtaddoutdoc.h"

namespace Ui {
class wgtAMZCases;
}

class wgtAMZCases : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAMZCases(QWidget *parent = 0);
	~wgtAMZCases();
	QStringList stlDecisionTypes;
	QStringList stlNLA;
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	DBOperations *dboper;

public slots:
	void SetCasesFormFields(int recordnum);
	void updateACSTable();
	void setCasesDBOperLink();
	void clearACSSearch();
	
signals:
	void setStatusText(int iCurrentRow, int iRowsCount);

private slots:
    void on_pbSearch_clicked();
	void on_pbDeleteCase_clicked();
	void on_pbFirst_clicked();
	void on_pbPrev_clicked();
	void on_pbSave_clicked();
	void on_pbNext_clicked();
	void on_pbLast_clicked();
	void on_pbAdd_clicked();
	void on_pbSend2Agreement_clicked();
	void on_pbPublish_clicked();
	void on_leClause_textChanged(const QString &arg1);
	void on_leCaseNum_textChanged(const QString &arg1);
	void on_leDecreeNum_textChanged(const QString &arg1);
	void on_deDecreeDate_dateChanged(const QDate &date);
	void on_teComplainant_textChanged();
	void on_teDefendant_textChanged();
	void on_leDecisionNum_textChanged(const QString &arg1);
	void on_deDecisionDate_dateChanged(const QDate &date);
	void on_teRequests_textChanged();
	void on_dteSubmissionDate_dateChanged(const QDate &date);
	void on_dteCommitteeDate_dateTimeChanged(const QDateTime &dateTime);
	void on_cbResult_currentIndexChanged(const QString &arg1);
	void on_tvDocuments_doubleClicked(const QModelIndex &index);
	void EditAction();
	void DeleteAction();
	void on_tvDocuments_clicked(const QModelIndex &index);
	void on_leSearch_returnPressed();
	void on_pbClear_clicked();
	void on_pbShowSearch_toggled(bool checked);
    void on_leSearch_textChanged(const QString &arg1);
	void on_pbArc_clicked();
	void on_cbNLA_currentIndexChanged(const QString &arg1);
	void on_cbOrder_stateChanged(int arg1);
	void on_cbOrderExecuted_stateChanged(int arg1);
	void on_leExecInfo_textChanged(const QString &arg1);
	void on_leClauses_textChanged(const QString &arg1);

    void on_cbProlong_toggled(bool checked);

    void on_deTill_dateChanged(const QDate &date);

    void on_teSummary_textChanged();

    void on_deExcitationDate_dateChanged(const QDate &date);

private:
    Ui::wgtAMZCases *ui;
	bool bFormChanged;
	bool bIsSearch;
	wgtaddoutdoc *wAddOutDoc;
    QAction *actEdit;
    QAction *actDelete;
	QAction *actSearch;
	void AskSaving();
};

#endif // WGTAMZCASES_H
