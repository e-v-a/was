#include "qfile.h"
#include "wgtadmmanagement.h"
#include "ui_wgtadmmanagement.h"

wgtAdmManagement::wgtAdmManagement(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAdmManagement)
{
    ui->setupUi(this);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	wUserRights = new wgtUserRights(this);
	wUserRights->setVisible(false);
    wUserRights->setWindowFlags(Qt::Dialog);
    wUserRights->setWindowFlags(wUserRights->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wUserRights->setWindowModality(Qt::WindowModal);
	connect(this,SIGNAL(initUserRights()),wUserRights,SLOT(initUserRights()));
	connect(wUserRights,SIGNAL(UpdateTable(int)),this,SLOT(UpdateTable(int)));
	wTwoFieldsDlg=new wgtTwoFieldsDialog(this);
	wTwoFieldsDlg->setVisible(false);
    wTwoFieldsDlg->setWindowFlags(Qt::Dialog);
    wTwoFieldsDlg->setWindowFlags(wUserRights->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wTwoFieldsDlg->setWindowModality(Qt::WindowModal);
	connect(this,SIGNAL(initTwoFieldsDlg(int)),wTwoFieldsDlg,SLOT(initTwoFieldsDlg(int)));
	connect(wTwoFieldsDlg,SIGNAL(UpdateTable(int)),this,SLOT(UpdateTable(int)));
}

wgtAdmManagement::~wgtAdmManagement()
{
    delete ui;
}

void wgtAdmManagement::on_pbNewUser_clicked()
{
	wUserRights->iRow=ui->tvUsers->currentIndex().row();
	emit initUserRights();
	wUserRights->bIsEdit=false;
	wUserRights->setVisible(true);
}

void wgtAdmManagement::on_pbChangeUserPerm_clicked()
{
	if(ui->tvUsers->currentIndex().isValid()) {
		wUserRights->iRow=ui->tvUsers->currentIndex().row();
		wUserRights->bIsEdit=true;
		emit initUserRights();
		wUserRights->setVisible(true);
	}
}

void wgtAdmManagement::on_pbFlushPassword_clicked()
{
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setText(tr("Are you sure want to flush user's password? After that operation user would have to enter a new password in the next login'"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAddItem->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

	if(msgQuestion.exec()==QMessageBox::Yes) {
		dboper->mdlDB05->setData(dboper->mdlDB05->index(ui->tvUsers->currentIndex().row(),5),"");
		dboper->mdlDB05->submitAll();
		UpdateTable(0);
	}
}

void wgtAdmManagement::on_pbDeleteUser_clicked()
{
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setText(tr("Are you sure want to delete user? This is irreversible operation!"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAddItem->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

	if(msgQuestion.exec()==QMessageBox::Yes) {
		dboper->mdlDB05->removeRow(ui->tvUsers->currentIndex().row());
		dboper->mdlDB05->submitAll();
		UpdateTable(0);
	}
}

void wgtAdmManagement::on_pbAddExecutor_clicked()
{
	bool ok;
    QString text = QInputDialog::getText(this, "was", tr("Enter executor:"), QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) {
		dboper->stmdlEOincomingDocs->insertRow(0);
		dboper->stmdlEOincomingDocs->setData(dboper->mdlDB05->index(0,1),text);
		dboper->stmdlEOincomingDocs->submitAll();
		UpdateTable(1);
	}
}

void wgtAdmManagement::on_pbDeleteExecutor_clicked()
{
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setWindowTitle("was");
	msgQuestion.setText(tr("Are you sure want to delete executor? This is irreversible operation!"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAddItem->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

	if(msgQuestion.exec()==QMessageBox::Yes) {
		dboper->stmdlEOincomingDocs->removeRow(ui->tvExecutors->currentIndex().row());
		dboper->stmdlEOincomingDocs->submitAll();
		UpdateTable(1);
	}
}

void wgtAdmManagement::on_pbAddItem_clicked()
{
	if(ui->twListOfVars->currentIndex()==1 || ui->twListOfVars->currentIndex()==2 || ui->twListOfVars->currentIndex()==4) {
		if(ui->twListOfVars->currentIndex()==1)
			wTwoFieldsDlg->iRow=ui->tvDecisions->currentIndex().row();
		else if(ui->twListOfVars->currentIndex()==2)
			wTwoFieldsDlg->iRow=ui->tvDocTypes->currentIndex().row();
		else if(ui->twListOfVars->currentIndex()==4)
			wTwoFieldsDlg->iRow=ui->tvNLA->currentIndex().row();
		emit initUserRights();
		wTwoFieldsDlg->bIsEdit=false;
		wTwoFieldsDlg->setVisible(true);
	}
	else {
		bool ok;
		QString strLabel;
		QString text;
		if(ui->twListOfVars->currentIndex()==0 || ui->twListOfVars->currentIndex()==5) strLabel=tr("Enter document type:");
		if(ui->twListOfVars->currentIndex()==3) strLabel=tr("Enter instance:");
		if(ui->twListOfVars->currentIndex()==0)
			text = QInputDialog::getText(this, "was", strLabel, QLineEdit::Normal, "", &ok);
		else if(ui->twListOfVars->currentIndex()==3)
			text = QInputDialog::getText(this, "was", strLabel, QLineEdit::Normal, "", &ok);
		else if(ui->twListOfVars->currentIndex()==5)
			text = QInputDialog::getText(this, "was", strLabel, QLineEdit::Normal, "", &ok);

		if (ok && !text.isEmpty()) {
			dboper->stmdlEOoutcomingDocs->insertRow(0);
			dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(0,1),text);
			dboper->stmdlEOoutcomingDocs->submitAll();
			UpdateTable(2);
		}
	}
}

void wgtAdmManagement::on_pbEditItem_clicked()
{
	if(ui->tvAttachedTo->currentIndex().isValid() || ui->tvDecisions->currentIndex().isValid() || ui->tvDocTypes->currentIndex().isValid() || ui->tvInstances->currentIndex().isValid() || ui->tvNLA->currentIndex().isValid() || ui->tvSedDocTypes->currentIndex().isValid()) {
		if(ui->twListOfVars->currentIndex()==0 || ui->twListOfVars->currentIndex()==3 || ui->twListOfVars->currentIndex()==5) {
			bool ok;
			QString strLabel;
			QString text;
			if(ui->twListOfVars->currentIndex()==0 || ui->twListOfVars->currentIndex()==5) strLabel=tr("Enter document type:");
			if(ui->twListOfVars->currentIndex()==3) strLabel=tr("Enter instance:");

			if(ui->twListOfVars->currentIndex()==0)
				text = QInputDialog::getText(this, "was", strLabel, QLineEdit::Normal, dboper->stmdlEOoutcomingDocs->record(ui->tvAttachedTo->currentIndex().row()).value("attachedtotype").toString(), &ok);

			else if(ui->twListOfVars->currentIndex()==3)
				text = QInputDialog::getText(this, "was", strLabel, QLineEdit::Normal, dboper->stmdlEOoutcomingDocs->record(ui->tvInstances->currentIndex().row()).value("instance").toString(), &ok);

			else if(ui->twListOfVars->currentIndex()==5)
				text = QInputDialog::getText(this, "was", strLabel, QLineEdit::Normal, dboper->stmdlEOoutcomingDocs->record(ui->tvSedDocTypes->currentIndex().row()).value("seddoctype").toString(), &ok);

			if (ok && !text.isEmpty()) {
				if(ui->twListOfVars->currentIndex()==0)
					dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvAttachedTo->currentIndex().row(),1),text);
				else if(ui->twListOfVars->currentIndex()==3)
					dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvInstances->currentIndex().row(),1),text);
				else if(ui->twListOfVars->currentIndex()==5)
					dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvSedDocTypes->currentIndex().row(),1),text);

				dboper->stmdlEOoutcomingDocs->submitAll();
				UpdateTable(2);
			}
		}
		else {
			if((ui->twListOfVars->currentIndex()==1 && ui->tvDecisions->currentIndex().isValid()) || (ui->twListOfVars->currentIndex()==2 && ui->tvDocTypes->currentIndex().isValid()) || (ui->twListOfVars->currentIndex()==4 && ui->tvNLA->currentIndex().isValid())) {
				if(ui->twListOfVars->currentIndex()==1)
					wTwoFieldsDlg->iRow=ui->tvDecisions->currentIndex().row();
				else if(ui->twListOfVars->currentIndex()==2)
					wTwoFieldsDlg->iRow=ui->tvDocTypes->currentIndex().row();
				else if(ui->twListOfVars->currentIndex()==4)
					wTwoFieldsDlg->iRow=ui->tvNLA->currentIndex().row();
				wTwoFieldsDlg->bIsEdit=true;
				emit initTwoFieldsDlg(ui->twListOfVars->currentIndex());
				wTwoFieldsDlg->setVisible(true);
			}
		}
	}
}

void wgtAdmManagement::on_pbDeleteItem_clicked()
{
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setWindowTitle("was");
	msgQuestion.setText(tr("Are you sure want to delete selected item? This is irreversible operation!"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAddItem->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

	if(msgQuestion.exec()==QMessageBox::Yes) {
		if(ui->twListOfVars->currentIndex()==0)
			dboper->stmdlEOoutcomingDocs->removeRow(ui->tvAttachedTo->currentIndex().row());
		else if(ui->twListOfVars->currentIndex()==1)
			dboper->stmdlEOoutcomingDocs->removeRow(ui->tvDecisions->currentIndex().row());
		else if(ui->twListOfVars->currentIndex()==2)
			dboper->stmdlEOoutcomingDocs->removeRow(ui->tvDocTypes->currentIndex().row());
		else if(ui->twListOfVars->currentIndex()==3)
			dboper->stmdlEOoutcomingDocs->removeRow(ui->tvInstances->currentIndex().row());
		else if(ui->twListOfVars->currentIndex()==4)
			dboper->stmdlEOoutcomingDocs->removeRow(ui->tvNLA->currentIndex().row());
		else if(ui->twListOfVars->currentIndex()==5)
			dboper->stmdlEOoutcomingDocs->removeRow(ui->tvSedDocTypes->currentIndex().row());
		dboper->stmdlEOoutcomingDocs->submitAll();
		UpdateTable(2);
	}
}

void wgtAdmManagement::on_twListOfVars_currentChanged(int index)
{
	switch(index) {
		case 0: //attachedto
			dboper->stmdlEOoutcomingDocs->setTable("attachedtotype");
			dboper->stmdlEOoutcomingDocs->select();
			dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Attachment type"));
			ui->tvAttachedTo->setModel(dboper->stmdlEOoutcomingDocs);
			ui->tvAttachedTo->hideColumn(0);
			ui->tvAttachedTo->resizeColumnsToContents();
			ui->tvAttachedTo->horizontalHeader()->setStretchLastSection(true);
		break;
		case 1: //decisions
			dboper->stmdlEOoutcomingDocs->setTable("decisions");
			dboper->stmdlEOoutcomingDocs->sort(2,Qt::AscendingOrder);
			dboper->stmdlEOoutcomingDocs->select();
			dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Decision type"));
			dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Using form"));
			ui->tvDecisions->setModel(dboper->stmdlEOoutcomingDocs);
			ui->tvDecisions->hideColumn(0);
			ui->tvDecisions->resizeColumnsToContents();
			ui->tvDecisions->horizontalHeader()->setStretchLastSection(true);
		break;
		case 2: //extdocs
			dboper->stmdlEOoutcomingDocs->setTable("doctypes");
			dboper->stmdlEOoutcomingDocs->sort(2,Qt::AscendingOrder);
			dboper->stmdlEOoutcomingDocs->select();
			dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document type"));
			dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Using form"));
			ui->tvDocTypes->setModel(dboper->stmdlEOoutcomingDocs);
			ui->tvDocTypes->hideColumn(0);
			ui->tvDocTypes->resizeColumnsToContents();
			ui->tvDocTypes->horizontalHeader()->setStretchLastSection(true);
		break;
		case 3: //instances
			dboper->stmdlEOoutcomingDocs->setTable("instances");
			dboper->stmdlEOoutcomingDocs->select();
			dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Instance"));
			ui->tvInstances->setModel(dboper->stmdlEOoutcomingDocs);
			ui->tvInstances->hideColumn(0);
			ui->tvInstances->resizeColumnsToContents();
			ui->tvInstances->horizontalHeader()->setStretchLastSection(true);
		break;
		case 4: //nla
			dboper->stmdlEOoutcomingDocs->setTable("npa");
			dboper->stmdlEOoutcomingDocs->sort(2,Qt::AscendingOrder);
			dboper->stmdlEOoutcomingDocs->select();
			dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("NLA"));
			dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Using form"));
			ui->tvNLA->setModel(dboper->stmdlEOoutcomingDocs);
			ui->tvNLA->hideColumn(0);
			ui->tvNLA->resizeColumnsToContents();
			ui->tvNLA->horizontalHeader()->setStretchLastSection(true);
		break;
		case 5: //intdocs
			dboper->stmdlEOoutcomingDocs->setTable("seddoctype");
			dboper->stmdlEOoutcomingDocs->select();
			dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document type"));
			ui->tvSedDocTypes->setModel(dboper->stmdlEOoutcomingDocs);
			ui->tvSedDocTypes->hideColumn(0);
			ui->tvSedDocTypes->resizeColumnsToContents();
			ui->tvSedDocTypes->horizontalHeader()->setStretchLastSection(true);
		break;
	}
}

void wgtAdmManagement::on_tvUsers_doubleClicked(const QModelIndex &index)
{
	ui->pbChangeUserPerm->click();
}

void wgtAdmManagement::on_tvDecisions_doubleClicked(const QModelIndex &index)
{
	ui->pbEditItem->click();
}

void wgtAdmManagement::on_tvDocTypes_doubleClicked(const QModelIndex &index)
{
	ui->pbEditItem->click();
}

void wgtAdmManagement::on_tvInstances_doubleClicked(const QModelIndex &index)
{
	ui->pbEditItem->click();
}

void wgtAdmManagement::on_tvNLA_doubleClicked(const QModelIndex &index)
{
	ui->pbEditItem->click();
}

void wgtAdmManagement::initAdminPanel() {
	ui->tvUsers->setModel(dboper->mdlDB05);
	ui->tvUsers->hideColumn(0);
	ui->tvUsers->resizeColumnsToContents();
	ui->tvUsers->horizontalHeader()->setStretchLastSection(true);
	ui->tvExecutors->setModel(dboper->stmdlEOincomingDocs);
	ui->tvExecutors->hideColumn(0);
	ui->tvExecutors->resizeColumnsToContents();
	ui->tvExecutors->horizontalHeader()->setStretchLastSection(true);
	ui->tvAttachedTo->setModel(dboper->stmdlEOoutcomingDocs);
	ui->tvAttachedTo->hideColumn(0);
	ui->tvAttachedTo->resizeColumnsToContents();
	ui->tvAttachedTo->horizontalHeader()->setStretchLastSection(true);
}

void wgtAdmManagement::on_tvAttachedTo_doubleClicked(const QModelIndex &index)
{
	ui->pbEditItem->click();
}

void wgtAdmManagement::on_tvSedDocTypes_doubleClicked(const QModelIndex &index)
{
	ui->pbEditItem->click();
}

void wgtAdmManagement::setAdmPanelDBOperLink() {
	wUserRights->dboper=dboper;
	wTwoFieldsDlg->dboper=dboper;
}

void wgtAdmManagement::UpdateTable(int iTable) {
	if(iTable==0) dboper->mdlDB05->select();
	else if(iTable==1) dboper->stmdlEOincomingDocs->select();
	else if(iTable==2) dboper->stmdlEOoutcomingDocs->select();
}