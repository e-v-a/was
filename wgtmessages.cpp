#include "wgtmessages.h"
#include "ui_wgtmessages.h"
#include "udatabase.h"

wgtmessages::wgtmessages(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::wgtmessages();
	ui->setupUi(this);
	wNewMessage=new wgtnewmessage(this);
	wNewMessage->setVisible(false);
	connect(this,SIGNAL(ClearNewMessageForm()),wNewMessage,SLOT(ClearNewMessageForm()));
    connect(this,SIGNAL(initNewMessageForm(bool,QString,QString,QString)),wNewMessage,SLOT(initForm(bool,QString,QString,QString)));
    connect(wNewMessage,SIGNAL(SendInternalMessage(QDateTime&,QString,QString,QString,QString,int)),this,SLOT(SendInternalMessageSlot(QDateTime&,QString,QString,QString,QString,int)));
	
}

wgtmessages::~wgtmessages()
{
	delete ui;
}


void wgtmessages::on_pbNew_clicked()
{
	emit ClearNewMessageForm();
	wNewMessage->setWindowFlags(Qt::Dialog);
	wNewMessage->setWindowFlags(wNewMessage->windowFlags() & ~Qt::WindowContextHelpButtonHint);
	wNewMessage->setWindowModality(Qt::WindowModal);
	wNewMessage->stlUsers=stlUsers;
	emit initNewMessageForm(true,"","","");
	wNewMessage->setVisible(true);
}

void wgtmessages::on_pbReply_clicked()
{
	QString strOrigMessage;
	if(ui->tvInbox->currentIndex().isValid()) {
		strOrigMessage="\n\n---"+tr("Original Message")+"---\n";
		strOrigMessage+=tr("Date, time")+":"+ui->lblDateTime->text()+"\n";
		strOrigMessage+=tr("From")+":";
		strOrigMessage+=" " + ui->leFrom->text()+"\n";
		strOrigMessage+=tr("Subject")+":"+ui->leSubject->text()+"\n\n";
		strOrigMessage+=ui->teMessages->toPlainText();

		emit ClearNewMessageForm();
		wNewMessage->setWindowFlags(Qt::Dialog);
		wNewMessage->setWindowFlags(wNewMessage->windowFlags() & ~Qt::WindowContextHelpButtonHint);
		wNewMessage->setWindowModality(Qt::WindowModal);
		wNewMessage->stlUsers=stlUsers;
		emit initNewMessageForm(false,ui->leFrom->text(),"RE:"+ui->leSubject->text(),strOrigMessage);
		wNewMessage->setVisible(true);
	}
}

void wgtmessages::on_pbForward_clicked()
{
	QString strOrigMessage;
	if(ui->tvInbox->currentIndex().isValid() || ui->tvOutbox->currentIndex().isValid()) {
		strOrigMessage="\n\n---"+tr("Original Message")+"---\n";
		strOrigMessage+=tr("Date, time")+":"+ui->lblDateTime->text()+"\n";
		if(ui->tvInbox->currentIndex().isValid()) {
			strOrigMessage+=tr("From")+":";
			strOrigMessage+=" " + ui->leFrom->text()+"\n";
			strOrigMessage+=tr("To")+":";
			strOrigMessage+=" " + strCurrentUser+"\n";
			strOrigMessage+=tr("Subject")+":"+ui->leSubject->text()+"\n\n";
		}
		if(ui->tvOutbox->currentIndex().isValid()) {
			strOrigMessage+=tr("From")+":";
			strOrigMessage+=strCurrentUser+"\n";
			strOrigMessage+=tr("To")+":";
			strOrigMessage+=" " + ui->leFrom->text()+"\n";
			strOrigMessage+=tr("Subject")+":"+ui->leSubject->text()+"\n\n";
		}
		strOrigMessage+=ui->teMessages->toPlainText();

		emit ClearNewMessageForm();
		wNewMessage->setWindowFlags(Qt::Dialog);
		wNewMessage->setWindowFlags(wNewMessage->windowFlags() & ~Qt::WindowContextHelpButtonHint);
		wNewMessage->setWindowModality(Qt::WindowModal);
		wNewMessage->stlUsers=stlUsers;
		emit initNewMessageForm(false,"","FW:"+ui->leSubject->text(),strOrigMessage);
		wNewMessage->setVisible(true);
	}
}

void wgtmessages::on_pbDelete_clicked()
{
	if(ui->tvInbox->currentIndex().isValid() || ui->tvOutbox->currentIndex().isValid()) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setInformativeText(tr("Delete message"));
		msgQuestion.setText(tr("You're going to delete selected message. Are you sure?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) {
			if(ui->twgtMessages->currentIndex()==0) {
//				dbQuery("DB05",3,"","","messages",true,"id="+rmmdlIncoming->record(ui->tvInbox->currentIndex().row()).value("id").toString());
				rmmdlIncoming->select();
			}
			else {
//				dbQuery("DB05",3,"","","messages",true,"id="+rmmdlOutcoming->record(ui->tvOutbox->currentIndex().row()).value("id").toString());
				rmmdlOutcoming->select();
			}
		}
	}
}

void wgtmessages::ConnectMessagesModelToView() {
	ui->leFrom->clear();
	ui->leSubject->clear();
	ui->teMessages->clear();
	ui->wgtPreview->setVisible(false);
	
	rmmdlIncoming=new ReadMessageModel(0,QSqlDatabase::database("DB05"));
	rmmdlOutcoming=new ReadMessageModel(0,QSqlDatabase::database("DB05"));
	rmmdlIncoming->setTable("messages");
	rmmdlOutcoming->setTable("messages");
	ui->tvInbox->setItemDelegateForColumn(8,new CheckBoxDelegate());
	ui->tvOutbox->setItemDelegateForColumn(8,new CheckBoxDelegate());

	QString strBuffer;
	strBuffer="[to]='"+strCurrentUser+"'";

	rmmdlIncoming->setFilter(strBuffer);
	rmmdlIncoming->setSort(1,Qt::DescendingOrder);
	rmmdlIncoming->select();

	strBuffer="[from]='"+strCurrentUser+"'";
	rmmdlOutcoming->setFilter(strBuffer);
	rmmdlOutcoming->setSort(1,Qt::DescendingOrder);
	rmmdlOutcoming->select();

	rmmdlIncoming->setHeaderData(1, Qt::Horizontal, tr("Date, time"));
    rmmdlIncoming->setHeaderData(2, Qt::Horizontal, tr("From"));
    rmmdlIncoming->setHeaderData(3, Qt::Horizontal, tr("To"));
	rmmdlIncoming->setHeaderData(4, Qt::Horizontal, tr("Subject"));
	rmmdlIncoming->setHeaderData(5, Qt::Horizontal, tr("Message"));
    rmmdlIncoming->setHeaderData(6, Qt::Horizontal, tr("Attachment type"));
	rmmdlIncoming->setHeaderData(8, Qt::Horizontal, tr("Read"));

	rmmdlOutcoming->setHeaderData(1, Qt::Horizontal, tr("Date, time"));
    rmmdlOutcoming->setHeaderData(2, Qt::Horizontal, tr("From"));
    rmmdlOutcoming->setHeaderData(3, Qt::Horizontal, tr("To"));
	rmmdlOutcoming->setHeaderData(4, Qt::Horizontal, tr("Subject"));
	rmmdlOutcoming->setHeaderData(5, Qt::Horizontal, tr("Message"));
    rmmdlOutcoming->setHeaderData(6, Qt::Horizontal, tr("Attachment type"));
	rmmdlOutcoming->setHeaderData(8, Qt::Horizontal, tr("Read"));

	ui->tvInbox->setModel(rmmdlIncoming);
	ui->tvInbox->hideColumn(0);
	ui->tvInbox->hideColumn(5);
	ui->tvInbox->hideColumn(6);
	ui->tvInbox->hideColumn(7);

	ui->tvOutbox->setModel(rmmdlOutcoming);
	ui->tvOutbox->hideColumn(0);
	ui->tvOutbox->hideColumn(5);
	ui->tvOutbox->hideColumn(6);
	ui->tvOutbox->hideColumn(7);
	
	ui->tvInbox->resizeColumnsToContents();
	ui->tvInbox->horizontalHeader()->setStretchLastSection(true);
	ui->tvInbox->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	ui->tvOutbox->resizeColumnsToContents();
	ui->tvOutbox->horizontalHeader()->setStretchLastSection(true);
	ui->tvOutbox->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	connect(ui->tvInbox->selectionModel(),SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		this, SLOT(on_SelectionChanged(const QItemSelection &, const QItemSelection &)));

	connect(ui->tvOutbox->selectionModel(),SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		this, SLOT(on_SelectionChanged(const QItemSelection &, const QItemSelection &)));
}

void wgtmessages::on_SelectionChanged(const QItemSelection & selected, const QItemSelection & deselected) {
	if (!ui->wgtPreview->isVisible()) ui->wgtPreview->setVisible(true);
	QDateTime dtBuffer;
	if(ui->twgtMessages->currentIndex()==0) {
		dtBuffer=rmmdlIncoming->record(selected.indexes().first().row()).value(1).toDateTime();
		ui->lblDateTime->setText(dtBuffer.toString("dd.MM.yyyy hh:mm:ss"));
		ui->leFrom->setText(rmmdlIncoming->record(selected.indexes().first().row()).value(2).toString());
		ui->leSubject->setText(rmmdlIncoming->record(selected.indexes().first().row()).value(4).toString());
		ui->teMessages->setText(rmmdlIncoming->record(selected.indexes().first().row()).value(5).toString());
	
		if(rmmdlIncoming->record(selected.indexes().first().row()).value(8).toBool()==false) {
			QString id;
			QModelIndex currentIndex=selected.indexes().first();
			id.setNum(rmmdlIncoming->record(selected.indexes().first().row()).value(0).toInt());
//			dbQuery("DB05",2,"[read]", "true","messages",true,"id="+id);
			rmmdlIncoming->select();
			ui->tvInbox->setCurrentIndex(currentIndex);
			emit UpdateMessagesNum();
		}
	}
	else {
		dtBuffer=rmmdlOutcoming->record(selected.indexes().first().row()).value(1).toDateTime();
		ui->lblDateTime->setText(dtBuffer.toString("dd.MM.yyyy hh:mm:ss"));
		ui->leFrom->setText(rmmdlOutcoming->record(selected.indexes().first().row()).value(3).toString());
		ui->leSubject->setText(rmmdlOutcoming->record(selected.indexes().first().row()).value(4).toString());
		ui->teMessages->setText(rmmdlOutcoming->record(selected.indexes().first().row()).value(5).toString());
	}
}
void wgtmessages::SendInternalMessageSlot(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage,
    QString strAttachedType, int iAttachedDocId) {
    emit SendInternalMessage(dtSendDateTime, strTo, strSubject, strMessage, strAttachedType, iAttachedDocId);
}

void wgtmessages::on_twgtMessages_currentChanged(int index)
{
	if (ui->wgtPreview->isVisible()) ui->wgtPreview->setVisible(false);
	if(index==0) ui->lblFrom->setText("From:");
	else ui->lblFrom->setText("To:");
}
