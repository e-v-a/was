#define DEBUG
#define PRODUCTVERSION	"0.0.2.558"
#include <QApplication>
#include "wgtchangeconnection.h"
#include "wgtauthorize.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	QTranslator translator;
	translator.load("was_ru",qApp->applicationDirPath());
    a.installTranslator(&translator);
	
#ifdef DEBUG
	//BUILD AUTOINCREMENT
	QFile buildfile("buildnum.txt");
	if (!buildfile.open(QIODevice::ReadOnly | QIODevice::Text)) qDebug() <<  "Cannot open a file 4 reading";
    else {
		int iLine;
		QTextStream in(&buildfile);
		iLine = in.readLine().toInt();
		iLine++;
		QString strBuf;
		strBuf.setNum(iLine);
		buildfile.close();
		if (!buildfile.open(QIODevice::WriteOnly | QIODevice::Text))	qDebug() <<  "Cannot open a file 4 writing";
		else {
			QTextStream out(&buildfile);
			out << strBuf;
			out << "\n";
			buildfile.close();
		}
	}
#endif

	wgtAuthorize wgtauth;
    wgtChangeConnection wgtCCN;
	DBOperations dboper;
	QSqlDatabase sqlDB;
	bool bFirstRun=false;
	bool bUpdate=false;
    bool bUseLocalDB=false;

	QFile flSettings(QString("%1/settings").arg(a.applicationDirPath()));
	if(!flSettings.exists()) bFirstRun=true;
	
	QString strBuffer;
	QString strBuffer2;
	QString strUpdatePath;
	QString strCommonFolder;
	QString strPublishDir;
	QString strLastUser="";

	QString strArgv;
	bool bChangeConnection=false;
	if(argc>1) {
		for(int i=0;i<argc;i++) {
			strArgv=argv[i];
			if(strArgv=="switch_connection") bChangeConnection=true;
			else if(strArgv=="console") dboper.EnvSettings.bConsole=true;
		}
	}
	
	if(!dboper.dbConnLocal(QString("%1/settings").arg(a.applicationDirPath()), "dbsettings") && dboper.dbLocalConnectionError.type() != QSqlError::NoError) 
		QMessageBox::warning(0, QObject::tr("Unable to read settings"),	
		QObject::tr("An error occurred while opening the database: ") + dboper.dbLocalConnectionError.text());
	
	if(bFirstRun==true) {
		QFileDialog fldPath;

        QMessageBox msgQuestion;
        msgQuestion.setParent(0);
        msgQuestion.setWindowFlags(Qt::Dialog);
        msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
        msgQuestion.setIcon(QMessageBox::Question);
        msgQuestion.setWindowTitle("was");
        msgQuestion.setText(QObject::tr("Use local database?"));
        msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgQuestion.setDefaultButton(QMessageBox::Yes);

        msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
        msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

        msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
        msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
        msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
        msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
        msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
        msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
        msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
        msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);

        msgQuestion.button(QMessageBox::Yes)->setText(QObject::tr("Yes"));
        msgQuestion.button(QMessageBox::No)->setText(QObject::tr("No"));

        QString strServer="";
        if(msgQuestion.exec()==QMessageBox::Yes) {
            bUseLocalDB=true;
            strBuffer=fldPath.getOpenFileName(0,QObject::tr("Select department's database path"),a.applicationDirPath(),"All files (*.*)");
            strBuffer2=fldPath.getOpenFileName(0,QObject::tr("Select governance's database path"),a.applicationDirPath(),"All files (*.*)");
        }
        else wgtCCN.show();

        strCommonFolder=fldPath.getExistingDirectory(0,QObject::tr("Select common directory"),a.applicationDirPath());
		strPublishDir=fldPath.getExistingDirectory(0,QObject::tr("Select directory for publishing documents"),a.applicationDirPath());
		strUpdatePath=fldPath.getExistingDirectory(0,QObject::tr("Select update directory"),a.applicationDirPath());
				
        dboper.dbQuery(PRIVATE_QUERY,"dbsettings",4,"LocalDB boolean not null CHECK (LocalDB IN (0,1)),Server varchar(300) null,DB05Path varchar(300) null, LoginDB05 varchar(300) null,PwdDB05 varchar(300) null,mainbase varchar(300) null, LoginMB varchar(300) null,PwdMB varchar(300) null,commonfolder varchar(300) null,updatepath varchar(300) null, publishpath varchar (300) null,lastuser varchar(300) null, currentversion varchar(20) null,mwstate integer,mwwidth integer,mwheight integer,sync boolean  not null CHECK (sync IN (0,1))","","main",false,"");
		dboper.dbQuery(PRIVATE_QUERY,"dbsettings",4,"form varchar(25) null, sizeconst varchar(250) null","","sizeconst",false,"");
        if(bUseLocalDB)
            dboper.dbQuery(PRIVATE_QUERY,"dbsettings",1,"LocalDB,DB05Path,mainbase, commonfolder, updatepath, publishpath, currentversion,mwstate,mwwidth,mwheight,sync","0,'"+strBuffer+"','"+strBuffer2+"','"+strCommonFolder+"','"+strUpdatePath+"','"+strPublishDir+"','"+PRODUCTVERSION+"',0,750,615,0","main",false,"");
        else
            dboper.dbQuery(PRIVATE_QUERY,"dbsettings",1,"LocalDB,Server,DB05Path,LoginDB05,PwdDB05,mainbase, LoginMB,PwdMB, commonfolder, updatepath, publishpath, currentversion,mwstate,mwwidth,mwheight,sync","1,'"+wgtCCN.strServerAddr+"','"+wgtCCN.strDepDB+"','"+wgtCCN.strOffDBLogin+"','"+wgtCCN.strOffDBPwd+"','"+wgtCCN.strOffDB+"','"+wgtCCN.strDepDBLogin+"','"+wgtCCN.strDepDBPwd+"','"+strCommonFolder+"','"+strUpdatePath+"','"+strPublishDir+"','"+PRODUCTVERSION+"',0,750,615,0","main",false,"");

		dboper.EnvSettings.strCommonFolder=strCommonFolder;
		dboper.EnvSettings.strPublishFolder=strPublishDir;
		dboper.EnvSettings.bMWState=false;
		dboper.EnvSettings.iMWWidth=750;
		dboper.EnvSettings.iMWHeight=615;
        dboper.EnvSettings.bSync=false;
		dboper.mdlSettings=new QSqlTableModel(0,QSqlDatabase::database("dbsettings"));
		dboper.mdlSettings->setTable("main");
		dboper.mdlSettings->select();
	}
	else {
		dboper.mdlSettings=new QSqlTableModel(0,QSqlDatabase::database("dbsettings"));
		dboper.mdlSettings->setTable("main");
		dboper.mdlSettings->select();
        bUseLocalDB=dboper.mdlSettings->record(0).value("LocalDB").toBool();
		//qDebug()<<bUseLocalDB;
		//bUseLocalDB=true; //������� ���� ������ ������� ����������
        
		if(bUseLocalDB) {
            strBuffer=dboper.mdlSettings->record(0).value("DB05Path").toString();
            strBuffer2=dboper.mdlSettings->record(0).value("mainbase").toString();
        }
        else {
            wgtCCN.strServerAddr=dboper.mdlSettings->record(0).value("Server").toString();
			strBuffer=dboper.mdlSettings->record(0).value("DB05Path").toString(); //�������, ����� ����������� ����������� �������� ������� ������
          /* wgtCCN.strDepDB=dboper.mdlSettings->record(0).value("DB05Path").toString();
            wgtCCN.strDepDBLogin=dboper.mdlSettings->record(0).value("LoginDB05").toString();
            wgtCCN.strDepDBPwd=dboper.mdlSettings->record(0).value("PwdDB05").toString();*/ //��������, ����� ����������� ����������� �������� ������� ������
            wgtCCN.strOffDB=dboper.mdlSettings->record(0).value("mainbase").toString();
            wgtCCN.strOffDBLogin=dboper.mdlSettings->record(0).value("LoginMB").toString();
            wgtCCN.strOffDBPwd=dboper.mdlSettings->record(0).value("PwdMB").toString();
        }

        strLastUser=dboper.mdlSettings->record(0).value("lastuser").toString();
        strUpdatePath=dboper.mdlSettings->record(0).value("updatepath").toString();

        dboper.EnvSettings.strCommonFolder=dboper.mdlSettings->record(0).value("commonfolder").toString();
        dboper.EnvSettings.strPublishFolder=dboper.mdlSettings->record(0).value("publishpath").toString();
        dboper.EnvSettings.bMWState=dboper.mdlSettings->record(0).value("mwstate").toBool();
        dboper.EnvSettings.iMWWidth=dboper.mdlSettings->record(0).value("mwwidth").toInt();
        dboper.EnvSettings.iMWHeight=dboper.mdlSettings->record(0).value("mwheight").toInt();

		dboper.EnvSettings.bSync=dboper.mdlSettings->record(0).value("sync").toBool();
	}
	
	QString strUpdateDB=strUpdatePath;
	strUpdateDB+="/update.mdb";
	if(!dboper.dbConn(true,strUpdateDB, "UPDATEDB") && dboper.dbConnectionError.type() != QSqlError::NoError) 
		QMessageBox::warning(0, QObject::tr("Unable to open database"),	
		QObject::tr("An error occurred while opening the connection: ") + dboper.dbConnectionError.text());
	else {
		dboper.dbQuery(PUBLIC_QUERY,"UPDATEDB",0,"*","","main",false,"");
		dboper.query.next();
		QString strActualVersion=dboper.query.value("actualversion").toString();
	
		if(strActualVersion!=PRODUCTVERSION) {
			QProcess *Update = new QProcess;
			QString strExec=strUpdatePath;
			strExec+="/";
			strExec+=PRODUCTVERSION;
			strExec+=".exe";//+strUpdateID;
			bUpdate=true;
			Update->startDetached("\""+strExec+"\"");
			Update->waitForStarted(5000);
		}
	}
		
	if(!bUpdate) { //��������, ��� ��������� ������� ������
		QFileDialog fldPath;
		QFile flBase(strBuffer);
		//if(bUseLocalDB) {
			if(!flBase.exists()) {
				QMessageBox::warning(0, QObject::tr("Department's database not found"),
				QObject::tr("Please specify the right path"));
				//FILE OPEN
				strBuffer=fldPath.getOpenFileName(0,QObject::tr("Select department's database path"),a.applicationDirPath(),"All files (*.*)");
				dboper.dbQuery(PRIVATE_QUERY,"dbsettings",2,"DB05Path","'"+strBuffer+"'","main",false,"");
				//it might be better to cycle file check until it won't found required files or user will close app
			}

			if(!dboper.dbConn(true,strBuffer, "DB05") && dboper.dbConnectionError.type() != QSqlError::NoError)
                QMessageBox::warning(0, QObject::tr("Unable to open database"),
                    QObject::tr("An error occurred while opening the connection: ") + dboper.dbConnectionError.text());
            else {
                dboper.dbQuery(PUBLIC_QUERY,"DB05",0,"*","","users",false,"");
                while (dboper.query.next()) wgtauth.stlUsers.append(dboper.query.value("username").toString());
            }
        //}
        //else {
            //NETWORK CONNECTION
        //}

		wgtauth.dboper=&dboper;

		if(strLastUser!="") wgtauth.initAuthForm(true,strLastUser);
		else wgtauth.initAuthForm(false,"");
		
        if(bUseLocalDB) {
            flBase.setFileName(strBuffer2);
            if(!flBase.exists()) {
                QMessageBox::warning(0, QObject::tr("Governance's database not found"),
                QObject::tr("Please specify the right path"));
                //FILE OPEN
                strBuffer=fldPath.getOpenFileName(0,QObject::tr("Select governance's database path"),a.applicationDirPath(),"All files (*.*)");
                dboper.dbQuery(PRIVATE_QUERY,"dbsettings",2,"mainbase","'"+strBuffer+"'","main",false,"");
            }

            if(!dboper.dbConn(true,strBuffer2, "officeDB") && dboper.dbConnectionError.type() != QSqlError::NoError)
                QMessageBox::warning(0, QObject::tr("Unable to open database"),
                QObject::tr("An error occurred while opening the connection: ") + dboper.dbConnectionError.text());
        }
        else {
            //NETWORK CONNECTION
			if(!dboper.dbConn(false,wgtCCN.strOffDB, "officeDB",wgtCCN.strServerAddr,wgtCCN.strOffDBLogin,wgtCCN.strOffDBPwd) && dboper.dbConnectionError.type() != QSqlError::NoError)
                QMessageBox::warning(0, QObject::tr("Unable to open database"),
                QObject::tr("An error occurred while opening the connection: ") + dboper.dbConnectionError.text());
        }
		dboper.query.finish();

		dboper.dbCloseConn("UPDATEDB");
		wgtauth.show();
		return a.exec();
	}
	else {
		dboper.query.finish();
		dboper.query=QSqlQuery();
		dboper.dbCloseConn("UPDATEDB");
		return 8;
	}
}