#ifndef WGTCHANGECONNECTION_H
#define WGTCHANGECONNECTION_H

#include <QWidget>

namespace Ui {
class wgtChangeConnection;
}

class wgtChangeConnection : public QWidget
{
    Q_OBJECT

public:
    explicit wgtChangeConnection(QWidget *parent = 0);
    QString strServerAddr;
    QString strOffDB;
    QString strOffDBLogin;
    QString strOffDBPwd;
    QString strDepDB;
    QString strDepDBLogin;
    QString strDepDBPwd;

    ~wgtChangeConnection();

private:
    Ui::wgtChangeConnection *ui;
};

#endif // WGTCHANGECONNECTION_H
