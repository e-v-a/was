#ifndef WGTARCHIVE_H
#define WGTARCHIVE_H

#include <QWidget>
#include "wgtarchivehistory.h"

namespace Ui {
class wgtarchive;
}

class wgtarchive : public QWidget
{
    Q_OBJECT

public:
    explicit wgtarchive(QWidget *parent = 0);
    ~wgtarchive();
	DBOperations *dboper;
	wgtarchivehistory *wArcHistory;

public slots:
	void ConnectSlots();
	void ConnectArchiveModelToView();
	void BackToArchive(int iMode);

private slots:
    void on_tvEO_doubleClicked(const QModelIndex &index);
	void on_tvClaims_doubleClicked(const QModelIndex &index);
	void on_leSearch_textChanged(const QString &arg1);
	void on_twArchive_currentChanged(int index);
	void on_tvCases_doubleClicked(const QModelIndex &index);
    void on_tvAdmCases_doubleClicked(const QModelIndex &index);
	void on_tvLawsuites_doubleClicked(const QModelIndex &index);

signals:
	void SetArcHistoryFields(int iRow);

private:
    Ui::wgtarchive *ui;
};

#endif // WGTARCHIVE_H
