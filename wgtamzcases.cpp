#include <qmessagebox.h>
#include "wgtamzcases.h"
#include "ui_wgtamzcases.h"
#include "datedelegate.h"

wgtAMZCases::wgtAMZCases(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAMZCases)
{
    ui->setupUi(this);
	iRowIndex=-1;
	wAddOutDoc = new wgtaddoutdoc(this);
	wAddOutDoc->setVisible(false);
    wAddOutDoc->setWindowFlags(Qt::Dialog);
    wAddOutDoc->setWindowFlags(wAddOutDoc->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wAddOutDoc->setWindowModality(Qt::WindowModal);
	connect(wAddOutDoc,SIGNAL(updateACSTable()),this,SLOT(updateACSTable()));
    actEdit=new QAction(tr("Edit"),this);
    actDelete=new QAction(tr("Delete"),this);
	actSearch=new QAction(tr("Search"),this);
	actSearch->setCheckable(true);
	QList<QKeySequence> scList;
	scList.append(QKeySequence(Qt::CTRL + Qt::Key_F));
	scList.append(QKeySequence(Qt::Key_F3));
	actSearch->setShortcuts(scList);
    connect(actEdit, SIGNAL(triggered()), this, SLOT(EditAction()));
    connect(actDelete, SIGNAL(triggered()), this, SLOT(DeleteAction()));
	connect(actSearch, SIGNAL(toggled(bool)), this, SLOT(on_pbShowSearch_toggled(bool)));
	addAction(actSearch);
	ui->tvDocuments->addAction(actEdit);
	ui->tvDocuments->addAction(actDelete);
	ui->tvDocuments->setContextMenuPolicy(Qt::ActionsContextMenu);
	ui->pbSend2Agreement->setVisible(false);
	ui->pbPublish->setVisible(false);
    bIsSearch=false;
	ui->lblNoMatches->setVisible(false);
	ui->wgtArcNotify->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtAMZCases::~wgtAMZCases()
{
    delete ui;
}

void wgtAMZCases::on_pbSearch_clicked()
{
    QString strSearchString;
    if(ui->leSearch->text()!="") {
        bIsSearch=true;
        QString arg1=ui->leSearch->text();
        strSearchString="([claimnum] LIKE '%"+arg1+"%' OR [casenum] LIKE '%"+arg1+"%' OR [decreenum] LIKE '%"+arg1+"%' OR [complainant] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1;
        strSearchString+="%' OR [decisionnum] LIKE '%"+arg1+"%') AND ([executor]='";
        strSearchString+=dboper->UsrSettings.sUserName+"') ORDER BY [claimdate] DESC, [claimnum] DESC";

        dboper->mdlDB05->setFilter(strSearchString);
        dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount() == 0) {
			ui->wgtArcNotify->setVisible(true);
			ui->lblNoMatches->setVisible(true);
		}
        else if(dboper->mdlDB05->rowCount() > 0) {
			ui->lblNoMatches->setVisible(false);
			ui->wgtArcNotify->setVisible(false);
            iRowCount=dboper->mdlDB05->rowCount();
            iCurrentRow=0;
            emit setStatusText(iCurrentRow+1, iRowCount);
            SetCasesFormFields(iCurrentRow);
        }
    }
}

void wgtAMZCases::on_pbDeleteCase_clicked()
{
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setWindowTitle("was");
	msgQuestion.setText(tr("You're going to delete the case. This will cause the removal of all attached documents. Are you sure?"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAdd->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);
	
	if(msgQuestion.exec()==QMessageBox::Yes) {
		QSqlQuery query;
		QString strBuffer;
		QString strTextFile;
		QString strScanFile;
		strBuffer.setNum(iRowIndex);
		QFile flAttachment;
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","documents",true,"[attachedtodoctype]='acs' AND [attachedtodocid]="+strBuffer);
		strBuffer=dboper->EnvSettings.strCommonFolder+"/";
		while(dboper->query.next()) {
			strTextFile=dboper->query.value("textfile").toString();
			if(strTextFile!="") {
				flAttachment.setFileName(strBuffer+strTextFile);
				flAttachment.remove();
			}
			strScanFile=dboper->query.value("scanfile").toString();
			if(strScanFile!="") {
				flAttachment.setFileName(strBuffer+strScanFile);
				flAttachment.remove();
			}
		}
		strBuffer.setNum(iRowIndex);
		dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,"[attachedtodoctype]='acs' AND [attachedtodocid]="+strBuffer);
		dboper->dbQuery(PRIVATE_QUERY,"DB05",2,"[casenum]","''","amzclaims",true,"[casenum]='"+ui->leCaseNum->text()+"'");
		dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","amzcases",true,"[id]="+strBuffer);
	}
	ui->pbFirst->click();
}

void wgtAMZCases::on_pbFirst_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=0;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetCasesFormFields(iCurrentRow);
}

void wgtAMZCases::on_pbPrev_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow--;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetCasesFormFields(iCurrentRow);
}

void wgtAMZCases::on_pbSave_clicked()
{
	QStringList stlFields;
	QList<QVariant> stlValues;

	stlFields.append("3");//casenum
	stlValues.append(ui->leCaseNum->text());
	stlFields.append("4");//decreenum
	stlValues.append(ui->leDecreeNum->text());
	stlFields.append("5");//decreedate
	stlValues.append(ui->deDecreeDate->date());
	stlFields.append("8");//complainant
	stlValues.append(ui->teComplainant->toPlainText());
	stlFields.append("9");//defendant
	stlValues.append(ui->teDefendant->toPlainText());
	stlFields.append("10");//summary (decisionnum)
	stlValues.append(ui->teSummary->toPlainText());
	if(ui->cbProlong->isChecked()) {
		stlFields.append("11");//prolongdate (decisiondate)
		stlValues.append(ui->deTill->date());
	}
	stlFields.append("12");//clause
	stlValues.append(ui->leClause->text());
	stlFields.append("13");//nla
	stlValues.append(ui->cbNLA->currentText());
	stlFields.append("15");//submissiondate
	stlValues.append(ui->dteSubmissionDate->date());
	stlFields.append("16");//committeedate
	stlValues.append(ui->dteCommitteeDate->dateTime());
	stlFields.append("17");//result
	stlValues.append(ui->cbResult->currentText());
	stlFields.append("18");//clauses
	stlValues.append(ui->leClauses->text());
	stlFields.append("19");//order
	stlValues.append(ui->cbOrder->isChecked());
	stlFields.append("20");//orderexec
	stlValues.append(ui->cbOrderExecuted->isChecked());
	stlFields.append("21");//execinfo
	stlValues.append(ui->leExecInfo->text());
	stlFields.append("22");//prolong
	stlValues.append(ui->cbProlong->isChecked());
	stlFields.append("23");//prolong
	stlValues.append(ui->deExcitationDate->date());
	
	ui->pbSave->setEnabled(!dboper->dbWriteData(false,true,iCurrentRow,stlFields,stlValues));
	bFormChanged=ui->pbSave->isEnabled();
}

void wgtAMZCases::on_pbNext_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow++;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetCasesFormFields(iCurrentRow);
}

void wgtAMZCases::on_pbLast_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=iRowCount-1;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetCasesFormFields(iCurrentRow);
}

void wgtAMZCases::on_pbAdd_clicked()
{
	wAddOutDoc->setAODVars("acs", iRowIndex);
    wAddOutDoc->setVisible(true);
}

void wgtAMZCases::on_pbSend2Agreement_clicked()
{
	if(ui->tvDocuments->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("agreed").toBool()) {
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvDocuments->currentIndex().row(),8),true);
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvDocuments->currentIndex().row(),9),false);
		dboper->stmdlEOoutcomingDocs->submitAll();
		ui->pbSend2Agreement->setVisible(false);
		ui->pbPublish->setVisible(false);
	}
}

void wgtAMZCases::on_pbPublish_clicked()
{
	if(ui->tvDocuments->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("agreed").toBool()) {
		QSqlRecord record=dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row());
		record.setValue("senttoagreement",true);
		dboper->stmdlEOoutcomingDocs->setRecord(iCurrentRow,record);
		dboper->stmdlEOoutcomingDocs->submitAll();
		record.clear();
		ui->pbPublish->setVisible(false);
	}
}

void wgtAMZCases::SetCasesFormFields(int recordnum) {
	ui->leCaseNum->clear();
	ui->leClaimNum->clear();
	ui->deClaimDate->clear();
	ui->deHandoverDate->clear();
	ui->leExecutor->clear();
	ui->leClause->clear();
	ui->leDecreeNum->clear();
	ui->deDecreeDate->setDate(QDate(2013,1,1));
	ui->teComplainant->clear();
	ui->teDefendant->clear();
	ui->teSummary->clear();
	ui->dteSubmissionDate->setDate(QDate(2013,1,1));
	ui->dteCommitteeDate->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
    ui->wgtSearch->setVisible(bIsSearch);
	ui->cbProlong->setChecked(false);
	ui->deExcitationDate->setDate(QDate(2013,1,1));
	ui->deTill->setDate(QDate(2013,1,1));
	ui->deTill->setVisible(false);
		
	iRowIndex=dboper->mdlDB05->record(recordnum).value("id").toInt();
	ui->leClaimNum->setText(dboper->mdlDB05->record(recordnum).value("claimnum").toString());
	ui->deClaimDate->setDate(dboper->mdlDB05->record(recordnum).value("claimdate").toDate());
	ui->leCaseNum->setText(dboper->mdlDB05->record(recordnum).value("casenum").toString());
	ui->leDecreeNum->setText(dboper->mdlDB05->record(recordnum).value("decreenum").toString());
	ui->deDecreeDate->setDate(dboper->mdlDB05->record(recordnum).value("decreedate").toDate());
	ui->deHandoverDate->setDate(dboper->mdlDB05->record(recordnum).value("handoverdate").toDate());
	ui->leExecutor->setText(dboper->mdlDB05->record(recordnum).value("executor").toString());
	ui->teComplainant->setText(dboper->mdlDB05->record(recordnum).value("complainant").toString());
	ui->teDefendant->setText(dboper->mdlDB05->record(recordnum).value("defendant").toString());
	ui->leClause->setText(dboper->mdlDB05->record(recordnum).value("clause").toString());
	ui->teSummary->setText(dboper->mdlDB05->record(recordnum).value("summary").toString());
	ui->deExcitationDate->setDate(dboper->mdlDB05->record(recordnum).value("excitationdate").toDate());
	ui->cbProlong->setChecked(dboper->mdlDB05->record(recordnum).value("prolong").toBool());
	if(ui->cbProlong->isChecked()) ui->deTill->setDate(dboper->mdlDB05->record(recordnum).value("prolongdate").toDate());
	ui->lblTill->setVisible(ui->cbProlong->isChecked());
	ui->deTill->setVisible(ui->cbProlong->isChecked());
	ui->dteSubmissionDate->setDate(dboper->mdlDB05->record(recordnum).value("submissiondate").toDate());
	ui->dteCommitteeDate->setDateTime(dboper->mdlDB05->record(recordnum).value("committeedate").toDateTime());

	if(ui->cbNLA->count()==0) ui->cbNLA->addItems(stlNLA);
	ui->cbNLA->setCurrentIndex(ui->cbNLA->findText(dboper->mdlDB05->record(recordnum).value("nla").toString(),Qt::MatchExactly));

	if(ui->cbResult->count()==0) ui->cbResult->addItems(stlDecisionTypes);
	ui->cbResult->setCurrentIndex(ui->cbResult->findText(dboper->mdlDB05->record(recordnum).value("result").toString(),Qt::MatchExactly));

	ui->cbOrder->setChecked(dboper->mdlDB05->record(recordnum).value("order").toBool());
	ui->cbOrderExecuted->setVisible(ui->cbOrder->isChecked());
	ui->cbOrderExecuted->setChecked(dboper->mdlDB05->record(recordnum).value("orderexec").toBool());
	ui->lblExecInfo->setVisible(dboper->mdlDB05->record(recordnum).value("orderexec").toBool());
	ui->leExecInfo->setVisible(dboper->mdlDB05->record(recordnum).value("orderexec").toBool());
	if(dboper->mdlDB05->record(recordnum).value("orderexec").toBool()) ui->leExecInfo->setText(dboper->mdlDB05->record(recordnum).value("execinfo").toString());
	ui->leClauses->setText(dboper->mdlDB05->record(recordnum).value("clauses").toString());

	dboper->stmdlEOincomingDocs->setTable("incoming");
	QString strFilter;
	strFilter="[attachedto] = '";
	strFilter+=tr("Case");
	strFilter+="' AND [attachedtodocid]=";
	QString strBuffer;
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;
	
	dboper->stmdlEOincomingDocs->setFilter(strFilter);
	dboper->stmdlEOincomingDocs->select();
	dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
	dboper->stmdlEOincomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOincomingDocs->setHeaderData(8, Qt::Horizontal, tr("Addressee"));
	dboper->stmdlEOincomingDocs->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->stmdlEOincomingDocs->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	ui->tvIncoming->setModel(dboper->stmdlEOincomingDocs);
	ui->tvIncoming->hideColumn(0);
	int i;
	for(i=3;i<8;i++) ui->tvIncoming->hideColumn(i);
	ui->tvIncoming->hideColumn(9);
	for(i=12;i<20;i++) ui->tvIncoming->hideColumn(i);
	
	ui->tvIncoming->resizeColumnsToContents();
	ui->tvIncoming->horizontalHeader()->setStretchLastSection(true);

    dboper->stmdlEOoutcomingDocs->setTable("documents");
    strFilter="[attachedtodoctype] = 'acs'";
    strFilter+=" AND [attachedtodocid]=";
    strBuffer.setNum(iRowIndex);
    strFilter+=strBuffer;

    dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
    dboper->stmdlEOoutcomingDocs->select();
    dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document number"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Document date"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Reg Num"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Document Type"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Post ID"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Execution date"));
	ui->tvDocuments->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(7, new DataDelegate());
    ui->tvDocuments->setModel(dboper->stmdlEOoutcomingDocs);
    ui->tvDocuments->hideColumn(0);
	
    for(i=8;i<16;i++) ui->tvDocuments->hideColumn(i);
    ui->tvDocuments->resizeColumnsToContents();
    ui->tvDocuments->horizontalHeader()->setStretchLastSection(true);

	ui->pbSave->setEnabled(false);
	bFormChanged=false;

	ui->leCaseNum->setEnabled(!iRowCount<1);
	ui->leClause->setEnabled(!iRowCount<1);
	ui->leDecreeNum->setEnabled(!iRowCount<1);
	ui->deDecreeDate->setEnabled(!iRowCount<1);
	ui->teComplainant->setEnabled(!iRowCount<1);
	ui->teDefendant->setEnabled(!iRowCount<1);
    ui->leClause->setEnabled(!iRowCount<1);
    ui->teSummary->setEnabled(!iRowCount<1);
    ui->deExcitationDate->setEnabled(!iRowCount<1);
    ui->cbProlong->setEnabled(!iRowCount<1);
    ui->deTill->setEnabled(!iRowCount<1);
	ui->dteSubmissionDate->setEnabled(!iRowCount<1);
	ui->dteCommitteeDate->setEnabled(!iRowCount<1);
	ui->cbResult->setEnabled(!iRowCount<1);
	ui->twIODocs->setEnabled(!iRowCount<1);
	ui->pbFirst->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbPrev->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbNext->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	ui->pbLast->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
}

void wgtAMZCases::on_leClause_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_leCaseNum_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_leDecreeNum_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_deDecreeDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_teComplainant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_teDefendant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_leDecisionNum_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_deDecisionDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_teRequests_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_dteSubmissionDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_dteCommitteeDate_dateTimeChanged(const QDateTime &dateTime)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::on_cbResult_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtAMZCases::updateACSTable() {
	dboper->stmdlEOoutcomingDocs->select();
}

void wgtAMZCases::EditAction() {
    if(ui->tvDocuments->currentIndex().isValid()) {
		wAddOutDoc->setAODVars("acs", iRowIndex,true,dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("id").toInt());
		wAddOutDoc->setVisible(true);
	}
}

void wgtAMZCases::DeleteAction() {
	if(ui->tvDocuments->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("agreed").toBool()) {
		QString strID;
		QString strSelectString;
		QMessageBox msgQuestion;
		
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Delete document"));
		msgQuestion.setText(tr("You're going to delete selected document. Are you sure?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		
		msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
		msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
		
		msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
		msgQuestion.button(QMessageBox::No)->setText(tr("No"));

		QFont fntBold;
		fntBold.setFamily(ui->pbAdd->font().family());
		fntBold.setPointSize(8);
		fntBold.setBold(true);
		msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);
		if(msgQuestion.exec()==QMessageBox::Yes)
			wAddOutDoc->deleteDocument(dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("id").toInt());

		dboper->stmdlEOoutcomingDocs->select();
	}
}

void wgtAMZCases::on_tvDocuments_doubleClicked(const QModelIndex &index)
{
    EditAction();
}

void wgtAMZCases::setCasesDBOperLink() {
	wAddOutDoc->dboper=dboper;
}

void wgtAMZCases::on_tvDocuments_clicked(const QModelIndex &index)
{
	if(!dboper->stmdlEOoutcomingDocs->record(index.row()).value("senttoagreement").toBool()) ui->pbSend2Agreement->setVisible(true);
	else ui->pbSend2Agreement->setVisible(false);
	if((dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()==tr("Award") || dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()==tr("Injunction") || dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()==tr("Decision")) && dboper->stmdlEOoutcomingDocs->record(index.row()).value("agreed").toBool() && !dboper->stmdlEOoutcomingDocs->record(index.row()).value("published").toBool()) ui->pbPublish->setVisible(true);
	else ui->pbPublish->setVisible(false);
}

void wgtAMZCases::on_leSearch_returnPressed()
{
    ui->pbSearch->click();
}

void wgtAMZCases::on_pbClear_clicked()
{
    QString strFilter;
    ui->leSearch->clear();
    strFilter="[executor] = '"+dboper->UsrSettings.sUserName+"' ORDER BY [claimdate] DESC, [claimnum] DESC";
    dboper->mdlDB05->setFilter(strFilter);
    dboper->mdlDB05->select();

    if(dboper->mdlDB05->rowCount() > 0) {
        iRowCount=dboper->mdlDB05->rowCount();
        iCurrentRow=0;
        emit setStatusText(iCurrentRow+1, iRowCount);
        SetCasesFormFields(iCurrentRow);
    }
	ui->lblNoMatches->setVisible(false);
}

void wgtAMZCases::on_pbShowSearch_toggled(bool checked)
{
    if(ui->pbSave->isEnabled()) AskSaving();
    bIsSearch=checked;
	ui->wgtSearch->setVisible(checked);
    ui->leSearch->clear();
    (checked)?ui->leSearch->setFocus():ui->pbClear->click();
	ui->pbShowSearch->setChecked(checked);
	actSearch->setChecked(checked);
    ui->lblNoMatches->setVisible(false);
	ui->pbClear->setVisible(false);
}

void wgtAMZCases::clearACSSearch() {
	bIsSearch=false;
	ui->leSearch->clear();
	ui->pbClear->setVisible(false);
	ui->pbShowSearch->setChecked(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
}

void wgtAMZCases::on_leSearch_textChanged(const QString &arg1)
{
    if(arg1!="") ui->pbClear->setVisible(true);
    else ui->pbClear->setVisible(false);
}

void wgtAMZCases::on_pbArc_clicked()
{
	QString strFields;
	QString strValues;
	QString strBuffer;
	bool bNotAgreed=false;
	if(dboper->stmdlEOoutcomingDocs->rowCount()>0) {
		for(int i=0;i<dboper->stmdlEOoutcomingDocs->rowCount();i++) {
			if(dboper->stmdlEOoutcomingDocs->record(i).value("agreed").toBool()) {
				if((dboper->stmdlEOoutcomingDocs->record(i).value("doctype").toString()==tr("Award") || dboper->stmdlEOoutcomingDocs->record(i).value("doctype").toString()==tr("Injunction") || dboper->stmdlEOoutcomingDocs->record(i).value("doctype").toString()==tr("Decision")) && (!dboper->stmdlEOoutcomingDocs->record(i).value("published").toBool())) {
					QMessageBox::warning(0, tr("Archiving is not possible"),	
						tr("One or several outcoming documents are not agreed or not published. Archiving is not possible until all procedural documents are agreed and published."));
					bNotAgreed=true;
					break;
				}
			}
			else {
				QMessageBox::warning(0, tr("Archiving is not possible"),	
					tr("One or several outcoming documents are not agreed or not published. Archiving is not possible until all procedural documents are agreed and published."));
				bNotAgreed=true;
				break;
			}
		}
	}
	if(!bNotAgreed) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Archive claim"));
		msgQuestion.setText(tr("You're going to archive current claim. Any information about claim will be freezed. Do you want to continue?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) {
			strFields="[claimnum],[claimdate],[casenum],[decreenum],[decreedate],[handoverdate],[executor],[complainant],[defendant],[summary]";
			if(ui->cbProlong->isChecked()) strFields+=",[prolongdate]";
			strFields+=",[clause],[nla],[submissiondate],[committeedate],[result],[clauses],[order],[orderexec],[execinfo],[prolong],[excitationdate]";


			strValues=ui->leClaimNum->text() + ",#" + ui->deClaimDate->date().toString("MM/dd/yyyy") + "#,'";
			strValues+=ui->leCaseNum->text() + "','" + ui->leDecreeNum->text() + "',#" + ui->deDecreeDate->date().toString("MM/dd/yyyy");
			strValues+="#,#" + ui->deHandoverDate->date().toString("MM/dd/yyyy") + "#,'";
			strValues+=ui->leExecutor->text() + "','" + ui->teComplainant->toPlainText() + "','" + ui->teDefendant->toPlainText();
			strValues+="','" + ui->teSummary->toPlainText() + "',";
			if(ui->cbProlong->isChecked()) strValues+="#"+ui->deTill->date().toString("MM/dd/yyyy") + "#,";

			strValues+="'" + ui->leClause->text() + "','" + ui->cbNLA->currentText() + "',#" +ui->dteSubmissionDate->date().toString("MM/dd/yyyy") + "#,#";
			strValues+=ui->dteCommitteeDate->dateTime().toString("MM/dd/yyyy hh:mm:ss") + "#,'" + ui->cbResult->currentText() +"','";
			strValues+=ui->leClauses->text()+"',";
			if(ui->cbOrder->isChecked()) {
				strValues+="1,";
				if(ui->cbOrderExecuted->isChecked()) {
					strValues+="1,'";
					strValues+=ui->leExecInfo->text()+"',";
				}
				else strValues+="0,'',";
			}
			else strValues+="0,0,'',";
			if(ui->cbProlong->isChecked()) strValues+="1,";
			else strValues+="0,";
			strValues+="#"+ui->deExcitationDate->date().toString("MM/dd/yyyy") + "#";
			
			dboper->dbQuery(PRIVATE_QUERY,"DB05",1,strFields,strValues,"amzcases_arc",false,"");
			dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","amzcases_arc",true,"[casenum]='"+ui->leCaseNum->text()+"'");
			dboper->query.next();
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedto]$[attachedtodocid]";
			strValues="'"+tr("Cases archive")+"'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"incoming",true,"[attachedto]='"+tr("Case")+"' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedtodoctype]$[attachedtodocid]";
			strValues="'acs_a'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,"[attachedtodoctype]='acs' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","amzcases",true,"[casenum]='"+ui->leCaseNum->text()+"'");
			dboper->mdlDB05->select();
			iRowCount=dboper->mdlDB05->rowCount();
			iCurrentRow=0;
			emit setStatusText(iCurrentRow, iRowCount);
			SetCasesFormFields(iCurrentRow);
		}
	}
}

void wgtAMZCases::on_cbNLA_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::on_cbOrder_stateChanged(int arg1)
{
	if(arg1==0) {
		ui->cbOrderExecuted->setChecked(false);
		ui->lblExecInfo->setVisible(false);
		ui->leExecInfo->setVisible(false);
	}
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::on_cbOrderExecuted_stateChanged(int arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::on_leExecInfo_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::on_leClauses_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}
void wgtAMZCases::on_cbProlong_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::on_deTill_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::on_teSummary_textChanged()
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtAMZCases::on_deExcitationDate_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}
