#include "wgtallinboxdocs.h"
#include "datedelegate.h"
#include "ui_wgtallinboxdocs.h"

wgtallinboxdocs::wgtallinboxdocs(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::wgtallinboxdocs();
	ui->setupUi(this);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	wgtChangeTypeUser=new wgtTwoFieldsWCombo(0);
	wgtChangeTypeUser->setVisible(false);
	connect(this,SIGNAL(initTwoFields(int,int,QString,QString,QString,QStringList)),wgtChangeTypeUser,SLOT(initTwoFields(int,int,QString,QString,QString,QStringList)));
	connect(wgtChangeTypeUser,SIGNAL(ChangeValue(int,int,QString)),this,SLOT(ChangeValue(int,int,QString)));

	//ui->pbPrint->setVisible(false);
}

wgtallinboxdocs::~wgtallinboxdocs()
{
	delete ui;
}

void wgtallinboxdocs::ConnectModelToView() {
	dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
    dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
    dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("Citizen Appeal"));
	dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Entry Date"));
	dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Send Way"));
    dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Outcome N"));
	dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Outcome Date"));
	dboper->mdlDB05->setHeaderData(8, Qt::Horizontal, tr("Addressee Name"));
	dboper->mdlDB05->setHeaderData(9, Qt::Horizontal, tr("Destination Address"));
	dboper->mdlDB05->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->mdlDB05->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	dboper->mdlDB05->setHeaderData(13, Qt::Horizontal, tr("Executor"));
	dboper->mdlDB05->setHeaderData(14, Qt::Horizontal, tr("Handoverdate"));
	dboper->mdlDB05->setHeaderData(18, Qt::Horizontal, tr("Handovered to department"));
	
	ui->tvIncomingDocs->setModel(dboper->mdlDB05);
	
	ui->tvIncomingDocs->hideColumn(0);
	ui->tvIncomingDocs->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvIncomingDocs->hideColumn(3);
	ui->tvIncomingDocs->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvIncomingDocs->setItemDelegateForColumn(7, new DataDelegate());
	ui->tvIncomingDocs->hideColumn(12);
	ui->tvIncomingDocs->setItemDelegateForColumn(14, new DataDelegate());
	ui->tvIncomingDocs->hideColumn(15);
	ui->tvIncomingDocs->hideColumn(16);
	ui->tvIncomingDocs->hideColumn(17);

	ui->tvIncomingDocs->resizeColumnsToContents();
	ui->tvIncomingDocs->horizontalHeader()->setStretchLastSection(true);
	ui->tvIncomingDocs->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	ui->tvIncomingDocs->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	ui->tvIncomingDocs->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	
}

void wgtallinboxdocs::on_leSearch_textChanged(const QString &arg1)
{
    QString strFilterString="";
	if(!dboper->UsrSettings.stkUserPermissions.bAllIncoming) strFilterString="(";
    strFilterString+="[regnum] LIKE '%"+arg1+"%' OR [outcomenum] LIKE '%"+arg1+"%' OR [addressee] LIKE '%"+arg1+"%' OR [summary] LIKE '%"+arg1+"%' OR [executor] LIKE'%"+arg1+"%'";
	if(!dboper->UsrSettings.stkUserPermissions.bAllIncoming) strFilterString+=") AND [executor] = '" + dboper->UsrSettings.sUserName + "'";

    dboper->mdlDB05->setFilter(QString(strFilterString));
    dboper->mdlDB05->select();
}

void wgtallinboxdocs::initEditActions() {
	actChangeExecutor=new QAction(tr("Change executor"),this);
    actChangeType=new QAction(tr("Change type"),this);
	actDeleteRow=new QAction(tr("Delete row"),this);

    connect(actChangeExecutor, SIGNAL(triggered()), this, SLOT(ChangeExecutorAction()));
    connect(actChangeType, SIGNAL(triggered()), this, SLOT(ChangeTypeAction()));
	connect(actDeleteRow, SIGNAL(triggered()), this, SLOT(DeleteRowAction()));

	ui->tvIncomingDocs->addAction(actChangeExecutor);
	ui->tvIncomingDocs->addAction(actChangeType);
	ui->tvIncomingDocs->addAction(actDeleteRow);
	ui->tvIncomingDocs->setContextMenuPolicy(Qt::ActionsContextMenu);
}

void wgtallinboxdocs::ChangeExecutorAction() {
	dboper->stmdlEOincomingDocs->setTable("executors");
	dboper->stmdlEOincomingDocs->select();
	QStringList stlExecutors;
	for(int i=0;i<dboper->stmdlEOincomingDocs->rowCount();i++) stlExecutors.append(dboper->stmdlEOincomingDocs->record(i).value("executor").toString());
	emit initTwoFields(0,ui->tvIncomingDocs->currentIndex().row(),tr("Current executor:"),dboper->mdlDB05->record(ui->tvIncomingDocs->currentIndex().row()).value("executor").toString(),tr("New executor:"), stlExecutors);
	wgtChangeTypeUser->setVisible(true);
}

void wgtallinboxdocs::ChangeTypeAction() {
	dboper->stmdlEOincomingDocs->setTable("seddoctype");
	dboper->stmdlEOincomingDocs->select();
	QStringList stlSedDocType;
	for(int i=0;i<dboper->stmdlEOincomingDocs->rowCount();i++) stlSedDocType.append(dboper->stmdlEOincomingDocs->record(i).value("seddoctype").toString());
	emit initTwoFields(1,ui->tvIncomingDocs->currentIndex().row(),tr("Current type:"),dboper->mdlDB05->record(ui->tvIncomingDocs->currentIndex().row()).value("seddoctype").toString(),tr("New type:"), stlSedDocType);
	wgtChangeTypeUser->setVisible(true);
}

void wgtallinboxdocs::DeleteRowAction() {
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setWindowTitle(tr("Delete row"));
	msgQuestion.setText(tr("You are going to delete row. That operation might cause changes into other records. This is irreversible operation! Do you want to continue?"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);
		
	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#FEFEFE;border:1px solid #e2e2e2;color:#000000;} QPushButton:hover {border:1px solid #7a6e6e;}");
	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->lblTitle->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

	if(msgQuestion.exec()==QMessageBox::Yes) {
		QString strRegNum=dboper->mdlDB05->record(ui->tvIncomingDocs->currentIndex().row()).value("regnum").toString();
		QString strFilter;
		strFilter="[regnum]='"+strRegNum+"'";


		QString strId=dboper->mdlDB05->record(ui->tvIncomingDocs->currentIndex().row()).value("id").toString();
		QString strId2=dboper->mdlDB05->record(ui->tvIncomingDocs->currentIndex().row()).value("attachedtodocid").toString();
		if(dboper->mdlDB05->record(ui->tvIncomingDocs->currentIndex().row()).value("attachedtodocid").toInt()>0) {
			strFilter="[id]="+strId2;
			emit ExecQuery(PRIVATE_QUERY,"DB05",3,"","","amzclaims",true,strFilter);
			strFilter="[attachedtodoctype]='acl' AND [attachedtodocid]="+strId2;
			dboper->stmdlEOoutcomingDocs->setTable("documents");
			dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
			dboper->stmdlEOoutcomingDocs->select();

			QString strSelectString;
			QString strDestPath;
			QString strEditable;
			QString strScan;
			QFile fileAtt;
			bool bFilesDeleted=false;

			for(int i=0;i<dboper->stmdlEOoutcomingDocs->rowCount();i++) {
				strEditable=dboper->stmdlEOoutcomingDocs->record(i).value("textfile").toString();
				strScan=dboper->stmdlEOoutcomingDocs->record(i).value("scanfile").toString();
				strDestPath=dboper->EnvSettings.strCommonFolder+"/";
				if(strEditable!="") {
					bFilesDeleted=false;
					fileAtt.setFileName(strDestPath+strEditable);
					if(fileAtt.remove()) bFilesDeleted=true;
				}

				if(strScan!="") {
					bFilesDeleted=false;
					fileAtt.setFileName(strDestPath+strScan);
					if(fileAtt.remove()) bFilesDeleted=true;
				}
	
				if(bFilesDeleted) {
					strSelectString="[id]="+dboper->stmdlEOoutcomingDocs->record(i).value("id").toString();
					dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,strSelectString);
					close();
				}
				else QMessageBox::warning(0, tr("Error"), tr("Unable to delete attached file"));
			}

			emit ExecQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,strFilter);
		}
		emit ExecQuery(PRIVATE_QUERY,"DB05",3,"","","incoming",true,"[id]="+strId);
		
/*		
		if(dboper->mdlDB05->record(ui->tvIncomingDocs->currentIndex().row()).value("seddoctype").toString()==tr("Claim")) {
			dboper->stmdlEOincomingDocs->setTable("amzclaims");

			dboper->stmdlEOincomingDocs->setFilter(strFilter);
			dboper->stmdlEOincomingDocs->select();
			QString strId=dboper->stmdlEOincomingDocs->record(0).value("id").toString();
			QString strFilter2;
			strFilter2="[attachedtodocid]="+strId;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,"[attachedto]$[attachedtodocid]","''$'0'","incoming",true,strFilter2);
			
			dboper->stmdlEOoutcomingDocs->setTable("documents");
			dboper->stmdlEOoutcomingDocs->setFilter(strFilter2);
			dboper->stmdlEOoutcomingDocs->select();

			QString strSelectString;
			QString strDestPath;
			QString strEditable;
			QString strScan;
			QFile fileAtt;
			bool bFilesDeleted=false;

			for(int i=0;i<dboper->stmdlEOoutcomingDocs->rowCount();i++) {
				strEditable=dboper->stmdlEOoutcomingDocs->record(i).value("textfile").toString();
				strScan=dboper->stmdlEOoutcomingDocs->record(i).value("scanfile").toString();
				strDestPath=dboper->EnvSettings.strCommonFolder+"/";
				if(strEditable!="") {
					bFilesDeleted=false;
					fileAtt.setFileName(strDestPath+strEditable);
					if(fileAtt.remove()) bFilesDeleted=true;
				}

				if(strScan!="") {
					bFilesDeleted=false;
					fileAtt.setFileName(strDestPath+strScan);
					if(fileAtt.remove()) bFilesDeleted=true;
				}
	
				if(bFilesDeleted) {
					strSelectString="[id]="+dboper->stmdlEOoutcomingDocs->record(i).value("id").toString();
					dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,strSelectString);
					close();
				}
				else QMessageBox::warning(0, tr("Error"), tr("Unable to delete attached file"));
			}

			dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","amzclaims",true,strFilter);
			dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","incoming",true,strFilter);
		}
		else dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","incoming",true,strFilter);*/
	}
	
	dboper->mdlDB05->select();
}

void wgtallinboxdocs::ChangeValue(int iMode, int iRow, QString strNewValue) {
	QString strId;
	QString strId2;
	QString strCaseNumFilter;
	QString strFilter;
	QString strRegNum=dboper->mdlDB05->record(iRow).value("regnum").toString();
	strId=dboper->mdlDB05->record(iRow).value("id").toString();
	if(iMode==0) {
		if(dboper->mdlDB05->record(iRow).value("attachedtodocid").toInt()>0) {
			strFilter="[id]="+dboper->mdlDB05->record(iRow).value("attachedtodocid").toString();
			emit ExecQuery(PRIVATE_QUERY,"DB05",2,"executor","'"+strNewValue+"'","amzclaims",true,strFilter);
			dboper->stmdlEOincomingDocs->setTable("amzclaims");
			dboper->stmdlEOincomingDocs->setFilter(strFilter);
			dboper->stmdlEOincomingDocs->select();
			if(dboper->stmdlEOincomingDocs->record(0).value("casenum").toString()!="") {
				strCaseNumFilter="[casenum]='"+dboper->stmdlEOincomingDocs->record(0).value("casenum").toString()+"'";
				emit ExecQuery(PRIVATE_QUERY,"DB05",2,"executor","'"+strNewValue+"'","amzcases",true,strCaseNumFilter);
			}
		}
		emit ExecQuery(PRIVATE_QUERY,"DB05",2,"executor","'"+strNewValue+"'","incoming",true,"[id]="+strId);
	}
	else if(iMode==1) {
		strId2=dboper->mdlDB05->record(iRow).value("attachedtodocid").toString();

		if(dboper->mdlDB05->record(iRow).value("seddoctype").toString()==tr("Claim")) {
			strFilter="[id]="+strId2;
			emit dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","amzclaims",true,strFilter);
				
			if(strId2!="") {
				strFilter="[attachedtodoctype]='acl' AND [attachedtodocid]="+strId2;
				dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,strFilter);
			}
		}
		strFilter="[id]="+strId;
		dboper->dbQuery(PRIVATE_QUERY,"DB05",2,"[seddoctype]$[attachtoexisteddoc]$[attachedto]$[attachedtodocid]","'"+strNewValue+"'$0$''$0","incoming",true,strFilter);
	}
	dboper->mdlDB05->select();
}

void wgtallinboxdocs::on_pbPrint_clicked()
{
	QString strHTML;
	QString strBuffer;
	bool bOdd=false;
	strHTML+="<h3><center>"+tr("All incoming documents")+"</center></h3><br />";
	
	strHTML+="<table style='border-spacing:0;border-collapse:collapse;width:100%' cellpadding=0 cellspacing=0><tbody>"; //������ cellpadding/cellspacing ����� ����� ��������� ��������� ��������������� ������ css
	
	for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
		strHTML+="<tr><th></th>";

		if(bOdd) {
			strBuffer="background-color:#f7f7f7;";
			bOdd=false;
		}
		else {
			strBuffer="";
			bOdd=true;
		}

		strHTML+="<td style='padding: 5px;"+strBuffer+"' />"+dboper->mdlDB05->record(i).value("regnum").toString()+"</td>";
		strHTML+="<td style='padding: 5px;"+strBuffer+"'>"+dboper->mdlDB05->record(i).value("regdate").toDate().toString("dd.MM.yyyy")+"</td>";
		strHTML+="<td style='padding: 5px;"+strBuffer+"' />"+dboper->mdlDB05->record(i).value("addessee").toString()+"</td>";
		strHTML+="<td style='padding: 5px;"+strBuffer+"'>"+dboper->mdlDB05->record(i).value("address").toString()+"</td>";
		strHTML+="<td style='padding: 5px;"+strBuffer+"' />"+dboper->mdlDB05->record(i).value("doctype").toString()+"</td>";
		strHTML+="<td style='padding: 5px;"+strBuffer+"'>"+dboper->mdlDB05->record(i).value("summary").toString()+"</td>";
		strHTML+="<td style='padding: 5px;"+strBuffer+"' />"+dboper->mdlDB05->record(i).value("executor").toString()+"</td>";
		strHTML+="<td style='padding: 5px;"+strBuffer+"'>"+dboper->mdlDB05->record(i).value("hadoverdate").toDate().toString("dd.MM.yyyy")+"</td>";
		strHTML+="</tr>";
	}

	strHTML+="</tbody></table>";
	emit PrintHTML(strHTML);
}
