#include <qcryptographichash.h>
#include "wgtauthorize.h"

wgtAuthorize::wgtAuthorize(QWidget *parent)
	: QWidget(parent),
    ui(new Ui::wgtAuthorize)
{
	wMW = new mw();
	wMW->setVisible(false);
	ui->setupUi(this);
	setWindowFlags(Qt::Dialog);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	connect(this,SIGNAL(initMainForm()),wMW,SLOT(initMainForm()));
	ui->lblWrongPassword->setVisible(false);
	bNotCloseButton=false;
	setMaximumSize(sizeHint());

	QFile file(qApp->applicationDirPath() + "/styles/default/auth.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtAuthorize::~wgtAuthorize()
{
	delete ui;
}

void wgtAuthorize::on_pbOK_clicked()
{
	if((ui->lePassword->text()==ui->leReenterPassword->text()) || (ui->lePassword->isModified() && ui->leReenterPassword->isVisible()==false)) {
		QCryptographicHash hash(QCryptographicHash::Md5);
		QByteArray baQuery;
		baQuery.insert(0,ui->lePassword->text());
		hash.addData(baQuery);
		QString hashStr(hash.result().toHex());
		QString strFilter="[Username]='";
		strFilter+=ui->cbUsername->currentText();
		strFilter+="'";
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","users",true,strFilter);
		dboper->query.next();
		if(ui->leReenterPassword->isVisible()==true || dboper->query.value("passwordhash").toString()==hashStr) {
			//first authorize
			dboper->UsrSettings.iUid=dboper->query.value("uid").toInt();
			dboper->UsrSettings.sUserName=dboper->query.value("username").toString();
			dboper->UsrSettings.iUserPermissions=dboper->query.value("permissions").toInt();

			dboper->UsrSettings.stkUserPermissions.bEighteenone=false;
			dboper->UsrSettings.stkUserPermissions.bClaims=false;
			dboper->UsrSettings.stkUserPermissions.bCases=false;
			dboper->UsrSettings.stkUserPermissions.bAdmCases=false;
			dboper->UsrSettings.stkUserPermissions.bLawSuites=false;
			dboper->UsrSettings.stkUserPermissions.bFASOrders=false;
			dboper->UsrSettings.stkUserPermissions.bArchiveEighteenOne=false;
			dboper->UsrSettings.stkUserPermissions.bArchiveClaims=false;
			dboper->UsrSettings.stkUserPermissions.bArchiveCases=false;
			dboper->UsrSettings.stkUserPermissions.bArchiveAdmCases=false;
			dboper->UsrSettings.stkUserPermissions.bArchiveLawSuites=false;
			dboper->UsrSettings.stkUserPermissions.bArchiveFASOrders=false;
			dboper->UsrSettings.stkUserPermissions.bAllIncoming=false;
			dboper->UsrSettings.stkUserPermissions.bSelfReport=false;
			dboper->UsrSettings.stkUserPermissions.bFullReport=false;
			dboper->UsrSettings.stkUserPermissions.bHandoverIncoming=false;
			dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming=false;
			dboper->UsrSettings.stkUserPermissions.bUserManagement=false;

			double iNum=dboper->UsrSettings.iUserPermissions;
			double iTemp=iNum;
			double iCount=0.0;
			double rTemp=0.0;
			while (iNum>0) {
			while (iTemp>2) {
				iTemp=iTemp/2;
				iCount++;
			}
			if(iTemp==2) iCount++;
			rTemp=pow(2.0,iCount);

			iNum=iNum-rTemp;
			switch(QVariant(rTemp).toInt()) {
				case 1:
					dboper->UsrSettings.stkUserPermissions.bEighteenone=true;
					break;
				case 2:
					dboper->UsrSettings.stkUserPermissions.bClaims=true;
					break;
				case 4:
					dboper->UsrSettings.stkUserPermissions.bCases=true;
					break;
				case 8:
					dboper->UsrSettings.stkUserPermissions.bAdmCases=true;
					break;
				case 16:
					dboper->UsrSettings.stkUserPermissions.bLawSuites=true;
					break;
				case 32:
					dboper->UsrSettings.stkUserPermissions.bFASOrders=true;
					break;
				case 64:
					dboper->UsrSettings.stkUserPermissions.bArchiveEighteenOne=true;
					break;
				case 128:
					dboper->UsrSettings.stkUserPermissions.bArchiveClaims=true;
					break;
				case 256:
					dboper->UsrSettings.stkUserPermissions.bArchiveCases=true;
					break;
				case 512:
					dboper->UsrSettings.stkUserPermissions.bArchiveAdmCases=true;
					break;
				case 1024:
					dboper->UsrSettings.stkUserPermissions.bArchiveLawSuites=true;
					break;
				case 2048:
					dboper->UsrSettings.stkUserPermissions.bArchiveFASOrders=true;
					break;
				case 4096:
					dboper->UsrSettings.stkUserPermissions.bAllIncoming=true;
					break;
				case 8192:
					dboper->UsrSettings.stkUserPermissions.bSelfReport=true;
					break;
				case 16384:
					dboper->UsrSettings.stkUserPermissions.bFullReport=true;
					break;
				case 32768:
					dboper->UsrSettings.stkUserPermissions.bHandoverIncoming=true;
					break;
				case 65536:
					dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming=true;
					break;
				case 131072:
					dboper->UsrSettings.stkUserPermissions.bUserManagement=true;
					break;
				}
				//permissions sets by addition permissions group. for example permission for managing users and changing server settings is 34 (16+8).
				iTemp=iNum;
				iCount=0;
			}
			
			dboper->query.finish();
			if(ui->leReenterPassword->isVisible()==true) dboper->dbQuery(PRIVATE_QUERY,"DB05",2,"passwordhash","'"+hashStr+"'","users",true,strFilter);
			dboper->dbQuery(PRIVATE_QUERY,"dbsettings",2,"lastuser","'"+dboper->UsrSettings.sUserName+"'","main",false,"");
			bNotCloseButton=true;
			close();
			wMW->dboper=dboper;
			emit initMainForm();
		}
		else {
			ui->lePassword->clear();
			ui->lePassword->setStyleSheet("border-color:#DD4B39;");
			ui->lblWrongPassword->setVisible(true);
			ui->lePassword->setFocus();
			//WRONG PASSWORD OR PASSWORDS MISMATCH
		}
	}
}

void wgtAuthorize::on_lePassword_returnPressed()
{
	if(ui->leReenterPassword->isVisible()==true) ui->leReenterPassword->setFocus();
	else ui->pbOK->click();
}

void wgtAuthorize::initAuthForm(bool bIsLastUser, QString sUsername) {
	ui->cbUsername->clear();
	ui->cbUsername->addItems(stlUsers);
	if(bIsLastUser) {
		ui->cbUsername->setCurrentIndex(ui->cbUsername->findText(sUsername,Qt::MatchExactly));
		on_cbUsername_currentTextChanged(sUsername);
		ui->lePassword->setFocus();
	}
	else {
		ui->cbUsername->setCurrentIndex(0);
		on_cbUsername_currentTextChanged(ui->cbUsername->currentText());
		ui->cbUsername->setFocus();
	}
}

void wgtAuthorize::on_cbUsername_currentTextChanged(const QString &arg1)
{
	QString strBuffer="[Username]='";
	strBuffer+=arg1;
	strBuffer+="'";
	dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","users",true,strBuffer);
	dboper->query.next();
	if(dboper->query.value("passwordhash").toString()=="") {
		ui->lblReenterPassword->setVisible(true);
		ui->leReenterPassword->setVisible(true);
	}
	else {
		ui->lblReenterPassword->setVisible(false);
		ui->leReenterPassword->setVisible(false);
	}
	dboper->query.finish();
	adjustSize();
}

void wgtAuthorize::closeEvent(QCloseEvent *event)
{
	if(!bNotCloseButton) {
		dboper->query=QSqlQuery();
		dboper->TimerQuery=QSqlQuery();
		dboper->TimerMessagesQuery=QSqlQuery();
		dboper->TimerLocalQuery=QSqlQuery();
		dboper->mdlSettings->clear();

		dboper->dbCloseConn("UPDATEDB");
		dboper->dbCloseConn("DB05");
		dboper->dbCloseConn("officeDB");
		dboper->dbCloseConn("dbsettings");
	}
	event->accept();
}