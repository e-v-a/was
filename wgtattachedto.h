#ifndef WGTATTACHEDTO_H
#define WGTATTACHEDTO_H

#include <QWidget>
#include "udatabase.h"

namespace Ui {class wgtattachedto;};

class wgtattachedto : public QWidget
{
	Q_OBJECT

public:
	wgtattachedto(QWidget *parent = 0);
	~wgtattachedto();
	DBOperations *dboper;

public slots:
	void LoadTable(QString table,QString executor);

private slots:
    void on_pbOK_clicked();
	void on_tvItems_clicked(const QModelIndex &index);

signals:
	void SetAttachment(int iId);

private:
	Ui::wgtattachedto *ui;
};

#endif // WGTATTACHEDTO_H
