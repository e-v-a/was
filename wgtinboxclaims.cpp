#include <qmessagebox.h>
#include "wgtinboxclaims.h"
#include "ui_wgtinboxclaims.h"

wgtInboxClaims::wgtInboxClaims(QWidget *parent)
	: QWidget(parent),
    ui(new Ui::wgtInboxClaims)
{
	ui->setupUi(this);
	iCurrentRow=-1;
	wAttach=new wgtattachedto(this);
	wAttach->setVisible(false);
	connect(this,SIGNAL(LoadTable(QString,QString)),wAttach,SLOT(LoadTable(QString,QString)));
	connect(wAttach,SIGNAL(SetAttachment(int)),this,SLOT(SetAttachment(int)));
	ui->cbAttach->setEnabled(false);
	iIdR=-1;
	bIsSearch=false;
	ui->lblNoMatches->setVisible(false);
	actSearch=new QAction(tr("Search"),this);
	actSearch->setCheckable(true);
	QList<QKeySequence> scList;
	scList.append(QKeySequence(Qt::CTRL + Qt::Key_F));
	scList.append(QKeySequence(Qt::Key_F3));
	actSearch->setShortcuts(scList);
	connect(actSearch, SIGNAL(toggled(bool)), this, SLOT(on_pbShowSearch_toggled(bool)));
	addAction(actSearch);
	ui->cbTheirDocs->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtInboxClaims::~wgtInboxClaims()
{

}

void wgtInboxClaims::SetInboxFields(int recordnum) {
	QString strFilter;
	ui->leRegnum->clear();
	ui->deRegDate->setDate(QDate(2013,1,1));
	ui->leOutcomeNum->clear();
	ui->deOutcomeDate->setDate(QDate(2013,1,1));
	ui->leAddressee->clear();
	ui->leAddress->clear();
	ui->leSendWay->clear();
	ui->deHandoverDate->setDate(QDate(2013,1,1));
	ui->cbExecutor->clear();
	ui->cbDocType->clear();
	ui->teSummary->clear();
	ui->cbAttachedToType->clear();
	ui->leHandoverDep->clear();
	strAttachmentID="";
	ui->leHandoverDep->setVisible(false);
	ui->lblHandoverDep->setVisible(false);
	ui->wgtSearch->setVisible(bIsSearch);
	
	if(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming) ui->cbTheirDocs->setVisible(true);
	else ui->cbTheirDocs->setVisible(false);

	iRowIndex=dboper->mdlDB05->record(recordnum).value("id").toInt();
		
	ui->leRegnum->setText(dboper->mdlDB05->record(recordnum).value("regnum").toString());
	ui->deRegDate->setDate(dboper->mdlDB05->record(recordnum).value("regdate").toDate());
	ui->leOutcomeNum->setText(dboper->mdlDB05->record(recordnum).value("outcomenum").toString());
	ui->deOutcomeDate->setDate(dboper->mdlDB05->record(recordnum).value("outcomedate").toDate());
	ui->leAddressee->setText(dboper->mdlDB05->record(recordnum).value("addressee").toString());
	ui->leAddress->setText(dboper->mdlDB05->record(recordnum).value("destination").toString());
	ui->leSendWay->setText(dboper->mdlDB05->record(recordnum).value("sendway").toString());
	ui->leDocType->setText(dboper->mdlDB05->record(recordnum).value("doctype").toString());
		
	if(ui->cbDocType->count()==0) ui->cbDocType->addItems(stlDoctype);
	ui->cbDocType->setCurrentIndex(ui->cbDocType->findText(dboper->mdlDB05->record(recordnum).value("seddoctype").toString(),Qt::MatchExactly));

	ui->cbCitizenAppeal->setChecked(dboper->mdlDB05->record(recordnum).value("citizenappeal").toBool());
	ui->teSummary->setText(dboper->mdlDB05->record(recordnum).value("summary").toString());

	if(ui->cbExecutor->count()==0) ui->cbExecutor->addItems(stlExecutor);
	ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->mdlDB05->record(recordnum).value("executor").toString(),Qt::MatchExactly));

	ui->deHandoverDate->setDate(dboper->mdlDB05->record(recordnum).value("handoverdate").toDate());
	ui->cbAttach->setChecked(dboper->mdlDB05->record(recordnum).value("attachtoexisteddoc").toBool());
	
	if(ui->cbAttachedToType->count()==0) ui->cbAttachedToType->addItems(stlAttachedtotype);
	ui->cbAttachedToType->setCurrentIndex(ui->cbAttachedToType->findText(dboper->mdlDB05->record(recordnum).value("attachedto").toString(),Qt::MatchExactly));

	iIdR=dboper->mdlDB05->record(recordnum).value("attachedtodocid").toInt();
	strAttachmentID.setNum(iIdR);
	if(iIdR>0) ui->pbAddAttachment->setText(tr("Change attachment"));
	else ui->pbAddAttachment->setText(tr("Add attachment"));

	if(ui->cbDocType->currentText()==tr("Wrong department")) {
		ui->leHandoverDep->setVisible(true);
		ui->lblHandoverDep->setVisible(true);
	}

	ui->pbFirst->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbPrev->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbNext->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	ui->pbLast->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	
	ui->pbSave->setEnabled(false);
	ui->lblAttachedTo->setVisible(false);
	ui->cbAttachedToType->setVisible(false);
	ui->pbAddAttachment->setVisible(false);
	ui->pbDeleteAttachment->setVisible(false);
	ui->pbDeleteAttachment->setEnabled(false);

	if(!(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming || dboper->UsrSettings.stkUserPermissions.bHandoverIncoming)) {
		ui->cbExecutor->setEnabled(false);
		ui->cbDocType->setEnabled(false);
		ui->deHandoverDate->setEnabled(false);
		ui->cbAttach->setEnabled(true);
	}
	else {
		ui->cbExecutor->setEnabled(true);
		ui->cbDocType->setEnabled(true);
		ui->deHandoverDate->setEnabled(true);
		if(!ui->cbTheirDocs->isChecked())
			ui->cbAttach->setEnabled(false);
	}

	if(!bIsSearch) emit UpdateClaimsNum(iRowCount);
}

void wgtInboxClaims::on_pbFirst_clicked()
{
	if(ui->pbSave->isEnabled()) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) 
			ui->pbSave->click();
	}

	iCurrentRow=0;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetInboxFields(iCurrentRow);
}

void wgtInboxClaims::on_pbPrev_clicked()
{
	if(ui->pbSave->isEnabled()) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
	}

	iCurrentRow--;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetInboxFields(iCurrentRow);
}

void wgtInboxClaims::on_pbSave_clicked()
{
	QString strValues;
	/*strValues=tr("A new document %1 %2 is reffered to you").arg(tr("register number"),ui->leRegnum->text()+tr(" from ")+ui->deRegDate->text());
	emit SendInternalMessage(QDateTime::currentDateTime(), ui->cbExecutor->currentText(), tr("New document reffered"), strValues,
		ui->cbDocType->currentText(), iRowIndex);*/

	QString strAttachedTo=ui->cbAttachedToType->currentText();

	if(ui->cbDocType->currentText()==tr("Eighteen one claim") && !ui->cbAttach->isChecked()) {
		strValues="'";
		strValues+=ui->leRegnum->text(); //regnum
		strValues+="',#";
		strValues+=ui->deRegDate->date().toString("yyyy-MM-dd"); //regdate
		strValues+="#,#";
		strValues+=ui->deHandoverDate->date().toString("yyyy-MM-dd"); //handoverdate
		strValues+="#,'";
		strValues+=ui->cbExecutor->currentText(); //executor
		strValues+="','";
		strValues+=ui->leAddressee->text(); // complainant;
		strValues+="'";
				
		dboper->dbQuery(PRIVATE_QUERY,"DB05",1,"[regnum],[regdate],[handoverdate],[executor],[complainant]", strValues,"eighteenone",false,"");
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","eighteenone",true,"[regnum]="+ui->leRegnum->text());
		dboper->query.next();
		iIdR=dboper->query.value("id").toInt();
		dboper->query.finish();
		strAttachedTo=tr("Eighteenone claim");
	}
	if(ui->cbDocType->currentText()==tr("Claim") && !ui->cbAttach->isChecked()) {
		strValues="'";
		strValues+=ui->leRegnum->text(); //regnum
		strValues+="',#";
		strValues+=ui->deRegDate->date().toString("yyyy-MM-dd"); //regdate
		strValues+="#,#";
		strValues+=ui->deHandoverDate->date().toString("yyyy-MM-dd"); //handoverdate
		strValues+="#,'";
		strValues+=ui->cbExecutor->currentText(); //executor
		strValues+="','";
		strValues+=ui->leAddressee->text(); // complainant;
		strValues+="'";
		
		dboper->dbQuery(PRIVATE_QUERY,"DB05",1,"[regnum],[regdate],[handoverdate],[executor],[complainant]", strValues,"amzclaims",false,"");
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","amzclaims",true,"[regnum]="+ui->leRegnum->text());
		dboper->query.next();
		iIdR=dboper->query.value("id").toInt();
		dboper->query.finish();
		strAttachedTo=tr("Claim");
	}

	QStringList stlFields;
	QList<QVariant> stlValues;

	stlFields.append("12");//seddoctype
	stlValues.append(ui->cbDocType->currentText());
	if(ui->cbExecutor->currentText()!="") {
		stlFields.append("13");//executor
		stlValues.append(ui->cbExecutor->currentText());
	}
	stlFields.append("14");//handoverdate
	stlValues.append(ui->deHandoverDate->date());
	if(iIdR>0) {
		stlFields.append("15");//attachtoexisteddoc
		stlValues.append(true);
		stlFields.append("16");//attachedto
		stlValues.append(strAttachedTo);
		stlFields.append("17");//attachedtodocid
		stlValues.append(iIdR);
	}
	stlFields.append("18");//handoverdep
	stlValues.append(ui->leHandoverDep->text());

	ui->pbSave->setEnabled(!dboper->dbWriteData(false,true,iCurrentRow,stlFields,stlValues));

	strValues.setNum(iRowIndex);

	//if(doctype==tr("Answer to request") && !isattached)
	//if(doctype==tr("Lawsuit documents") && !isattached)
	//if(doctype==tr("FAS, prosecutor's office's letter") && !isattached)
	//if(doctype==tr("Letters of other agencies") && !isattached)

	if(iRowCount!=1 && iCurrentRow!=iRowCount-1) iNextRow=iCurrentRow;
	else if(iCurrentRow==iRowCount-1 && iRowCount!=1) iNextRow=iCurrentRow-1;
	else if(iRowCount==1) iNextRow=-1;
	dboper->mdlDB05->select();
	iRowCount=dboper->mdlDB05->rowCount();
	if(iRowCount>=1) iNextRow=0; 
	else emit closeIncoming();	//if recordcount>=1 then set firstinboxfield otherwise close widget
	
	if(iNextRow!=-1) {
		emit setStatusText(iNextRow+1, iRowCount);
		emit UpdateClaimsNum(iRowCount);
		SetInboxFields(iNextRow);
	}
}

void wgtInboxClaims::on_pbNext_clicked()
{
	if(ui->pbSave->isEnabled()) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
	}
	
	iCurrentRow++;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetInboxFields(iCurrentRow);
}

void wgtInboxClaims::on_pbLast_clicked()
{
	if(ui->pbSave->isEnabled()) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
	}

	iCurrentRow=iRowCount-1;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetInboxFields(iCurrentRow);
}

void wgtInboxClaims::on_pbDeleteAttachment_clicked()
{
	iIdR=-1;
	ui->cbAttachedToType->setCurrentText("");
	ui->pbAddAttachment->setText(tr("Add attachment"));
}

void wgtInboxClaims::on_cbExecutor_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	if(ui->cbExecutor->currentText()!="") ui->deHandoverDate->setDate(QDate::currentDate());
	else ui->deHandoverDate->setDate(QDate(2013,1,1));
}

void wgtInboxClaims::on_cbDocType_currentIndexChanged(const QString &arg1)
{
	if(ui->cbDocType->currentText()==tr("Wrong department")) {
		ui->leHandoverDep->setVisible(true);
		ui->lblHandoverDep->setVisible(true);
	}
	ui->pbSave->setEnabled(true);
}

void wgtInboxClaims::on_deHandoverDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
}

void wgtInboxClaims::on_cbAttach_stateChanged(int arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtInboxClaims::on_cbAttachedToType_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtInboxClaims::on_leAttachedToLink_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtInboxClaims::on_cbAttachedToType_currentTextChanged(const QString &arg1)
{
	if(arg1==tr("Eighteen one claim")) ui->pbAddAttachment->setEnabled(true);
	else if(arg1==tr("Claim")) ui->pbAddAttachment->setEnabled(true);
	else if(arg1==tr("Case")) ui->pbAddAttachment->setEnabled(true);
	else if(arg1==tr("Administrative case")) ui->pbAddAttachment->setEnabled(true);
	else if(arg1==tr("Lawsuite")) ui->pbAddAttachment->setEnabled(true);
	else ui->pbAddAttachment->setEnabled(false);
}

void wgtInboxClaims::on_pbAddAttachment_clicked()
{
	if(ui->cbAttachedToType->currentText()==tr("Eighteen one claim")) emit LoadTable("eighteenone",ui->cbExecutor->currentText());
	if(ui->cbAttachedToType->currentText()==tr("Claim")) emit LoadTable("amzclaims",ui->cbExecutor->currentText());
	if(ui->cbAttachedToType->currentText()==tr("Case")) emit LoadTable("amzcases",ui->cbExecutor->currentText());
	if(ui->cbAttachedToType->currentText()==tr("Administrative case")) emit LoadTable("admcases",ui->cbExecutor->currentText());
	if(ui->cbAttachedToType->currentText()==tr("Lawsuite")) emit LoadTable("lawsuites",ui->cbExecutor->currentText());

	wAttach->setWindowFlags(Qt::Dialog);
	wAttach->setWindowFlags(wAttach->windowFlags() & ~Qt::WindowContextHelpButtonHint);
	wAttach->setWindowModality(Qt::WindowModal);
	wAttach->setVisible(true);
}

void wgtInboxClaims::SetAttachment(int iId) {
	iIdR=iId;
	ui->pbSave->setEnabled(true);
	ui->pbAddAttachment->setText(tr("Change attachment"));
}

void wgtInboxClaims::on_cbExecutor_currentTextChanged(const QString &arg1)
{
	if(!ui->cbTheirDocs->isChecked()) {
		if(arg1!="") ui->cbAttach->setEnabled(true);
		else ui->cbAttach->setEnabled(false);
	}
}

void wgtInboxClaims::on_pbShowSearch_toggled(bool checked)
{
	if(ui->pbSave->isEnabled()) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
	}
	bIsSearch=checked;
	ui->wgtSearch->setVisible(checked);
    ui->leSearch->clear();
    (checked)?ui->leSearch->setFocus():ui->pbClear->click();
	ui->pbShowSearch->setChecked(checked);
	actSearch->setChecked(checked);
	ui->lblNoMatches->setVisible(false);
    ui->pbClear->setVisible(false);
}

void wgtInboxClaims::on_leSearch_returnPressed()
{
    ui->pbSearch->click();
}

void wgtInboxClaims::on_pbSearch_clicked()
{
    if(ui->leSearch->text()!="") {
        bIsSearch=true;
        QString arg1=ui->leSearch->text();
        strSearchString="([regnum] LIKE '%"+arg1+"%' OR [addressee] LIKE '%"+arg1+"%' OR [summary] LIKE '%"+arg1+"%') AND ";

        if(dboper->UsrSettings.iUserPermissions>0) strSearchString+="([executor] is null OR [executor] = '') AND ([seddoctype] is null OR [seddoctype] NOT LIKE '" + tr("Wrong department")+"')";// OR ([executor] = '" + usrCurrentUser->sName + "' AND [attachedtodocid] = 0)";
        else strSearchString+="[executor] = '" + dboper->UsrSettings.sUserName + "' AND [attachedtodocid] = 0";

        strSearchString+=" ORDER BY [regdate] DESC, [regnum] DESC";

        dboper->mdlDB05->setFilter(strSearchString);
        dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount() == 0) ui->lblNoMatches->setVisible(true);
        if(dboper->mdlDB05->rowCount() > 0) {
			ui->lblNoMatches->setVisible(false);
            iRowCount=dboper->mdlDB05->rowCount();
            iCurrentRow=0;
            emit setStatusText(iCurrentRow+1, iRowCount);
            SetInboxFields(iCurrentRow);
        }
    }
}

void wgtInboxClaims::on_pbClear_clicked()
{
    ui->leSearch->clear();
    if(dboper->UsrSettings.iUserPermissions>0) strSearchString="([executor] is null OR [executor] = '') AND ([seddoctype] is null OR [seddoctype] NOT LIKE '" + tr("Wrong department")+"')";// OR ([executor] = '" + usrCurrentUser->sName + "' AND [attachedtodocid] = 0)";
    else strSearchString="[executor] = '" + dboper->UsrSettings.sUserName + "' AND [attachedtodocid] = 0";
	//strSearchString+=" AND [seddoctype]  NOT LIKE '" + tr("Wrong department")+"'";
    strSearchString+=" ORDER BY [regdate] DESC, [regnum] DESC";
    dboper->mdlDB05->setFilter(strSearchString);
    dboper->mdlDB05->select();

    if(dboper->mdlDB05->rowCount() > 0) {
        iRowCount=dboper->mdlDB05->rowCount();
        iCurrentRow=0;
        emit setStatusText(iCurrentRow+1, iRowCount);
        SetInboxFields(iCurrentRow);
    }
	ui->lblNoMatches->setVisible(false);
    //ui->pbClear->setVisible(false);
}

void wgtInboxClaims::on_leSearch_textChanged(const QString &arg1)
{
    if(arg1!="") ui->pbClear->setVisible(true);
	else ui->pbClear->setVisible(false);
}

void wgtInboxClaims::clearInboxSearch() {
	bIsSearch=false;
	ui->leSearch->clear();
	ui->pbClear->setVisible(false);
	ui->pbShowSearch->setChecked(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
	ui->cbTheirDocs->setChecked(false);
}

void wgtInboxClaims::on_cbTheirDocs_toggled(bool checked)
{
	QString strFilter;
	if(checked) {
		strFilter="[executor] = '" + dboper->UsrSettings.sUserName + "' AND [attachedtodocid] = 0";
		ui->cbAttach->setEnabled(true);
	}
	else {
		strFilter="([executor] is null OR [executor] = '') AND ([seddoctype] is null OR [seddoctype] NOT LIKE '" + tr("Wrong department")+"')";
		ui->cbAttach->setEnabled(false);
	}
	strFilter+=" ORDER BY [regdate] DESC, [regnum] DESC";

	dboper->mdlDB05->setEditStrategy(QSqlTableModel::OnRowChange);
	dboper->mdlDB05->setTable("incoming");
	dboper->mdlDB05->setFilter(strFilter);
		
	dboper->mdlDB05->select();

	iRowCount=dboper->mdlDB05->rowCount();

	iCurrentRow=0;

	if(!dboper->mdlDB05->record(0).isEmpty()) {
		SetInboxFields(iCurrentRow);
		setStatusText(iCurrentRow+1, iRowCount);
	}
}

void wgtInboxClaims::setAttachedOperLink() {
	wAttach->dboper=dboper;
}

void wgtInboxClaims::on_leHandoverDep_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}
