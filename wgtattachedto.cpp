#include "wgtattachedto.h"
#include "ui_wgtattachedto.h"

wgtattachedto::wgtattachedto(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::wgtattachedto();
	ui->setupUi(this);
}

wgtattachedto::~wgtattachedto()
{
	delete ui;
}


void wgtattachedto::on_pbOK_clicked()
{
	emit SetAttachment(dboper->stmdlEOoutcomingDocs->record(ui->tvItems->currentIndex().row()).value(0).toInt());
	close();
}

void wgtattachedto::LoadTable(QString table,QString executor) {
	dboper->stmdlEOoutcomingDocs->setTable(table);
	
	QString strBuffer;
	strBuffer="[executor]='";
	strBuffer+=executor;
	strBuffer+="'";
	dboper->stmdlEOoutcomingDocs->setFilter(strBuffer);
	dboper->stmdlEOoutcomingDocs->select();

	ui->tvItems->setModel(dboper->stmdlEOoutcomingDocs);
	
	if(table=="eighteenone") {
		dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Handover date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Executor"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Complainant"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Defendant"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Notification"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(8, Qt::Horizontal, tr("NLA"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(9, Qt::Horizontal, tr("Submission date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(10, Qt::Horizontal, tr("Committee date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(11, Qt::Horizontal, tr("Result"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(12, Qt::Horizontal, tr("Decision num"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(13, Qt::Horizontal, tr("Decision date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(14, Qt::Horizontal, tr("Order"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(15, Qt::Horizontal, tr("Executed"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(16, Qt::Horizontal, tr("Execution info"));
	}
	else if(table=="amzclaims") {
		dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Handover date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Executor"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Complainant"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Defendant"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Clause"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(8, Qt::Horizontal, tr("NLA"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(9, Qt::Horizontal, tr("Request"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(10, Qt::Horizontal, tr("Submission date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(11, Qt::Horizontal, tr("Result"));
		
		dboper->stmdlEOoutcomingDocs->setHeaderData(12, Qt::Horizontal, tr("Case num"));
	}
	else if(table=="amzcases") {
		dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Claim number"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Claim date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Case number"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Decree N"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Decree date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Handover date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Executor"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(8, Qt::Horizontal, tr("Complainant"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(9, Qt::Horizontal, tr("Defendant"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(10, Qt::Horizontal, tr("Decision num"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(11, Qt::Horizontal, tr("Decision date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(12, Qt::Horizontal, tr("Clause"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(13, Qt::Horizontal, tr("NLA"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(14, Qt::Horizontal, tr("Request"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(15, Qt::Horizontal, tr("Submission date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(16, Qt::Horizontal, tr("Committee date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(17, Qt::Horizontal, tr("Result"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(18, Qt::Horizontal, tr("Clauses"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(19, Qt::Horizontal, tr("Order"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(20, Qt::Horizontal, tr("Order executed"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(21, Qt::Horizontal, tr("Execution info"));
	}
	else if(table=="admcases") {
		dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Case number"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Excitation date"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Defendant"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Defendant's address"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Clause"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Clause part"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Date of judgement"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(8, Qt::Horizontal, tr("Decision"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(9, Qt::Horizontal, tr("Amount"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(10, Qt::Horizontal, tr("Executor"));
	}
	else if(table=="lawsuites") {
		dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Case number"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Claim date"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Complainant"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Defendant"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Third persons"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Other persons"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Judge"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(8, Qt::Horizontal, tr("Trial date/time"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(9, Qt::Horizontal, tr("Court address"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(10, Qt::Horizontal, tr("Case card url"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(11, Qt::Horizontal, tr("Result"));

		dboper->stmdlEOoutcomingDocs->setHeaderData(12, Qt::Horizontal, tr("Instance"));
		dboper->stmdlEOoutcomingDocs->setHeaderData(13, Qt::Horizontal, tr("Executor"));
	}
	//ui->tvItems->update();
	ui->pbOK->setEnabled(false);
}
void wgtattachedto::on_tvItems_clicked(const QModelIndex &index)
{
    ui->pbOK->setEnabled(true);
}