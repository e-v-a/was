#ifndef WGTADMINISTRATIVECASES_H
#define WGTADMINISTRATIVECASES_H

#include <QWidget>
#include "wgtaddoutdoc.h"

namespace Ui {
class wgtAdministrativeCases;
}

class wgtAdministrativeCases : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAdministrativeCases(QWidget *parent = 0);
    ~wgtAdministrativeCases();
	QStringList stlDecisionTypes;
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	DBOperations *dboper;
	bool bIsNew;

public slots:
	void SetAdmCasesFormFields(int recordnum);
	void EnableBlankFields();
	void updateACTable();
	void setACDBOperLink();
	void clearADCSearch();

signals:
	void setStatusText(int iCurrentRow, int iRowsCount);
	void LoadAdded(QString strAdmCaseNum);

private slots:
    void on_leSearch_returnPressed();
	void on_pbClear_clicked();
	void on_pbSearch_clicked();
	void on_leCaseNum_textChanged(const QString &arg1);
	void on_deExcitDate_dateChanged(const QDate &date);
	void on_leDefendant_textChanged(const QString &arg1);
	void on_leAddress_textChanged(const QString &arg1);
	void on_leClause_textChanged(const QString &arg1);
	void on_dteJudgementDate_dateTimeChanged(const QDateTime &dateTime);
	void on_cbDecision_currentTextChanged(const QString &arg1);
	void on_leAmount_textChanged(const QString &arg1);
	void on_tvOutcoming_clicked(const QModelIndex &index);
	void on_tvOutcoming_doubleClicked(const QModelIndex &index);
	void on_pbShowSearch_toggled(bool checked);
	void on_pbDelete_clicked();
	void on_pbFirst_clicked();
	void on_pbPrev_clicked();
	void on_pbSave_clicked();
	void on_pbNext_clicked();
	void on_pbLast_clicked();
	void EditAction();
	void DeleteAction();
	void on_pbAddDoc_clicked();
	void on_pbSend2Agreement_clicked();
	void on_pbArc_clicked();
	void on_leSearch_textChanged(const QString &arg1);

    void on_cbProlong_toggled(bool checked);

    void on_deTill_dateChanged(const QDate &date);

private:
    Ui::wgtAdministrativeCases *ui;
	wgtaddoutdoc *wAddOutDoc;
	bool bFormChanged;
	bool bIsSearch;
	QAction *actEdit;
    QAction *actDelete;
	QAction *actSearch;
	int iAdmID;
	void AskSaving();
};

#endif // WGTADMINISTRATIVECASES_H
