#include "wgtarchivehistory.h"
#include "datedelegate.h"
#include "ui_wgtarchivehistory.h"

wgtarchivehistory::wgtarchivehistory(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtarchivehistory)
{
    ui->setupUi(this);
	wAddOutDoc = new wgtaddoutdoc(this);
	wAddOutDoc->setVisible(false);
    wAddOutDoc->setWindowFlags(Qt::Dialog);
    wAddOutDoc->setWindowFlags(wAddOutDoc->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wAddOutDoc->setWindowModality(Qt::WindowModal);
}

wgtarchivehistory::~wgtarchivehistory()
{
    delete ui;
}

void wgtarchivehistory::on_pbBack2Archive_clicked()
{
	setVisible(false);
	emit BackToArchive(iMode); //int iMode
}

void wgtarchivehistory::on_tvIncoming_doubleClicked(const QModelIndex &index)
{

}

void wgtarchivehistory::on_tvDocuments_doubleClicked(const QModelIndex &index)
{
    if(ui->tvDocuments->currentIndex().isValid()) {
		QString strType;
		if(iMode==0) strType="eo_a";
		else if(iMode==1) strType="acl_a";
		else if(iMode==2) strType="acs_a";
		wAddOutDoc->setAODVars(strType, 0,true,dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("id").toInt());
		wAddOutDoc->setVisible(true);
	}
}

void wgtarchivehistory::SetArcHistoryFields(int iRow) {
	QString strInfo;
	if(iMode==0) {
		strInfo="<b>"+tr("Reg N: ")+"</b>"+dboper->mdlDB05->record(iRow).value("regnum").toString()+" ";
		strInfo+="<b>"+tr("Reg date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("regdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Executor: ")+"</b>"+dboper->mdlDB05->record(iRow).value("executor").toString()+" ";
		strInfo+="<b>"+tr("Handover date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("handoverdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Complainant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("complainant").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Defendant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("defendant").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Notification: ")+"</b>"+dboper->mdlDB05->record(iRow).value("notification").toString()+"<br /><br />";
		strInfo+="<b>"+tr("NLA: ")+"</b>"+dboper->mdlDB05->record(iRow).value("npa").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Committee date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("committeedate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Result: ")+"</b>"+dboper->mdlDB05->record(iRow).value("result").toString();
		if(dboper->mdlDB05->record(iRow).value("result").toString()==tr("AWARD")) {
			strInfo+=" "+tr("#")+dboper->mdlDB05->record(iRow).value("decisionnum").toString()+" ";
			strInfo+=tr("from ")+dboper->mdlDB05->record(iRow).value("decisiondate").toDate().toString("dd.MM.yyyy")+", ";
			strInfo+="<b>"+tr("Order")+"</b>"+"<br /><br />";
			if(dboper->mdlDB05->record(iRow).value("executed").toBool()) {
				strInfo+="<b>"+tr("Order executed")+"</b><br /><br />";
				strInfo+="<b>"+tr("Execution info: ")+"</b>"+dboper->mdlDB05->record(iRow).value("executioninfo").toString();
			}
			else strInfo+="<b>"+tr("Order not executed")+"</b><br /><br />";
		}
	}
	else if(iMode==1) {
		strInfo="<b>"+tr("Reg N: ")+"</b>"+dboper->mdlDB05->record(iRow).value("regnum").toString()+" ";
		strInfo+="<b>"+tr("Reg date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("regdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Executor: ")+"</b>"+dboper->mdlDB05->record(iRow).value("executor").toString()+" ";
		strInfo+="<b>"+tr("Handover date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("handoverdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Complainant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("complainant").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Defendant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("defendant").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Clause: ")+"</b>"+dboper->mdlDB05->record(iRow).value("clause").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Result: ")+"</b>"+dboper->mdlDB05->record(iRow).value("result").toString();
		if(dboper->mdlDB05->record(iRow).value("result").toString()==tr("CASE"))
			strInfo+=" "+tr("#")+dboper->mdlDB05->record(iRow).value("casenum").toString()+" ";
	}
	else if(iMode==2) {
		strInfo="<b>"+tr("Claim N: ")+"</b>"+dboper->mdlDB05->record(iRow).value("claimnum").toString()+" ";
		strInfo+="<b>"+tr("Claim date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("claimdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Case N: ")+"</b>"+dboper->mdlDB05->record(iRow).value("casenum").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Decree #")+"</b>"+dboper->mdlDB05->record(iRow).value("decreenum").toString()+" ";
		strInfo+=tr("from ")+dboper->mdlDB05->record(iRow).value("decreedate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Executor: ")+"</b>"+dboper->mdlDB05->record(iRow).value("executor").toString()+" ";
		strInfo+="<b>"+tr("Handover date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("handoverdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Complainant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("complainant").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Defendant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("defendant").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Clause: ")+"</b>"+dboper->mdlDB05->record(iRow).value("clause").toString()+"<br /><br />";
		strInfo+="<b>"+tr("Committee date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("committeedate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
		strInfo+="<b>"+tr("Result: ")+"</b>"+dboper->mdlDB05->record(iRow).value("result").toString();
		if(dboper->mdlDB05->record(iRow).value("result").toString()==tr("AWARD")) {
			//strInfo+=" "+tr("#")+dboper->mdlDB05->record(iRow).value("decisionnum").toString()+" ";
			//strInfo+=tr("from ")+dboper->mdlDB05->record(iRow).value("decisiondate").toDate().toString("dd.MM.yyyy");//+", ";
			if(dboper->mdlDB05->record(iRow).value("order").toBool()) strInfo+="<b>"+tr("Order")+"</b>"+"<br /><br />";
			if(dboper->mdlDB05->record(iRow).value("orderexec").toBool()) {
				strInfo+="<b>"+tr("Order executed")+"</b><br /><br />";
				strInfo+="<b>"+tr("Execution info: ")+"</b>"+dboper->mdlDB05->record(iRow).value("execinfo").toString();
			}
			else strInfo+="<b>"+tr("Order not executed")+"</b><br /><br />";
		}
	}
    else if(iMode==3) {
        strInfo="<b>"+tr("Case N: ")+"</b>"+dboper->mdlDB05->record(iRow).value("casenum").toString()+" ";
        strInfo+="<b>"+tr("Excitation date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("exdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
        strInfo+="<b>"+tr("Executor: ")+"</b>"+dboper->mdlDB05->record(iRow).value("executor").toString()+" ";
        strInfo+="<b>"+tr("Defendant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("defendant").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Clause: ")+"</b>"+dboper->mdlDB05->record(iRow).value("clause").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Clause part: ")+"</b>"+dboper->mdlDB05->record(iRow).value("clausepart").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Date of judgement: ")+"</b>"+dboper->mdlDB05->record(iRow).value("dateofjudg").toDateTime().toString("dd.MM.yyyy hh:mm:ss")+"<br /><br />";
        strInfo+="<b>"+tr("Decision: ")+"</b>"+dboper->mdlDB05->record(iRow).value("decision").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Amount: ")+"</b>"+dboper->mdlDB05->record(iRow).value("amount").toString()+"<br /><br />";
    }
    else if(iMode==4) {
        strInfo="<b>"+tr("Case N: ")+"</b>"+dboper->mdlDB05->record(iRow).value("casenum").toString()+" ";
        strInfo+="<b>"+tr("Claim date: ")+"</b>"+dboper->mdlDB05->record(iRow).value("claimdate").toDate().toString("dd.MM.yyyy")+"<br /><br />";
        strInfo+="<b>"+tr("Executor: ")+"</b>"+dboper->mdlDB05->record(iRow).value("executor").toString()+" ";
        strInfo+="<b>"+tr("Complainant: ")+"</b>"+dboper->mdlDB05->record(iRow).value("complainant").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Clause: ")+"</b>"+dboper->mdlDB05->record(iRow).value("clause").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Third persons: ")+"</b>"+dboper->mdlDB05->record(iRow).value("thirdpersons").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Other persons: ")+"</b>"+dboper->mdlDB05->record(iRow).value("otherpersons").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Judge: ")+"</b>"+dboper->mdlDB05->record(iRow).value("judge").toString()+"<br /><br />";
        strInfo+="<b>"+tr("Trial date/time: ")+"</b>"+dboper->mdlDB05->record(iRow).value("trialdatetime").toDateTime().toString("dd.MM.yyyy hh:mm:ss")+"<br /><br />";
        strInfo+="<b>"+tr("Court address: ")+"</b>"+dboper->mdlDB05->record(iRow).value("courtaddress").toString();
        strInfo+="<b>"+tr("Case card url: ")+"</b>"+dboper->mdlDB05->record(iRow).value("casecardurl").toString();
        strInfo+="<b>"+tr("Result: ")+"</b>"+dboper->mdlDB05->record(iRow).value("result").toString();
        strInfo+="<b>"+tr("Instance: ")+"</b>"+dboper->mdlDB05->record(iRow).value("instance").toString();
    }
	ui->teInfo->setHtml(strInfo);

	dboper->stmdlEOincomingDocs->setTable("incoming");
	QString strFilter;
	strFilter="[attachedto] = '";
	if(iMode==0) strFilter+=tr("Eighteenone archive");
	else if(iMode==1) strFilter+=tr("Claims archive");
	else if(iMode==2) strFilter+=tr("Cases archive");
    else if(iMode==3) strFilter+=tr("Administrative cases archive");
    else if(iMode==2) strFilter+=tr("Lawsuites archive");
	strFilter+="' AND [attachedtodocid]="+dboper->mdlDB05->record(iRow).value("id").toString();

	dboper->stmdlEOincomingDocs->setFilter(strFilter);
	dboper->stmdlEOincomingDocs->select();
	dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
	dboper->stmdlEOincomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOincomingDocs->setHeaderData(8, Qt::Horizontal, tr("Addressee"));
	dboper->stmdlEOincomingDocs->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->stmdlEOincomingDocs->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	ui->tvIncoming->setModel(dboper->stmdlEOincomingDocs);
	ui->tvIncoming->hideColumn(0);
	int i;
	for(i=3;i<8;i++) ui->tvIncoming->hideColumn(i);
	ui->tvIncoming->hideColumn(9);
	for(i=12;i<20;i++) ui->tvIncoming->hideColumn(i);

	ui->tvIncoming->resizeColumnsToContents();
	ui->tvIncoming->horizontalHeader()->setStretchLastSection(true);

    dboper->stmdlEOoutcomingDocs->setTable("documents");
	if(iMode==0) strFilter="[attachedtodoctype] = 'eo_a'";
	else if(iMode==1) strFilter="[attachedtodoctype] = 'acl_a'";
	else if(iMode==2) strFilter="[attachedtodoctype] = 'acs_a'";
    else if(iMode==3) strFilter="[attachedtodoctype] = 'adc_a'";
    else if(iMode==4) strFilter="[attachedtodoctype] = 'ls_a'";
		
    strFilter+=" AND [attachedtodocid]="+dboper->mdlDB05->record(iRow).value("id").toString();

    dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
    dboper->stmdlEOoutcomingDocs->select();
    dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document number"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Document date"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Reg Num"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Document Type"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Post ID"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Execution date"));
	ui->tvDocuments->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(7, new DataDelegate());
    ui->tvDocuments->setModel(dboper->stmdlEOoutcomingDocs);
    ui->tvDocuments->hideColumn(0);

    for(i=8;i<16;i++) ui->tvDocuments->hideColumn(i);
    ui->tvDocuments->resizeColumnsToContents();
    ui->tvDocuments->horizontalHeader()->setStretchLastSection(true);

	setVisible(true);
}

void wgtarchivehistory::SetArchiveHistoryLinks() {
	wAddOutDoc->dboper=dboper;
}
