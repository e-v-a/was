#ifndef WGTINBOXCLAIMS_H
#define WGTINBOXCLAIMS_H

#include <QWidget>
#include "wgtattachedto.h"

namespace Ui {
class wgtInboxClaims;
}

class wgtInboxClaims : public QWidget
{
	Q_OBJECT

public:
	explicit wgtInboxClaims(QWidget *parent = 0);
	~wgtInboxClaims();
	QStringList stlDoctype;
	QStringList stlExecutor;
	QStringList stlAttachedtotype;
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	bool bIsSearch;
	DBOperations *dboper;
	
public slots:
	void SetInboxFields(int recordnum);
	void SetAttachment(int iId);
	void clearInboxSearch();
	void setAttachedOperLink();

signals:
	void SaveIncomingForm(QString executor, QDate &handoverdate, QString doctype, bool isattached, QString attachedtotype,
		QString attachedtolink);
	void SendInternalMessage(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage,
		QString strAttachedType, int iAttachedDocId);
	void LoadTable(QString table,QString executor);
	void setStatusText(int iCurrentRow, int iRowsCount);
	void UpdateClaimsNum(int iClaimsNum);
	void closeIncoming();
	void setFilterMB(QString sFilter);
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);

private slots:
    void on_pbFirst_clicked();
    void on_pbPrev_clicked();
    void on_pbSave_clicked();
    void on_pbNext_clicked();
    void on_pbLast_clicked();
    void on_pbDeleteAttachment_clicked();
    void on_cbExecutor_currentIndexChanged(const QString &arg1);
    void on_cbDocType_currentIndexChanged(const QString &arg1);
    void on_deHandoverDate_dateChanged(const QDate &date);
    void on_cbAttach_stateChanged(int arg1);
    void on_cbAttachedToType_currentIndexChanged(const QString &arg1);
    void on_leAttachedToLink_textChanged(const QString &arg1);
    void on_cbAttachedToType_currentTextChanged(const QString &arg1);
    void on_pbAddAttachment_clicked();
    void on_cbExecutor_currentTextChanged(const QString &arg1);
    void on_pbShowSearch_toggled(bool checked);
    void on_leSearch_returnPressed();
    void on_pbSearch_clicked();
    void on_pbClear_clicked();
    void on_leSearch_textChanged(const QString &arg1);
    void on_cbTheirDocs_toggled(bool checked);
    void on_leHandoverDep_textChanged(const QString &arg1);

private:
	Ui::wgtInboxClaims *ui;
	//QSqlQueryModel *sqmdlAttachmentId;
	QString strAttachmentID;
	QSqlQuery query;
//	int iQuerySize;
//	int iCurrentRow;
	int iNextRow;
	wgtattachedto *wAttach;
	int iIdR;
	QString strSearchString;
	QAction *actSearch;
};

#endif // WGTINBOXCLAIMS_H
