#include "wgtdocsforagreement.h"
#include "ui_wgtdocsforagreement.h"

wgtdocsforagreement::wgtdocsforagreement(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtdocsforagreement)
{
    ui->setupUi(this);
	
	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtdocsforagreement::~wgtdocsforagreement()
{
    delete ui;
}

void wgtdocsforagreement::SetD4AFormFields(int recordnum) {
	QString strBuffer;
	ui->leDocNum->setText(dboper->stmdlEOoutcomingDocs->record(recordnum).value("docnum").toString());
	ui->leOutcomeNum->setText(dboper->stmdlEOoutcomingDocs->record(recordnum).value("regnum").toString());
	ui->deDocDate->setDate(dboper->stmdlEOoutcomingDocs->record(recordnum).value("docdate").toDate());
	ui->deOutcomeDate->setDate(dboper->stmdlEOoutcomingDocs->record(recordnum).value("regdate").toDate());
	ui->leDocType->setText(dboper->stmdlEOoutcomingDocs->record(recordnum).value("doctype").toString());
	ui->deExecDate->setDate(dboper->stmdlEOoutcomingDocs->record(recordnum).value("execdate").toDate());
	strBuffer="[id] = "+dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodocid").toString();
	if(dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodoctype").toString()=="eo") {
		ui->leAttachedTo->setText(tr("Eighteen one claim"));
		dboper->mdlDB05->setTable("eighteenone");
	}
	else if(dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodoctype").toString()=="acl") {
		ui->leAttachedTo->setText(tr("Claim"));
		dboper->mdlDB05->setTable("amzclaims");
	}
	else if(dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodoctype").toString()=="acs") {
		ui->leAttachedTo->setText(tr("Case"));
		dboper->mdlDB05->setTable("amzcases");
	}
	else if(dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodoctype").toString()=="adc") {
		ui->leAttachedTo->setText(tr("Administrative case"));
		dboper->mdlDB05->setTable("admcases");
	}
	dboper->mdlDB05->setFilter(strBuffer);
	dboper->mdlDB05->select();
	if(dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodoctype").toString()=="eo" || dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodoctype").toString()=="acl")
		ui->leAttachedToNum->setText(dboper->mdlDB05->record(0).value("regnum").toString());
	else if(dboper->stmdlEOoutcomingDocs->record(recordnum).value("attachedtodoctype").toString()=="acs")
		ui->leAttachedToNum->setText(dboper->mdlDB05->record(0).value("casenum").toString());
	ui->leExecutor->setText(dboper->mdlDB05->record(0).value("executor").toString());
	ui->teComplainant->setPlainText(dboper->mdlDB05->record(0).value("complainant").toString());
	ui->teDefendant->setPlainText(dboper->mdlDB05->record(0).value("defendant").toString());
	ui->pbFirst->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbPrev->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbNext->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	ui->pbLast->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
}
void wgtdocsforagreement::on_pbFirst_clicked()
{
	iCurrentRow=0;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetD4AFormFields(iCurrentRow);
}

void wgtdocsforagreement::on_pbPrev_clicked()
{
	iCurrentRow--;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetD4AFormFields(iCurrentRow);
}

void wgtdocsforagreement::on_pbNext_clicked()
{
	iCurrentRow++;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetD4AFormFields(iCurrentRow);
}

void wgtdocsforagreement::on_pbLast_clicked()
{
	iCurrentRow=iRowCount-1;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetD4AFormFields(iCurrentRow);
}

void wgtdocsforagreement::on_pbView_clicked()
{
	QString strDestPath=dboper->EnvSettings.strCommonFolder+"/"+dboper->stmdlEOoutcomingDocs->record(iCurrentRow).value("textfile").toString();
	QDesktopServices::openUrl(QUrl::fromLocalFile(strDestPath));
}

void wgtdocsforagreement::on_pbReturn_clicked()
{
	QSqlRecord record=dboper->stmdlEOoutcomingDocs->record(iCurrentRow);
	record.setValue("senttoagreement",false);
	record.setValue("returned",true);
	dboper->stmdlEOoutcomingDocs->setRecord(iCurrentRow,record);
	dboper->stmdlEOoutcomingDocs->submitAll();
	record.clear();
	if(iRowCount!=1 && iCurrentRow!=iRowCount-1) iNextRow=iCurrentRow;
	else if(iCurrentRow==iRowCount-1 && iRowCount!=1) iNextRow=iCurrentRow-1;
	else if(iRowCount==1) iNextRow=-1;
	dboper->stmdlEOoutcomingDocs->select();
	iRowCount=dboper->stmdlEOoutcomingDocs->rowCount();

	if(iNextRow!=-1) {
		emit setStatusText(iNextRow+1, iRowCount);
		emit UpdateDocsNum(iRowCount);
		SetD4AFormFields(iNextRow);
	}
	else clearD4AFields();
}

void wgtdocsforagreement::on_pbAgree_clicked()
{
	QSqlRecord record=dboper->stmdlEOoutcomingDocs->record(iCurrentRow);
	record.setValue("agreed",true);
	dboper->stmdlEOoutcomingDocs->setRecord(iCurrentRow,record);
	dboper->stmdlEOoutcomingDocs->submitAll();
	record.clear();
	if(iRowCount!=1 && iCurrentRow!=iRowCount-1) iNextRow=iCurrentRow;
	else if(iCurrentRow==iRowCount-1 && iRowCount!=1) iNextRow=iCurrentRow-1;
	else if(iRowCount==1) iNextRow=-1;
	dboper->stmdlEOoutcomingDocs->select();
	iRowCount=dboper->stmdlEOoutcomingDocs->rowCount();

	if(iNextRow!=-1) {
		emit setStatusText(iNextRow+1, iRowCount);
		emit UpdateDocsNum(iRowCount);
		SetD4AFormFields(iNextRow);
	}
	else clearD4AFields();
}

void wgtdocsforagreement::clearD4AFields() {
	ui->deDocDate->setDate(QDate(2013,1,1));
	ui->deExecDate->setDate(QDate(2013,1,1));
	ui->deOutcomeDate->setDate(QDate(2013,1,1));
	ui->leAttachedTo->clear();
	ui->leAttachedToNum->clear();
	ui->leDocNum->clear();
	ui->leDocType->clear();
	ui->leExecutor->clear();
	ui->leOutcomeNum->clear();
	ui->teComplainant->clear();
	ui->teDefendant->clear();
}