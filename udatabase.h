#ifndef UDATABASE_H
#define UDATABASE_H

#include <QtWidgets>
#include <QtSql>

enum { PRIVATE_QUERY, PUBLIC_QUERY, TIMER_QUERY, TIMER_LOCAL_QUERY, TIMER_MESSAGES_QUERY };

struct stkEnvironment {
	bool bMWState;
	int iMWWidth;
	int iMWHeight;
	QString strCommonFolder;
	QString strPublishFolder;
	bool bSync;
	bool bConsole;
};

struct stkPermissions {
	bool bEighteenone; //1
	bool bClaims; //2
	bool bCases; //4
	bool bAdmCases; //8
	bool bLawSuites; //16
	bool bFASOrders; //32
	bool bArchiveEighteenOne; //64
	bool bArchiveClaims; //128
	bool bArchiveCases; //256
	bool bArchiveAdmCases; //512
	bool bArchiveLawSuites; //1024
	bool bArchiveFASOrders; //2048
	bool bAllIncoming; //4096
	bool bSelfReport; //8192
	bool bFullReport; //16384
	bool bHandoverIncoming; //32768
	bool bHandoverAllIncoming; //65536
	bool bUserManagement; //131072
};

struct stkUser {
	int iUid;
    QString sUserName;
    double iUserPermissions;
	stkPermissions stkUserPermissions;
};

struct stkShedule {
	bool bExpired;
	QDate dtDateOfEvent;
	QTime tmTimeOfEvent;
	QString strLinkId;
	QString strNotificationText;
	QString strExecutor;
};


class DBOperations : public QObject
{
	Q_OBJECT;

public:
	explicit DBOperations(QObject *parent = 0);
	~DBOperations();

	/* ������ ���� � ������ */
    bool dbConn(const bool bLocalDB=false,const QString sDBName="", const QString sConnectionName="",const QString sServAddr="", const QString sUsername="", const QString sPassword="");
	bool dbConnLocal(const QString sDBName, const QString sConnectionName);
	void dbCloseConn(const QString sConnectionName);
	void dbQuery(const int querymode, const QString &sConnectionName, const int iAccessType, const QString &sFields, const QString &sValues, const QString &sTable, const bool &bSelect, const QString &sSelectString);
	bool dbWriteData(bool bIsNew,bool bIsDB05Model,const int iRow, const QStringList stlFields, const QList<QVariant> stlValues);
	/* ����� ���������� ����� */

	bool IsFiteredByUser();
	bool IsFitered();
	QSqlError dbConnectionError;
	QSqlError dbLocalConnectionError;
	QSqlQuery query;
	QSqlQuery TimerQuery;
	QSqlQuery TimerMessagesQuery;
	QSqlQuery TimerLocalQuery;
	QSqlTableModel *mdlSettings;
	QSqlTableModel *mdlDB05;
	QSqlTableModel *stmdlEOincomingDocs;
	QSqlTableModel *stmdlEOoutcomingDocs;
	stkEnvironment EnvSettings;
	stkUser UsrSettings;

public slots:
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
	void MainTableAssign(QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
	void setFilterMB(QString sFilter);

private:
	bool bFilter;
	bool bFilterByUser;
};

#endif // UDATABASE_H
